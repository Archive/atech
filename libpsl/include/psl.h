/*
    Portable Sound Library - a portable interface to sound hardware
    Copyright (C) 1998  Andrew Clausen  <clausen@alphalink.com.au>
        
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.
                        
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.
                                        
    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the
    Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA  02111-1307  USA.
                                                        
    I can be contacted by mail:
    Andrew Clausen
    18 Shaw St
    Ashwood, 3147
    Victoria, Australia
*/

#ifndef PSL_H_INCLUDED
#define PSL_H_INCLUDED

#include <pthread.h>

/* parameters stuff */
/********************/
typedef enum _psl_direction         psl_direction;
typedef enum _psl_stereo            psl_stereo;
typedef enum _psl_sample_encoding   psl_sample_encoding;

enum _psl_direction {
    PSL_DIR_PLAY=1,
    PSL_DIR_RECORD=2,
    PSL_DIR_CONFIGABLE=3,
    PSL_DIR_DUPLEX=4        /* should only be used for devices, not channels */
};

#define PSL_DIR_WORST   PSL_DIR_PLAY
#define PSL_DIR_BEST    PSL_DIR_DUPLEX

enum _psl_stereo {
    PSL_MONO=1,
    PSL_STEREO=2,
    PSL_QUAD=4              /* unsupported - but put it in there ;) */
};

enum _psl_sample_encoding {
    PSL_ENCODING_UNSIGNED_DIFF_ENDIAN   = 1,
    PSL_ENCODING_UNSIGNED_SAME_ENDIAN   = 2,
    PSL_ENCODING_SIGNED_DIFF_ENDIAN     = 3,
    PSL_ENCODING_SIGNED_SAME_ENDIAN     = 4,
    PSL_ENCODING_FLOAT                  = 5
};

#define PSL_ENCODING_WORST      PSL_ENCODING_UNSIGNED_DIFF_ENDIAN
#define PSL_ENCODING_BEST       PSL_ENCODING_SIGNED_SAME_ENDIAN

extern char *psl_format_str[];

struct _psl_params {
    psl_direction       dir;                /* play/record/configuriable */
    int                 freq;               /* frequency */
    psl_stereo          stereo;             /* mono/stereo/quad */
    psl_sample_encoding encoding;           /* 8 bit/16 bit/float */
    int                 sample_size;        /* bytes per sample */
};

/* device & channel stuff */
/**************************/
typedef struct _psl_params          psl_params;
typedef struct _psl_channel         psl_channel;
typedef struct _psl_device          psl_device;

struct _psl_channel {
    psl_channel    *next;
    
    char           *name;               /* unique device name, or "GUS no 2"? */
    int             channel_no;         /* unique number that identifies dev. */
    psl_device     *device;             /* device that channel is on */

    int             active;             /* 1 if playing/recording */

    psl_params      current;            /* current settings in use */

    void           *buffer;             /* audio buffer */
    psl_params      conversion;         /* conversion type */
    int             frag_size;          /* buffer size (in OS) - samples */
    int             frag_count;         /* no blocks to read/write before
                                           looping back to beginning */

    pthread_mutex_t transfer_begin_mutex;
    pthread_cond_t  transfer_begin_cond;
    int             buffer_waiting;
    int             buffer_complete;

    pthread_mutex_t transfer_complete_mutex;
    pthread_cond_t  transfer_complete_cond;
};

/*
    a "device" contains channels up to 4 channels (L in, R in, L out, R out).
    This is just a nice hack to allow abstraction.
*/
struct _psl_device {
    psl_device         *next;

    void               *device_info;    /* driver specific/non-portable info */
    char               *name;
    int                 channel_base_no;/* number to start counting channels */
                                        /* from */
    psl_channel        *ch[4];

    int                 split_duplex;
    int                 split_stereo_in;
    int                 split_stereo_out;

    void               *in_buffer;
    void               *out_buffer;
    int                 frag_size;
    int                 frag_count;

/*
    settings for <duplex recording> or normal record/playback (depending on
    split_duplex)
*/
    psl_params          min;
    psl_params          current;
    psl_params          max;

/* settings for duplex playback */
    psl_params          play_min;
    psl_params          play_current;
    psl_params          play_max;

    pthread_mutex_t     read_thread_active;
    pthread_mutex_t     write_thread_active;

    pthread_t           read_thread;
    pthread_t           write_thread;
};

/* pereference stuff */
/*********************/
typedef enum _psl_priority  psl_priority;

enum _psl_priority {
    PSL_PREFER_DIR,
    PSL_PREFER_FREQ,
    PSL_PREFER_STEREO,
    PSL_PREFER_SAMPLE_SIZE
};

#define PSL_PRIORITY_COUNT  4

extern psl_params           default_params;
extern psl_priority         default_priority[];

psl_priority *psl_priority_list_new ();
void psl_priority_list_destroy (psl_priority *list);

/*  sets priority level.  If negative, count from end (eg -2 means second
    last, 1 means first) */
void psl_priority_set (psl_priority *list, psl_priority priority, int level);

#define PSL_SPLIT_STEREO_IN     1
#define PSL_SPLIT_STEREO_OUT    2
#define PSL_FORCE_PLAYBACK      4
#define PSL_FORCE_RECORD        8

int psl_init (int *argc, char *argv[], psl_params *best_params,
              psl_priority *params_priority, int flags);
                                        /* accepts some kind of parameters? */
                                        /* necessary?  perhaps remapping devs */
                                        /* should set all volume levels to
                                           unity - could be configured on cmd
                                           line if you want something other than
                                           default */

int psl_done ();

int psl_activate ();
int psl_deactivate ();

psl_channel *psl_channel_get_next (psl_channel *ch);/* returns the next channel
                                                after ch, first for ch==NULL */
psl_channel *psl_channel_get (int no);
psl_channel *psl_channel_get_by_name (char *name);

int psl_channel_validate (psl_channel *ch);     /* check if settings are ok */

void psl_start_transfer (psl_channel *ch);
void psl_complete_transfer (psl_channel *ch);

int psl_channel_activate (psl_channel *ch);
int psl_channel_deactivate (psl_channel *ch);

psl_device *psl_device_get_next (psl_device *dev);
int psl_device_configure (psl_device *dev, psl_params *best_params,
                          psl_priority *params_priority, int flags);

/* if you think this looks flash, look at convert.c */
#define psl_params_set_encoding(params, t)                          \
    {                                                               \
        params.encoding  = ((t)-1 == -1)                            \
                            ? (                                     \
                                ((t)(1.0)/10 == 0)                  \
                                ? PSL_ENCODING_SIGNED_SAME_ENDIAN   \
                                : PSL_ENCODING_FLOAT                \
                            )                                       \
                            : PSL_ENCODING_UNSIGNED_SAME_ENDIAN;    \
        params.sample_size = sizeof(t);                             \
    }                                                               \

/* internal stuff */

extern psl_channel     *first_channel;
extern psl_device      *first_device;
extern int              psl_active;
extern void            *silence;                /* buffer of zeros */

void psl_error (char *format, ...);
psl_channel *psl_channel_alloc ();

#endif

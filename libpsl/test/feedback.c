#include <math.h>
#include <stdio.h>
#include "psl.h"

int
main (int argc, char *argv[]) {
    psl_channel     *ch;
    psl_channel     *rec_ch;
    psl_channel     *play_ch_1;
    psl_channel     *play_ch_2;
    int              i, j, k;
    float           *rec_buf;
    float           *play_buf_1;
    signed short    *play_buf_2;        /* just show off the fact that
                                           you can have different conversions */

    psl_init (&argc, argv, &default_params, default_priority,
              PSL_SPLIT_STEREO_IN | PSL_SPLIT_STEREO_OUT);

    ch = NULL;
    play_ch_1 = NULL;
    play_ch_2 = NULL;
    rec_ch = NULL;
    while ( (ch = psl_channel_get_next (ch)) != NULL) {
        printf ("%d: %s: %d Hz %d bit %s\n",
                ch->channel_no,
                ch->name,
                ch->current.freq,
                ch->current.sample_size * 8,
                (ch->current.stereo == PSL_STEREO) ? "stereo" : "mono");

        printf ("%d\n", ch->current.encoding);

        if (ch->current.dir == PSL_DIR_PLAY)
            if (play_ch_1 == NULL)
                play_ch_1 = ch;
            else
                play_ch_2 = ch;

        if (ch->current.dir == PSL_DIR_RECORD) rec_ch = ch;
    }

    if (play_ch_1 == NULL || play_ch_2 == NULL || rec_ch == NULL) {
        printf ("you need a record and two playback channels\n");
        return 0;
    }

    printf ("using %s for playback...\n", play_ch_1->name);
    printf ("using %s for playback...\n", play_ch_2->name);
    printf ("using %s for recording...\n", rec_ch->name);

    psl_params_set_encoding(play_ch_1->conversion, float);
    psl_params_set_encoding(play_ch_2->conversion, signed short);
    psl_params_set_encoding(rec_ch->conversion, float);

    if (!psl_channel_activate (play_ch_1)) {
        printf ("Error activating play channel.\n");
        psl_done ();
        return 0;
    }

    if (!psl_channel_activate (play_ch_2)) {
        printf ("Error activating play channel.\n");
        psl_done ();
        return 0;
    }

    if (!psl_channel_activate (rec_ch)) {
        printf ("Error activating record channel.\n");
        psl_done ();
        return 0;
    }

    printf ("Buffer size is %d samples.  Latency should be %f seconds\n",
            play_ch_1->frag_size,
            2.0 * play_ch_1->frag_size / play_ch_1->current.freq);

    psl_activate ();

/* can assume that buffer stays the same during one session */
    rec_buf = (float *) rec_ch->buffer;
    play_buf_1 = (float *) play_ch_1->buffer;
    play_buf_2 = (signed short *) play_ch_2->buffer;

    while (1) {
        psl_complete_transfer (rec_ch);

        for (j=0; j<rec_ch->frag_size; j++) {
            play_buf_1[j] = rec_buf[j];
            play_buf_2[j] = -32768 * rec_buf[j];  /* - to change phase,
                                                     for no apparent reason
                                                     (other than it sounds
                                                     good)
                                                  */
        }

        /*  need to start all channels in same direction before complete
            can be demanded - otherwise, will just lock up if two channels
            are on the same device (for play_ch_1 to complete, play_ch_1 and
            play_ch_2 MUST be started.  Think about it) */
        psl_start_transfer (play_ch_1);
        psl_start_transfer (play_ch_2);
        psl_complete_transfer (play_ch_1);
        psl_complete_transfer (play_ch_2);
    }

    psl_deactivate ();

    psl_done ();
}

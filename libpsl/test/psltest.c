#include <math.h>
#include <stdio.h>
#include "psl.h"

int
main (int argc, char *argv[]) {
    psl_channel     *ch;
    psl_channel     *play_ch;
    int              i, j, k;
    float           *buffer;

    psl_init (&argc, argv, &default_params, default_priority,
              PSL_SPLIT_STEREO_IN | PSL_SPLIT_STEREO_OUT);

/* test if device can be reconfigured */
    psl_device_configure (psl_device_get_next (NULL),
                          &default_params, default_priority,
                          PSL_SPLIT_STEREO_OUT);

    ch = NULL;
    play_ch = NULL;
    while ( (ch = psl_channel_get_next (ch)) != NULL) {
        printf ("%d: %s: %d Hz %d bit %s\n",
                ch->channel_no,
                ch->name,
                ch->current.freq,
                ch->current.sample_size * 8,
                (ch->current.stereo == PSL_STEREO) ? "stereo" : "mono");
        
        if (ch->current.dir == PSL_DIR_PLAY) play_ch = ch;
    }

    if (play_ch == NULL) {
        printf ("error: no devices available for mono playback\n");
        exit (0);
    }

    printf ("using %s for playback...\n", play_ch->name);

    psl_params_set_encoding (play_ch->conversion, float);

    if (!psl_channel_activate (play_ch)) {
        printf ("Error activating sound card.\n");
        psl_done ();
        return 0;
    }

    psl_activate ();

/* can assume that buffer stays the same during one session */
    buffer = (float *) play_ch->buffer;

    k=0;
    for (i=0; i<100; i++) {
        for (j=0; j<play_ch->frag_size; j++, k++) {
            buffer [j] = sin (1.0 * k / (10 * i / 100.0));
        }

        psl_complete_transfer (play_ch);
    }

    psl_deactivate ();

    psl_done ();
}

/*
    Portable Sound Library - a portable interface to sound hardware
    Copyright (C) 1998  Andrew Clausen  <clausen@alphalink.com.au>
        
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.
                        
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.
                                        
    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the
    Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA  02111-1307  USA.
                                                        
    I can be contacted by mail:
    Andrew Clausen
    18 Shaw St
    Ashwood, 3147
    Victoria, Australia
*/

/*
    mass production of code: the bizarrest code that you've seen for a while
    anyone feel like calculating how much code is generated?

    WARNING: in traditional practise, it is better to rewrite bad code, rather
    than try and explain it (with comments).  I have chosen the latter because
    writing the code (as opposed to re-writing) would take AGES, and would be
    about 6*6*6*6*20 lines of code (about 26000).
    
    Basically, what I'm doing is generating a whole lot of conversion routines
    through MACROS.  This is no trivial task...
*/

#include <stdlib.h>
#include <config.h>
#include "psl.h"

/* hack to make all type names a single word - so they can become part of
   a conversion routine name automagically */
#ifndef uchar
typedef unsigned char   uchar;
#endif

#ifndef schar
typedef signed char     schar;
#endif

#ifndef ushort
typedef unsigned short  ushort;
#endif

#ifndef sshort
typedef signed short    sshort;
#endif

#define uchar_scale     -128        /* hack:  negative means unsigned */
#define schar_scale     128         /* 128 = not perfect (should be 127), but */
#define ushort_scale    -32768      /* FAST */
#define sshort_scale    32768
#define float_scale     1.0

/*  I'm beginning to like this :-)  Should write a program entirely in
    macros.
*/
#define SCALE(src_t) (src_t ##_scale)

/*  returns 1 if t is a type that is signed */
#define IS_SIGNED(t) (SCALE(t)>0)
#define IS_FLOAT(t) (SCALE(t)==1.0)

#define ABS_SCALE(t) ((SCALE(t)>0) ? SCALE(t) : - SCALE(t))

#define REL_SCALE(dst_t, src_t) (ABS_SCALE(dst_t) / ABS_SCALE(src_t))

/*  macro that "returns" a "routine" to convert src, of type
    src_t to dst_t.  It only accesses src once, so SCALE_FUNC(x,x, src++)
    can be used.
*/
#define SCALE_FUNC(src_t, dst_t, src)                                   \
(                                                                       \
    (IS_SIGNED(src_t) != IS_SIGNED(dst_t))                              \
    ? (                                                                 \
        (IS_SIGNED(src_t))                                              \
        ? (                                                             \
            (ABS_SCALE(dst_t) > ABS_SCALE(src_t))                       \
            ?   ( (dst_t) ((src) + ABS_SCALE(src_t)) )                  \
                    * REL_SCALE (dst_t, src_t)                          \
            :   ( (dst_t) ((src) / REL_SCALE(src_t, dst_t)) )           \
                    + ABS_SCALE(dst_t)                                  \
        )                                                               \
        : (                                                             \
            (ABS_SCALE(dst_t) > ABS_SCALE(src_t))                       \
            ?   ( (dst_t) ((src) - ABS_SCALE(src_t)) )                  \
                    * REL_SCALE(dst_t, src_t)                           \
            :   (dst_t) ( (src) * REL_SCALE(dst_t, src_t) )             \
                    - ABS_SCALE(dst_t)                                  \
        )                                                               \
    )                                                                   \
    : (                                                                 \
        (ABS_SCALE(dst_t) > ABS_SCALE(src_t))                           \
        ?   (IS_FLOAT(src_t))                                           \
            ? (dst_t) ( (src) * REL_SCALE(dst_t, src_t) )               \
            : ((dst_t) (src)) * REL_SCALE(dst_t, src_t)                 \
        :   (dst_t) ((src) / REL_SCALE(src_t, dst_t))                   \
    )                                                                   \
)

#define CONVERT_MONO(src_t, dst_t)            \
    void m_ ##src_t ##_ ##dst_t (void *src, void *dst, int cnt) {     \
        if (SCALE(src_t) == SCALE(dst_t))               \
            memcpy (src, dst, cnt * sizeof (src_t));    \
        else                                            \
            while (cnt--)                               \
                *((dst_t*)dst)++ = SCALE_FUNC(src_t, dst_t, *((src_t*)src)++); \
    }

#define CONVERT_STEREO(src_t, dst_t)          \
    void s_ ##src_t ##_ ##dst_t (void *src, void *dst, int cnt) { \
        cnt *= 2;                                   \
        while (cnt--)                               \
            *((dst_t*)dst)++ = SCALE_FUNC(src_t, dst_t, *((src_t*)src)++); \
    }

#define CONVERT_MONO_STEREO(src_t, dst_t)     \
    void m_s_ ##src_t ##_ ##dst_t (void *src, void *dst, int cnt) {     \
        while (cnt--)                                                   \
            *((dst_t*)dst)++ = *((dst_t*)dst)++                         \
                = SCALE_FUNC(src_t, dst_t, *((src_t*)src)++);           \
    }

#define CONVERT_SPLIT_STEREO(src_t, l_t, r_t)       \
    void split_ ##src_t ##_ ##l_t ##_ ##r_t         \
        (void *src, void *l, void *r, int cnt) {    \
        if (l==NULL || r==NULL) {                   \
            if (l==NULL && r==NULL) return;         \
            if (l==NULL) {l = r; ((src_t*)src)++;}  \
            while (cnt--) {                         \
                *((l_t*)l)++ = SCALE_FUNC(src_t, l_t, *(src_t*)src); \
                (src_t*)src += 2;                   \
            }                                       \
        }                                           \
        else                                        \
            while (cnt--) {                         \
                *((l_t*)l)++ = SCALE_FUNC(src_t, l_t, *((src_t*)src)++); \
                *((r_t*)r)++ = SCALE_FUNC(src_t, r_t, *((src_t*)src)++); \
            }                                       \
    }

#define CONVERT_JOIN_STEREO(l_t, r_t, dst_t)                                \
    void join_ ##l_t ##_ ##r_t ##_ ##dst_t                                  \
         (void *l, void *r, void *dst, int cnt) {                           \
        while (cnt--) {                                                     \
            *((dst_t*)dst)++ = SCALE_FUNC(l_t, dst_t, *((l_t*)l)++);        \
            *((dst_t*)dst)++ = SCALE_FUNC(r_t, dst_t, *((r_t*)r)++);        \
        }                                                                   \
    }

#define NULL_FUNC (void (*psl_convert) (void *, void *, int)) (NULL)


#define CREATE_SIMPLE_FUNCS(src_t, dst_t)       \
    CONVERT_MONO(src_t, dst_t)                  \
    CONVERT_STEREO(src_t, dst_t)                \
    CONVERT_MONO_STEREO(src_t, dst_t)

#define CREATE_SPLIT_FUNCS(other_t, l_t, r_t) \
    CONVERT_SPLIT_STEREO(other_t, l_t, r_t)   \
    CONVERT_JOIN_STEREO(l_t, r_t, other_t) 

#define CREATE_FUNCS(src_t, dst_t)              \
    CREATE_SIMPLE_FUNCS(src_t, dst_t)           \
    CREATE_SPLIT_FUNCS(src_t, dst_t, uchar)     \
    CREATE_SPLIT_FUNCS(src_t, dst_t, schar)     \
    CREATE_SPLIT_FUNCS(src_t, dst_t, ushort)    \
    CREATE_SPLIT_FUNCS(src_t, dst_t, sshort)    \
    CREATE_SPLIT_FUNCS(src_t, dst_t, float)

#define CREATE_X_TO_ALL(src_t)   \
    CREATE_FUNCS(src_t, uchar)   \
    CREATE_FUNCS(src_t, schar)   \
    CREATE_FUNCS(src_t, ushort)  \
    CREATE_FUNCS(src_t, sshort)  \
    CREATE_FUNCS(src_t, float)   \

CREATE_X_TO_ALL(uchar)
CREATE_X_TO_ALL(schar)
CREATE_X_TO_ALL(ushort)
CREATE_X_TO_ALL(sshort)
CREATE_X_TO_ALL(float)

#define NAME_SIMPLE_FUNCS(src_t, dst_t)    \
    {                               \
        m_ ##src_t ##_ ##dst_t,     \
        s_ ##src_t ##_ ##dst_t,     \
        m_s_ ##src_t ##_ ##dst_t,   \
    }

#define NAME_X_TO_SIMPLES(src_t)            \
    {                                       \
        NAME_SIMPLE_FUNCS(src_t, uchar),    \
        NAME_SIMPLE_FUNCS(src_t, schar),    \
        NAME_SIMPLE_FUNCS(src_t, ushort),   \
        NAME_SIMPLE_FUNCS(src_t, sshort),   \
        NAME_SIMPLE_FUNCS(src_t, float)     \
    }

static void *
psl_simple_converts [5][5][5] = {
    NAME_X_TO_SIMPLES(uchar),
    NAME_X_TO_SIMPLES(schar),
    NAME_X_TO_SIMPLES(ushort),
    NAME_X_TO_SIMPLES(sshort),
    NAME_X_TO_SIMPLES(float)
};

#define NAME_SPLIT_PAIR(other_t, l_t, r_t)      \
    {                                           \
        split_ ##other_t ##_ ##l_t ##_ ##r_t,   \
        join_ ##l_t ##_ ##r_t ##_ ##other_t     \
    }

#define NAME_SPLIT_FUNCS(other_t, l_t)          \
    {                                           \
        NAME_SPLIT_PAIR(other_t, l_t, uchar),   \
        NAME_SPLIT_PAIR(other_t, l_t, schar),   \
        NAME_SPLIT_PAIR(other_t, l_t, ushort),  \
        NAME_SPLIT_PAIR(other_t, l_t, sshort),  \
        NAME_SPLIT_PAIR(other_t, l_t, float)    \
    }

#define NAME_X_TO_SPLITS(other_t)           \
    {                                       \
        NAME_SPLIT_FUNCS(other_t, uchar),   \
        NAME_SPLIT_FUNCS(other_t, schar),   \
        NAME_SPLIT_FUNCS(other_t, ushort),  \
        NAME_SPLIT_FUNCS(other_t, sshort),  \
        NAME_SPLIT_FUNCS(other_t, float)    \
    }

static void *
psl_split_converts [5][5][5][2] = {
    NAME_X_TO_SPLITS(uchar),
    NAME_X_TO_SPLITS(schar),
    NAME_X_TO_SPLITS(ushort),
    NAME_X_TO_SPLITS(sshort),
    NAME_X_TO_SPLITS(float)
};

/* TODO: support different endianness */
int
psl_convert_type (psl_params *params) {
    if (params->sample_size == 1) {
        if (params->encoding == PSL_ENCODING_UNSIGNED_SAME_ENDIAN)
            return 0;   /* uchar */
        else
            return 1;   /* schar */
    } else {
        switch (params->encoding) {
            case PSL_ENCODING_UNSIGNED_SAME_ENDIAN: return 2;   /* ushort */
            case PSL_ENCODING_SIGNED_SAME_ENDIAN: return 3;     /* sshort */
            case PSL_ENCODING_FLOAT: return 4;                  /* float */
            default: return -1;
        }
    }
}

void *
psl_convert_func (psl_params *src, psl_params *dst, int flags) {
    return psl_simple_converts  [psl_convert_type (src)]
                                [psl_convert_type (dst)]
                                [flags];
}

void *
psl_split_func (psl_params *src, psl_params *l, psl_params *r) {
    return psl_split_converts   [psl_convert_type (src)]
                                [psl_convert_type (l)]
                                [psl_convert_type (r)]
                                [0];
}

void *
psl_join_func (psl_params *l, psl_params *r, psl_params *dst) {
    return psl_split_converts   [psl_convert_type (dst)]
                                [psl_convert_type (l)]
                                [psl_convert_type (r)]
                                [1];
}

int
psl_buffer_size (psl_params *parms, int sample_count) {
    return sample_count * parms->sample_size * parms->stereo;
}


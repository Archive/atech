/*
    Portable Sound Library - a portable interface to sound hardware
    Copyright (C) 1998  Andrew Clausen  <clausen@alphalink.com.au>
        
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.
                        
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.
                                        
    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the
    Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA  02111-1307  USA.

    I can be contacted by mail:
    Andrew Clausen
    18 Shaw St
    Ashwood, 3147
    Victoria, Australia
*/                                                     
                                                        
#include <stdio.h>
#include <stdarg.h>

#include "psl.h"
#include "sounddev.h"

int             psl_active = 0;
psl_channel    *first_channel = (psl_channel *) NULL;
psl_device     *first_device = (psl_device *) NULL;
char           *psl_format_str[] = {
                    "unsigned little endian",
                    "unsigned big endian",
                    "signed little endian",
                    "signed big endian",
                    "float",
                    NULL
                };
psl_params      default_params = {
                    PSL_DIR_DUPLEX,
                    44100,
                    PSL_STEREO,
                    2,
                    2
                };
psl_priority    default_priority[] = {
                    PSL_PREFER_FREQ,
                    PSL_PREFER_DIR,
                    PSL_PREFER_SAMPLE_SIZE,
                    PSL_PREFER_STEREO
                };
void           *silence;

/*
    prints out an error message
*/
void psl_error (char *format, ...) {
    va_list     parms;
    
    fprintf (stderr, "psl: ");

    va_start (parms, format);
    vfprintf (stderr, format, parms);
    va_end (parms);

    fprintf (stderr, "\n");
}

/*
    adds channel ch to the list
*/
void
psl_channel_add (psl_channel *ch) {
    psl_channel     *walk;
    psl_channel     *last;

    if (first_channel == NULL) {
        ch->next = NULL;
        first_channel = ch;
        return;
    }

    for (walk=first_channel, last=NULL;
         walk->next!=NULL && walk->channel_no < ch->channel_no;
         last=walk, walk=walk->next) ;

    if (walk->channel_no < ch->channel_no) {
        ch->next = walk->next;
        walk->next = ch;
    } else {
        if (last == NULL) {
            ch->next = first_channel;
            first_channel = ch;
        } else {
            ch->next = last->next;
            last->next = ch;
        }
    }
}

/*
    removes channel from the list - doesn't free up memory.
*/
void
psl_channel_remove (psl_channel *ch) {
    psl_channel     *walk;

    if (first_channel==ch) {
        first_channel = ch->next;
        return;
    }

    for (walk=first_channel; walk!=NULL && walk->next!=ch; walk=walk->next);
    if (walk==NULL) return;

    walk->next = walk->next->next;
}

/*
    removes channel from list AND frees up memory
*/
void
psl_channel_destroy (psl_channel *ch) {
    psl_channel_remove (ch);
    free (ch->name);
    free (ch);
}

psl_channel *
psl_channel_alloc () {
    psl_channel     *ch;

    ch = (psl_channel *) malloc (sizeof (psl_channel));
    if (ch == NULL) {
        fprintf (stderr, "psl_channel_alloc: out of memory");
        exit (1);
    }
    return ch;
}

/*
    returns next channel, or if ch==NULL, first_channel
*/
psl_channel *
psl_channel_get_next (psl_channel *ch) {
    if (ch == NULL)
        return first_channel;
    else
        return ch->next;
}

/*
    returns channel with channel_no==no
*/
psl_channel *
psl_channel_get (int no) {
    psl_channel     *ch;
    
    for (ch = first_channel; ch != NULL && ch->channel_no != no;
         ch = ch->next) ;

    return ch;
}

/*
    returns channel with channel_name==name
*/
psl_channel *
psl_channel_get_by_name (char *name) {
    psl_channel     *ch;

    for (ch = first_channel; ch != NULL && strcmp (name, ch->name) != 0;
         ch = ch->next) ;

    return ch;
}

/*
    marks a channel to be active (used) when the whole system is active.
*/
int
psl_channel_activate (psl_channel *ch) {
    if (psl_active) return 0;
    if (ch->active) return 1;
    
    if (!psl_device_validate (ch->device)) return 0;

    ch->buffer = (void *) malloc (ch->frag_size * ch->conversion.sample_size);
    if (ch->buffer == NULL) return 0;
    
    ch->active = 1;
    return 1;
}

/*
    marks a channel as inactive (unused) when the whole system is active.
*/
int
psl_channel_deactivate (psl_channel *ch) {
    if (psl_active) return 0;
    if (!ch->active) return 1;

    free (ch->buffer);

    ch->active = 0;
    return 0;
}

void
psl_device_add (psl_device *dev) {
    dev->next = first_device;
    first_device = dev;
}

psl_device *
psl_device_get_next (psl_device *dev) {
    if (dev == NULL)
        return first_device;
    else
        return dev->next;
}

/*
    activates the sound system, beginning transfers on all channels marked
    as "active"
*/
int
psl_activate () {
    psl_device *dev;
    psl_device *unstarted;
    int         failed = 0;

    psl_active = 1;

    dev = NULL;
    while ( (dev = psl_device_get_next (dev)) != NULL)
        if (!psl_device_start (dev)) {
            failed = 1;
            break;
        }

/*
    if initialising one channel fails, must go through and stop all the ones
    that were already started
*/
    if (failed) {
        unstarted = dev;
        psl_active = 0;
        
        dev = NULL;
        while ( (dev = psl_device_get_next (dev)) != NULL && dev != unstarted )
            psl_device_stop (dev);

        return 0;
    }

    return 1;
}

/*
    deactivates the sound system, halting all transfers
*/
int
psl_deactivate () {
    psl_device *dev;

    if (!psl_active) return 1;

    psl_active = 0;     /* tells all server threads to stop */

    dev = NULL;
    while ( (dev = psl_device_get_next (dev)) != NULL)
        psl_device_stop (dev);

    return 1;
}

/*
    initialises psl, and detects devices

    FIXME  should parse command line, and adjust flags
*/
int
psl_init (int *argc, char *argv [], psl_params *best_params,
          psl_priority *params_priority, int flags) {

    psl_active = 0;
    
    silence = (void *) malloc (65536);
    if (silence == NULL) return 0;
    memset (silence, 0, 65536);

    psl_probe_devices (best_params, params_priority, flags);
    return 1;
}

/*
    shuts down the psl sound system
*/
int
psl_done () {
    psl_deactivate ();
    psl_destroy_devices ();
    free (silence);
    return 1;
}

psl_priority *
psl_priority_list_new () {
    return (psl_priority *) malloc (sizeof (psl_priority) * PSL_PRIORITY_COUNT);
}

void
psl_priority_list_destroy (psl_priority *list) {
    free (list);
}

/*  sets priority level.  If negative, count from end (eg -2 means second
    last, 1 means first) */
void
psl_priority_set (psl_priority *list, psl_priority priority, int level) {
    int     old_level;
    int     i;

/* convert level into "real counting" */
    if (level > 0)
        level--;
    else
        level += PSL_PRIORITY_COUNT;

    for (old_level=0; old_level<PSL_PRIORITY_COUNT; old_level++)
        if (list[old_level] == priority) break;

    if (old_level > PSL_PRIORITY_COUNT - 1) return;  /* doesn't exist! */

    if (level < old_level) {
        for (i=level+1; i<old_level; i++)
            list[i] = list[i-1];
    } else {
        for (i=old_level; i<level; i++)
            list[i] = list[i+1];
    }
    list[level] = priority;
}


/*
    Portable Sound Library - a portable interface to sound hardware
    Copyright (C) 1998  Andrew Clausen  <clausen@alphalink.com.au>
        
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.
                        
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.
                                        
    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the
    Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA  02111-1307  USA.
                                                        
    I can be contacted by mail:
    Andrew Clausen
    18 Shaw St
    Ashwood, 3147
    Victoria, Australia
*/

#include "config.h"

#ifdef HAVE_OSS

#include <sys/soundcard.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

#include "psl.h"
#include "sounddev.h"
#include "convert.h"

#define DEVICE_PREFIX   "/dev/dsp"

int TEST_FREQ[] = {
    11025,
    22050,
    44100,
    48000
};

typedef struct _oss_device_info oss_device_info;

struct _oss_device_info {
    int                     device_no;

    int                     fd;     /* file descriptor - always keep open */
    pthread_mutex_t         hw_access_mutex;
    audio_buf_info          in_buf_info;
    audio_buf_info          out_buf_info;
};

/*
    brute-force attacks OSS storage and saves in psl_form
*/
static int
oss_fmt_to_psl (int *oss_form, psl_params *psl_form) {
    switch (*oss_form) {
    case AFMT_U8:
        psl_form->encoding = PSL_ENCODING_UNSIGNED_SAME_ENDIAN;
        psl_form->sample_size = 1;
        break;
        
    case AFMT_S8:
        psl_form->encoding = PSL_ENCODING_SIGNED_SAME_ENDIAN;
        psl_form->sample_size = 1;
        break;
        
    case AFMT_U16_LE:
#ifdef WORDS_BIGENDIAN
        psl_form->encoding = PSL_ENCODING_UNSIGNED_DIFF_ENDIAN;
#else
        psl_form->encoding = PSL_ENCODING_UNSIGNED_SAME_ENDIAN;
#endif
        psl_form->sample_size = 2;
        break;
        
    case AFMT_U16_BE:
#ifdef WORDS_BIGENDIAN
        psl_form->encoding = PSL_ENCODING_UNSIGNED_SAME_ENDIAN;
#else
        psl_form->encoding = PSL_ENCODING_UNSIGNED_DIFF_ENDIAN;
#endif
        psl_form->sample_size = 2;
        break;
        
    case AFMT_S16_LE:
#ifdef WORDS_BIGENDIAN
        psl_form->encoding = PSL_ENCODING_SIGNED_DIFF_ENDIAN;
#else
        psl_form->encoding = PSL_ENCODING_SIGNED_SAME_ENDIAN;
#endif
        psl_form->sample_size = 2;
        break;
        
    case AFMT_S16_BE:
#ifdef WORDS_BIGENDIAN
        psl_form->encoding = PSL_ENCODING_SIGNED_SAME_ENDIAN;
#else
        psl_form->encoding = PSL_ENCODING_SIGNED_DIFF_ENDIAN;
#endif
        psl_form->sample_size = 2;
        break;

    default:
        return 0;
    }
    return 1;
}

/*
    converts psl format of storage type to oss
*/
static int
psl_to_oss_fmt (psl_params *psl_form, int *oss_form) {
    if (psl_form->sample_size == 1) {
        switch (psl_form->encoding) {
        case PSL_ENCODING_SIGNED_SAME_ENDIAN:
            *oss_form = AFMT_S8;
            break;
            
        case PSL_ENCODING_UNSIGNED_SAME_ENDIAN:
            *oss_form = AFMT_U8;
            break;

        default:
            return 0;
        }
    } else {
        switch (psl_form->encoding) {
        case PSL_ENCODING_SIGNED_SAME_ENDIAN:
#ifdef WORDS_BIGENDIAN
            *oss_form = AFMT_S16_BE;
#else
            *oss_form = AFMT_S16_LE;
#endif
            break;
            
        case PSL_ENCODING_SIGNED_DIFF_ENDIAN:
#ifdef WORDS_BIGENDIAN
            *oss_form = AFMT_S16_LE;
#else
            *oss_form = AFMT_S16_BE;
#endif
            break;
            
        case PSL_ENCODING_UNSIGNED_SAME_ENDIAN:
#ifdef WORDS_BIGENDIAN
            *oss_form = AFMT_U16_BE;
#else
            *oss_form = AFMT_U16_LE;
#endif
            break;
            
        case PSL_ENCODING_UNSIGNED_DIFF_ENDIAN:
#ifdef WORDS_BIGENDIAN
            *oss_form = AFMT_U16_LE;
#else
            *oss_form = AFMT_U16_BE;
#endif
            break;
            
        default:
            return 0;
        }
    }
    return 1;
}

int
oss_set_format (psl_device *dev, int size, psl_sample_encoding encoding) {
    psl_params  params;
    int         oss_fmt;

    params.sample_size  = size;
    params.encoding     = encoding;
    psl_to_oss_fmt (&params, &oss_fmt);

    if (ioctl ( ((oss_device_info *) dev->device_info)->fd, SNDCTL_DSP_SETFMT,
               &oss_fmt) == -1)
        return 0;
    else
        return 1;
}

int
oss_set_stereo (psl_device *dev, psl_stereo stereo) {
    int i;

    i = (stereo == PSL_STEREO);
    if (ioctl ( ((oss_device_info *) dev->device_info)->fd, SNDCTL_DSP_STEREO,
               &i) == -1)
        return 0;
    else
        return 1;
}

int
oss_set_freq (psl_device *dev, int freq) {
    if (ioctl ( ((oss_device_info *) dev->device_info)->fd, SNDCTL_DSP_SPEED,
               &freq) == -1)
        return 0;
    else
        return 1;
}

int
oss_open_stream (psl_device *dev) {
    oss_device_info    *dev_info;
    int                 tmp;

    dev_info = (oss_device_info *) dev->device_info;

    close (dev_info->fd);

    switch (dev->current.dir) {
    case PSL_DIR_DUPLEX:
        dev_info->fd = open (dev->name, O_RDWR, 0);
        if (ioctl (dev_info->fd, SNDCTL_DSP_SETDUPLEX, 0) == -1) return 0;
        break;

    case PSL_DIR_PLAY:
        dev_info->fd = open (dev->name, O_WRONLY, 0);
        break;

    case PSL_DIR_RECORD:
        dev_info->fd = open (dev->name, O_RDONLY, 0);
        break;
    }

    if (dev_info->fd == -1) {
        perror (dev->name);
        return 0;
    }

/* calculate log2 (dev->frag_size) */
    for (tmp=0; 1<<tmp < psl_buffer_size (&dev->current, dev->frag_size);
         tmp++);
    if (dev->current.stereo) tmp++;

    tmp += 0x02 << 0x10;        /* frag_count goes in top bits */

    if (ioctl (dev_info->fd, SNDCTL_DSP_SETFRAGMENT, &tmp) == -1) {
        perror (dev->name);
        return 0;
    }

/* set parameters */
    if (!oss_set_format (dev, dev->current.sample_size, dev->current.encoding))
        return 0;
    if (!oss_set_stereo (dev, dev->current.stereo)) return 0;
    if (!oss_set_freq (dev, dev->current.freq)) return 0;

    return 1;
}

psl_device *
oss_device_new (int dev_no) {
    psl_device         *dev;
    oss_device_info    *dev_info;
    int                 size;
    int                 encoding;
    int                 i;

    dev = (psl_device *) malloc (sizeof (psl_device));
    if (dev == NULL) return NULL;
    dev->device_info = (void *) malloc (sizeof (oss_device_info));
    if (dev->device_info == NULL) {
        free (dev);
        return NULL;
    }
    dev_info = (oss_device_info *) dev->device_info;

    dev_info->device_no = dev_no;

    dev->name = (char *) malloc (strlen (DEVICE_PREFIX) + 3);
    if (dev->name == NULL) {
        free (dev->device_info);
        free (dev);
        return NULL;
    }
    sprintf (dev->name, "%s%d", DEVICE_PREFIX, dev_no);

    pthread_mutex_init (&dev->read_thread_active, NULL);
    pthread_mutex_init (&dev->write_thread_active, NULL);
    pthread_mutex_init (&dev_info->hw_access_mutex, NULL);
        
    dev->ch[0] = (psl_channel *) NULL;
    dev->ch[1] = (psl_channel *) NULL;
    dev->ch[2] = (psl_channel *) NULL;
    dev->ch[3] = (psl_channel *) NULL;

    dev->in_buffer = NULL;
    dev->out_buffer = NULL;

/* try and open the device, and see which modes work */
    dev_info->fd = open (dev->name, O_WRONLY, 0);
    if (dev_info->fd == -1) {
        dev->min.dir = PSL_DIR_PLAY;
    } else {
        dev->min.dir = PSL_DIR_RECORD;
        close (dev_info->fd);
    }

    dev_info->fd = open (dev->name, O_WRONLY, 0);
    if (dev_info->fd == -1) {
        dev->max.dir = PSL_DIR_RECORD;
    } else {
        dev->max.dir = PSL_DIR_PLAY;
        close (dev_info->fd);
    }

    dev_info->fd = open (dev->name, O_RDWR, 0);
    if (dev_info->fd != -1) {
        dev->max.dir = PSL_DIR_DUPLEX;
        close (dev_info->fd);
    }

    if (dev->min.dir == PSL_DIR_PLAY && dev->max.dir == PSL_DIR_RECORD) {
        /* assume the device doesn't exist - no point reporting errors */
        free (dev->name);
        free (dev->device_info);
        free (dev);
        return NULL;
    }

/* now open device in the minimum mode, to see what it can do */
    dev_info->fd = open (dev->name,
                         (dev->min.dir == PSL_DIR_PLAY)? O_WRONLY : O_RDONLY,
                         0);

/* search for min and max formats */
    dev->min.sample_size = 0;
    for (size=1; size<=2 && dev->min.sample_size==0; size++)
        for (encoding = PSL_ENCODING_UNSIGNED_LITTLE_ENDIAN;
             encoding <= PSL_ENCODING_SIGNED_BIG_ENDIAN;
             encoding++)
            if (oss_set_format (dev, size, encoding)) {
                dev->min.sample_size = size;
                dev->min.encoding = encoding;
                break;
            }

    if (dev->min.sample_size == 0) {
        fprintf (stderr, "%s: no formats supported\n", dev->name);
        free (dev->name);
        free (dev->device_info);
        free (dev);
        return NULL;
    }

    dev->max.sample_size    = dev->min.sample_size;
    dev->max.encoding       = dev->min.encoding;
    for (size=1; size<=2; size++)
        for (encoding = PSL_ENCODING_UNSIGNED_LITTLE_ENDIAN;
             encoding <= PSL_ENCODING_SIGNED_BIG_ENDIAN;
             encoding++)
            if (oss_set_format (dev, size, encoding)) {
                dev->max.sample_size = size;
                dev->max.encoding = encoding;
                break;
            }

/*  assume best no channels occurs for minimum sample encoding */
    oss_set_format (dev, dev->min.sample_size, dev->min.encoding);

    if (oss_set_stereo (dev, PSL_MONO))
        dev->min.stereo = PSL_STEREO;
    else
        dev->min.stereo = PSL_MONO;

    if (oss_set_stereo (dev, PSL_STEREO))
        dev->max.stereo = PSL_MONO;
    else
        dev->max.stereo = PSL_STEREO;

/*  search for best sample rate: assume that the best sample rate occurs
    when everything else is at "minimum".  (as on SB Pro cards)
*/
    oss_set_format (dev, dev->min.sample_size, dev->min.encoding);
    oss_set_stereo (dev, dev->min.stereo);

    dev->min.freq = 0;
    for (i=0; TEST_FREQ[i] != 0; i++)
        if (oss_set_freq (dev, TEST_FREQ[i])) {
            dev->min.freq = TEST_FREQ[i];
            break;
        }

    if (dev->min.freq == 0) {
        fprintf (stderr, "%s: no sample rates supported\n", dev->name);
        free (dev->name);
        free (dev->device_info);
        free (dev);
        return NULL;
    }

    for (; TEST_FREQ[i] != 0; i++)
        if (oss_set_freq (dev, TEST_FREQ[i])) dev->max.freq = TEST_FREQ[i];

    dev->frag_size = 1024;

    psl_device_add (dev);

    return dev;
}

void
psl_probe_devices (psl_params *best_params,
                   psl_priority *params_priority, int flags) {
    int         dev_no;
    psl_device *dev;

    for (dev_no=0; dev_no<16; dev_no++) {
        dev = oss_device_new (dev_no);
        if (dev != NULL)
                psl_device_configure (dev, best_params, params_priority, flags);
    }
}

void
oss_device_destroy (psl_device *dev) {
    close ( ((oss_device_info *)dev->device_info) ->fd );

    psl_device_clean (dev);
    free (dev->device_info);
    free (dev->name);
    free (dev);
}

void
psl_destroy_devices () {
    psl_device *temp;

    if (psl_active) return;

    while (first_device != NULL) {
        temp = first_device;
        first_device = first_device->next;
        oss_device_destroy (temp);
    }
}

int
psl_device_validate (psl_device *dev) {
    return oss_open_stream (dev);
}

int
psl_device_start (psl_device *dev) {
    memset (dev->in_buffer, 0, psl_buffer_size (&dev->current, dev->frag_size));    memset (dev->out_buffer, 0, psl_buffer_size(&dev->current, dev->frag_size));
    psl_device_alloc_buffers (dev);
        
    if (!oss_open_stream (dev)) return 0;
        
    pthread_mutex_init (&dev->read_thread_active, NULL);
    pthread_mutex_init (&dev->write_thread_active, NULL);
                    
    pthread_create (&dev->read_thread, NULL, psl_device_read_thread, dev);
    pthread_create (&dev->write_thread, NULL, psl_device_write_thread, dev);

    return 1;
}

int
psl_device_stop (psl_device *dev) {
    int     ch;
    
    /* if there's a channel sitting waiting to play, flush the buffer */
    for (ch=0; ch<4; ch++)
        if (dev->ch[ch] != NULL)
            if (dev->ch[ch]->active) psl_start_transfer (dev->ch[ch]);

    /* wait for threads to exit (the psl_active flags will tell them...) */
    pthread_mutex_lock (&dev->read_thread_active);
    pthread_mutex_lock (&dev->write_thread_active);
                                                    
    pthread_mutex_unlock (&dev->read_thread_active);
    pthread_mutex_unlock (&dev->write_thread_active);
                                                            
    psl_device_free_buffers (dev);

    return 1;
}

void
psl_block_write (psl_device *dev, void *buffer) {
    oss_device_info    *dev_info;

    dev_info = (oss_device_info *) dev->device_info;
    
/* OSS doesn't like threads - so avoid conflict */
    pthread_mutex_lock (&dev_info->hw_access_mutex);

    write (dev_info->fd, buffer,
           psl_buffer_size (&dev->current, dev->frag_size));

    pthread_mutex_unlock (&dev_info->hw_access_mutex);
}

void
psl_block_read (psl_device *dev, void *buffer) {
    oss_device_info    *dev_info;
    dev_info = (oss_device_info *) dev->device_info;
    
/* OSS doesn't like threads - so avoid conflict */
    pthread_mutex_lock (&dev_info->hw_access_mutex);
    read ( ((oss_device_info *) dev->device_info)->fd, buffer,
          psl_buffer_size (&dev->current, dev->frag_size));
    pthread_mutex_unlock (&dev_info->hw_access_mutex);
}

#endif /* HAVE_OSS */

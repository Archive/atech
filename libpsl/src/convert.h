/*
    Portable Sound Library - a portable interface to sound hardware
    Copyright (C) 1998  Andrew Clausen  <clausen@alphalink.com.au>
        
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.
                        
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.
                                        
    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the
    Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA  02111-1307  USA.
                                                        
    I can be contacted by mail:
    Andrew Clausen
    18 Shaw St
    Ashwood, 3147
    Victoria, Australia
*/

#ifndef PSL_CONVERT_H_INCLUDED
#define PSL_CONVERT_H_INCLUDED

#define MONO_MONO       0
#define STEREO_STEREO   1
#define MONO_STEREO     2
#define SPLIT_STEREO    3
#define JOIN_STEREO     4

extern int psl_convert_type (psl_params *params);
extern void *psl_convert_func (psl_params *src, psl_params *dst, int flags);
extern void *psl_split_func (psl_params *src, psl_params *l, psl_params *r);
extern void *psl_join_func (psl_params *l, psl_params *r, psl_params *dst);
extern int psl_buffer_size (psl_params *parms, int sample_count);
    
#endif

/*
    Portable Sound Library - a portable interface to sound hardware
    Copyright (C) 1998  Andrew Clausen  <clausen@alphalink.com.au>
        
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.
                        
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.
                                        
    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the
    Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA  02111-1307  USA.
                                                        
    I can be contacted by mail:
    Andrew Clausen
    18 Shaw St
    Ashwood, 3147
    Victoria, Australia
*/

#ifndef SOUNDDEV_H_INCLUDED
#define SOUNDDEV_H_INCLUDED

/* functions drivers must provide*/
void psl_probe_devices (psl_params *best_params, psl_priority *params_priority, 
                        int flags);

void psl_block_read (psl_device *dev, void *buffer);

void psl_block_write (psl_device *dev, void *buffer);

void psl_destroy_devices ();

/* useful functions for drivers (device.c) */
void psl_device_read_thread (psl_device *dev);

void psl_device_write_thread (psl_device *dev);

void psl_device_alloc_buffers (psl_device *dev);

void psl_device_free_buffers (psl_device *dev);

void psl_device_clean (psl_device *dev);

#endif

/*
    Portable Sound Library - a portable interface to sound hardware
    Copyright (C) 1998  Andrew Clausen  <clausen@alphalink.com.au>
        
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.
                        
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.
                                        
    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the
    Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA  02111-1307  USA.
                                                        
    I can be contacted by mail:
    Andrew Clausen
    18 Shaw St
    Ashwood, 3147
    Victoria, Australia
*/

#include "config.h"

#ifdef HAVE_ALSA

#include <sys/soundlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <pthread.h>

#include "psl.h"
#include "sounddev.h"
#include "convert.h"

#define MIN_BUF_SIZE    1024 /* buffer size in samples (bytes = stereo*bits) */

typedef struct _alsa_device_info    alsa_device_info;
typedef struct _alsa_card           alsa_card;

struct _alsa_device_info {
    psl_device                     *device;

    int                             device_no;
    void                           *handle;
    alsa_card                      *card;
    
    struct snd_pcm_playback_info    pcm_play_info;
    struct snd_pcm_info             pcm_info;
};

struct _alsa_card {
    int                     channel_count;  /* number of pcm devs used */
    int                     card_no;
    struct snd_ctl_hw_info  card_info;
    void                   *handle;
};

/*
    prints out ALSA error messages nicely, but doesn't exit.  The device
    is not used, but other devices might be ok...
*/
static void
alsa_error (int code, int card, int pcm) {
    if (pcm==-1)
        psl_error ("ALSA card %d: %s", card, snd_strerror (code));
    else
        psl_error ("ALSA card %d pcm %d: %s", card, pcm, snd_strerror (code));
}

/*
    Opens an ALSA card, gets some info on the card, and initialises an
    alsa_card structure.
*/
static alsa_card *
alsa_card_new (int card_no) {
    alsa_card  *card;
    void       *ctl_handle;
    int         status;

    card = (alsa_card *) malloc (sizeof (alsa_card));
    card->channel_count = 0;

    status = snd_ctl_open (&ctl_handle, card_no);
    if (status != 0) {
        alsa_error (status, card_no, -1);
        free (card);
        return NULL;
    }

    status = snd_ctl_hw_info (ctl_handle, &card->card_info);
    if (status != 0) {
        alsa_error (status, card_no, -1);
        free (card);
        return NULL;
    }

    status = snd_ctl_close (ctl_handle);
    if (status != 0) {
        alsa_error (status, card_no, -1);
        free (card);
        return NULL;
    }

    return card;
}

static void
alsa_card_destroy (alsa_card *card) {
    snd_ctl_close (card->handle);
    free (card);
    return;
}

/*
    brute-force attacks ALSA storage and saves in psl_form
*/
static int
alsa_fmt_to_psl (struct snd_pcm_format *alsa_form, psl_params *psl_form) {
    psl_form->freq  = alsa_form->rate;

    if (alsa_form->channels == 2)
        psl_form->stereo = PSL_STEREO;
    else
        psl_form->stereo = PSL_MONO;
    
    switch (alsa_form->format) {
    case SND_PCM_SFMT_U8:
        psl_form->encoding = PSL_ENCODING_UNSIGNED_SAME_ENDIAN;
        psl_form->sample_size = 1;
        break;
        
    case SND_PCM_SFMT_S8:
        psl_form->encoding = PSL_ENCODING_SIGNED_SAME_ENDIAN;
        psl_form->sample_size = 1;
        break;
        
    case SND_PCM_SFMT_U16_LE:
#ifdef WORDS_BIGENDIAN
        psl_form->encoding = PSL_ENCODING_UNSIGNED_DIFF_ENDIAN;
#else
        psl_form->encoding = PSL_ENCODING_UNSIGNED_SAME_ENDIAN;
#endif
        psl_form->sample_size = 2;
        break;
        
    case SND_PCM_SFMT_U16_BE:
#ifdef WORDS_BIGENDIAN
        psl_form->encoding = PSL_ENCODING_UNSIGNED_SAME_ENDIAN;
#else
        psl_form->encoding = PSL_ENCODING_UNSIGNED_DIFF_ENDIAN;
#endif
        psl_form->sample_size = 2;
        break;
        
    case SND_PCM_SFMT_S16_LE:
#ifdef WORDS_BIGENDIAN
        psl_form->encoding = PSL_ENCODING_SIGNED_DIFF_ENDIAN;
#else
        psl_form->encoding = PSL_ENCODING_SIGNED_SAME_ENDIAN;
#endif
        psl_form->sample_size = 2;
        break;
        
    case SND_PCM_SFMT_S16_BE:
#ifdef WORDS_BIGENDIAN
        psl_form->encoding = PSL_ENCODING_SIGNED_SAME_ENDIAN;
#else
        psl_form->encoding = PSL_ENCODING_SIGNED_DIFF_ENDIAN;
#endif
        psl_form->sample_size = 2;
        break;

    default:
        return 0;
    }
    return 1;
}

/*
    converts psl format of storage type to alsa
*/
static int
psl_to_alsa_fmt (psl_params *psl_form, struct snd_pcm_format *alsa_form) {
    alsa_form->rate     = psl_form->freq;
    alsa_form->channels = (psl_form->stereo == PSL_STEREO) ? 2 : 1;

    if (psl_form->sample_size == 1) {
        switch (psl_form->encoding) {
        case PSL_ENCODING_SIGNED_SAME_ENDIAN:
            alsa_form->format = SND_PCM_SFMT_S8;
            break;
            
        case PSL_ENCODING_UNSIGNED_SAME_ENDIAN:
            alsa_form->format = SND_PCM_SFMT_U8;
            break;

        default:
            return 0;
        }
    } else {
        switch (psl_form->encoding) {
        case PSL_ENCODING_SIGNED_SAME_ENDIAN:
#ifdef WORDS_BIGENDIAN
            alsa_form->format = SND_PCM_SFMT_S16_BE;
#else
            alsa_form->format = SND_PCM_SFMT_S16_LE;
#endif
            break;
            
        case PSL_ENCODING_SIGNED_DIFF_ENDIAN:
#ifdef WORDS_BIGENDIAN
            alsa_form->format = SND_PCM_SFMT_S16_LE;
#else
            alsa_form->format = SND_PCM_SFMT_S16_BE;
#endif
            break;
            
        case PSL_ENCODING_UNSIGNED_SAME_ENDIAN:
#ifdef WORDS_BIGENDIAN
            alsa_form->format = SND_PCM_SFMT_U16_BE;
#else
            alsa_form->format = SND_PCM_SFMT_U16_LE;
#endif
            break;
            
        case PSL_ENCODING_UNSIGNED_DIFF_ENDIAN:
#ifdef WORDS_BIGENDIAN
            alsa_form->format = SND_PCM_SFMT_U16_LE;
#else
            alsa_form->format = SND_PCM_SFMT_U16_BE;
#endif
            break;
            
        default:
            return 0;
        }
    }
    return 1;
}

static int
alsa_close_stream (psl_device *dev) {
    alsa_device_info   *dev_info = (alsa_device_info *) dev->device_info;

    snd_pcm_close (dev_info->handle);
    dev_info->handle = NULL;
}

/*
    attempts to open a stream on device dev, and returns 1 on success, 0 on
    failure.
*/
static int
alsa_open_stream (psl_device *dev) {
    alsa_device_info               *dev_info;
    alsa_card                      *card;
    int                             status;
    struct snd_pcm_format           alsa_format;
    struct snd_pcm_playback_params  play_params;
    struct snd_pcm_record_params    rec_params;

    dev_info    = (alsa_device_info *) dev->device_info;
    card        = dev_info->card;

    if (!psl_to_alsa_fmt (&dev->current, &alsa_format)) return 0;
    dev->play_current = dev->current;  /* is the case for MOST cards. */

    play_params.fragment_size   = psl_buffer_size (&dev->current,
                                                   dev->frag_size);
    play_params.fragments_max   = 1;
    play_params.fragments_room  = 2;

    rec_params.fragment_size    = play_params.fragment_size;
    rec_params.fragments_min    = 1;

//    printf ("alsa_open_stream: \trate=%d channels=%d format=%d\n",
//        alsa_format.rate, alsa_format.channels, alsa_format.format);

    if (dev->current.dir == PSL_DIR_DUPLEX)
        status = snd_pcm_open (&dev_info->handle, card->card_no,
                               dev_info->device_no, SND_PCM_OPEN_DUPLEX);
    else if (dev->current.dir == PSL_DIR_RECORD)
        status = snd_pcm_open (&dev_info->handle, card->card_no,
                               dev_info->device_no, SND_PCM_OPEN_RECORD);
    else
        status = snd_pcm_open (&dev_info->handle, card->card_no,
                               dev_info->device_no, SND_PCM_OPEN_PLAYBACK);
    if (status != 0 || dev_info->handle == NULL) {
        alsa_error (status, card->card_no, dev_info->device_no);
        return 0;
    }

    if (dev->current.dir==PSL_DIR_DUPLEX || dev->current.dir==PSL_DIR_PLAY) {
        status = snd_pcm_playback_format (dev_info->handle, &alsa_format);
        if (status != 0) {
            snd_pcm_close (dev_info->handle);
            return 0;
        }

        status = snd_pcm_playback_params (dev_info->handle, &play_params);
        if (status != 0) {
            snd_pcm_close (dev_info->handle);
            return 0;
        }
    }

    if (dev->current.dir==PSL_DIR_DUPLEX || dev->current.dir==PSL_DIR_RECORD) {
        status = snd_pcm_record_format (dev_info->handle, &alsa_format);
        if (status != 0) {
            snd_pcm_close (dev_info->handle);
            return 0;
        }
        
        status = snd_pcm_record_params (dev_info->handle, &rec_params);
        if (status != 0) {
            snd_pcm_close (dev_info->handle);
            return 0;
        }
    }

    return 1;
}

/*
    checks a format (encoding + sample size) to see if it works...
*/
static int
alsa_check_format (psl_device *dev, int size, psl_sample_encoding encoding,
                   int use_max) {

    dev->current             = (use_max) ? dev->max : dev->min;
    dev->current.sample_size = size;
    dev->current.encoding    = encoding;
    
    if (!alsa_open_stream (dev)) return 0;

    alsa_close_stream (dev);
    return 1;
}

/*
    creates a device, and fills in the appropriate minimum and maximum settings.
*/
static psl_device *
alsa_device_new (alsa_card *card, int device_no) {
    psl_device                     *dev;
    alsa_device_info               *device_info;
    int                             status;
    int                             size;
    int                             encoding;

/* allocate memory */
    dev = (psl_device *) malloc (sizeof (psl_device));
    if (dev == NULL) return NULL;
    dev->device_info = (void *) malloc (sizeof (alsa_device_info));
    if (dev->device_info == NULL) {
        free (dev);
        return NULL;
    }
    device_info = (alsa_device_info *) dev->device_info;

/* get info on card from ALSA */
    status = snd_ctl_pcm_playback_info (card->handle, device_no,
                                        &device_info->pcm_play_info);
    if (status != 0) {
        alsa_error (status, card->card_no, device_no);
        free (dev);
        free (dev->device_info);
        return NULL;
    }

    status = snd_ctl_pcm_info (card->handle, device_no,
                               &device_info->pcm_info);
    if (status != 0) {
        alsa_error (status, card->card_no, device_no);
        free (dev);
        free (dev->device_info);
        return NULL;
    }

/* house-keeping stuff */
    dev->name               = strdup (card->card_info.name);
    device_info->card       = card;
    device_info->device_no  = device_no;
    device_info->handle     = NULL;

    pthread_mutex_init (&dev->read_thread_active, NULL);
    pthread_mutex_init (&dev->write_thread_active, NULL);

    dev->ch[0] = (psl_channel *) NULL;      /* configable/playback - left/both*/
    dev->ch[1] = (psl_channel *) NULL;      /* configable/playback - right */
    dev->ch[2] = (psl_channel *) NULL;      /* record - left */
    dev->ch[3] = (psl_channel *) NULL;      /* record - right */

    dev->in_buffer  = NULL;
    dev->out_buffer = NULL;

    if (device_info->pcm_info.flags & SND_PCM_INFO_DUPLEX) {
        dev->min.dir = PSL_DIR_CONFIGABLE;  /* safe assumption: if it can do
                                               both, it can do either */
        dev->max.dir = PSL_DIR_DUPLEX;
    } else {
        if (device_info->pcm_play_info.flags & SND_PCM_INFO_PLAYBACK)
            if (device_info->pcm_play_info.flags & SND_PCM_INFO_RECORD)
                dev->min.dir = PSL_DIR_CONFIGABLE;
            else
                dev->min.dir = PSL_DIR_PLAY;
        else
            dev->min.dir = PSL_DIR_RECORD;

        if (dev->min.dir == PSL_DIR_RECORD) {
            status = snd_ctl_pcm_record_info (card->handle, device_no,
                   (struct snd_pcm_record_info *) &device_info->pcm_play_info);
            if (status != 0) {
                alsa_error (status, card->card_no, device_no);
                return 0;
            }
        }

        dev->max.dir = dev->min.dir;
    }

    dev->min.freq        = device_info->pcm_play_info.min_rate;
    dev->max.freq        = device_info->pcm_play_info.max_rate;

    dev->min.stereo      = (device_info->pcm_play_info.min_channels == 1)
                          ? PSL_MONO : PSL_STEREO;
    dev->max.stereo      = (device_info->pcm_play_info.max_channels == 1)
                          ? PSL_MONO : PSL_STEREO;

    dev->frag_size      = device_info->pcm_play_info.min_fragment_size;
    if (dev->frag_size < MIN_BUF_SIZE) dev->frag_size = MIN_BUF_SIZE;
    dev->frag_count     = 1;

/* find supported formats: have to do this the hard way.  yucky */
    dev->current = dev->min;
    dev->min.sample_size = 0;
    for (size=1; size<=2 && dev->min.sample_size == 0; size++)
        for (encoding=PSL_ENCODING_WORST;
             encoding<=PSL_ENCODING_BEST; encoding++)
            if (alsa_check_format (dev, size, encoding, 0)) {
                dev->min.encoding       = encoding;
                dev->min.sample_size    = size;
                break;
            }
    
    if (dev->min.sample_size == 0) {
        psl_error ("card %d device %d: no supported formats",
                   device_info->card->card_no, device_info->device_no);
        return 0;
    }

    /* this time, keep on going to find max */
    dev->max.encoding    = encoding;
    dev->max.sample_size = size;
    for (size = dev->min.sample_size; size<=2; size++)
        for (encoding=dev->min.encoding;
             encoding<=PSL_ENCODING_BEST; encoding++)
            if (alsa_check_format (dev, size, encoding, 1)) {
                dev->max.encoding    = encoding;
                dev->max.sample_size = size;
            }

    psl_device_add (dev);

    return dev;
}

/*
    probes for all alsa sound cards.  each channel is assigned the number:
    256*card + 4*(device or channel) + (2 if recording, +1 if right split
                                        channel)
    FIXME explain better.

    this is to allow for full duplex cards, that should be split into two
    channels.

    confusing ALSA terminology:
    * a card is a sound-card
    * a device is a (possibly full duplex) channel on a card

    flags are SPLIT_STEREO_IN and SPLIT_STEREO_OUT.  If these are set, L and R
    are treated as separate channels.  They are hacked by the sound driver
    (this code)
*/
void
psl_probe_devices (psl_params *best_params,
                   psl_priority *params_priority, int flags) {
    int         card_map;
    int         card_no;
    int         dev;
    int         status;
    alsa_card  *card;
    psl_device *psl_dev;

    card_map = snd_cards_mask ();
    for (card_no=0; card_no<SND_CARDS; card_no++) {
        if (~card_map & (1<<card_no)) continue;

        card = alsa_card_new (card_no);
        if (card == NULL) continue;

        status = snd_ctl_open (&card->handle, card_no);
        if (status != 0) {
            alsa_error (status, card->card_no, -1);
            free (card);
            continue;
        }

        for (dev=0; dev<card->card_info.pcmdevs; dev++) {
            psl_dev = alsa_device_new (card, dev);
            card->channel_count
                += psl_device_configure (psl_dev, best_params,
                                         params_priority, flags);
        }

        if (card->channel_count == 0) alsa_card_destroy (card);
    }
}

void
alsa_device_destroy (psl_device *dev) {
    psl_device_clean (dev);
    free (dev->device_info);
    free (dev->name);
    free (dev);
}

void
psl_destroy_devices () {
    psl_device *temp;

    if (psl_active) return;

    while (first_device != NULL) {
        temp = first_device;
        first_device = first_device->next;
        alsa_device_destroy (temp);
    }
}

int
psl_device_start (psl_device *dev) {
    memset (dev->in_buffer, 0, psl_buffer_size (&dev->current, dev->frag_size));
    memset (dev->out_buffer, 0, psl_buffer_size(&dev->current, dev->frag_size));

    psl_device_alloc_buffers (dev);

    if (!alsa_open_stream (dev)) return 0;

    pthread_mutex_init (&dev->read_thread_active, NULL);
    pthread_mutex_init (&dev->write_thread_active, NULL);

    pthread_create (&dev->read_thread, NULL, psl_device_read_thread, dev);
    pthread_create (&dev->write_thread, NULL, psl_device_write_thread, dev);
    return 1;
}

int
psl_device_stop (psl_device *dev) {
    int     ch;

    /* if there's a channel sitting waiting to play, flush the buffer */
    for (ch=0; ch<4; ch++)
        if (dev->ch[ch] != NULL)
            if (dev->ch[ch]->active) psl_start_transfer (dev->ch[ch]);

    /* wait for threads to exit (the psl_active flags will tell them...) */
    pthread_mutex_lock (&dev->read_thread_active);
    pthread_mutex_lock (&dev->write_thread_active);

    pthread_mutex_unlock (&dev->read_thread_active);
    pthread_mutex_unlock (&dev->write_thread_active);
 
    psl_device_free_buffers (dev);
 
    return alsa_close_stream (dev);
}

int
psl_device_validate (psl_device *dev) {
    if (!alsa_open_stream (dev)) return 0;
    alsa_close_stream (dev);
    return 1;
}

void
psl_block_read (psl_device *dev, void *buffer) {
    snd_pcm_read (((alsa_device_info *) dev->device_info)->handle, buffer,
                  psl_buffer_size (&dev->current, dev->frag_size));
}

void
psl_block_write (psl_device *dev, void *buffer) {
    snd_pcm_write (((alsa_device_info *) dev->device_info)->handle, buffer,
                   psl_buffer_size (&dev->play_current, dev->frag_size));
}

#endif /* HAVE_ALSA */

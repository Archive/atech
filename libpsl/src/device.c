/*
    Portable Sound Library - a portable interface to sound hardware
    Copyright (C) 1998  Andrew Clausen  <clausen@alphalink.com.au>
        
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.
                        
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.
                                        
    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the
    Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA  02111-1307  USA.
                                                        
    I can be contacted by mail:
    Andrew Clausen
    18 Shaw St
    Ashwood, 3147
    Victoria, Australia
*/

#include <stdio.h>
#include <pthread.h>
#include "psl.h"
#include "convert.h"

static void
psl_signal_complete (psl_channel *ch) {
    pthread_mutex_lock (&ch->transfer_complete_mutex);
    ch->buffer_complete = 1;
    pthread_cond_broadcast (&ch->transfer_complete_cond);
    pthread_mutex_unlock (&ch->transfer_complete_mutex);
}

static void
psl_wait_begin (psl_channel *ch) {
    pthread_mutex_lock (&ch->transfer_begin_mutex);
    while (!ch->buffer_waiting)
        pthread_cond_wait (&ch->transfer_begin_cond, &ch->transfer_begin_mutex);
    ch->buffer_waiting = 0;
    pthread_mutex_unlock (&ch->transfer_begin_mutex);
}

void
psl_device_alloc_buffers (psl_device *dev) {
    int     i;

    for (i=0; i<4; i++)
        if (dev->ch[i] != NULL)
            if (dev->ch[i]->active)
                dev->ch[i]->buffer
                   = (void *) malloc (psl_buffer_size (&dev->ch[i]->conversion,
                                                       dev->frag_size));
}

void
psl_device_free_buffers (psl_device *dev) {
    int     i;

    for (i=0; i<4; i++)
        if (dev->ch[i] != NULL)
            if (dev->ch[i]->active) free (dev->ch[i]->buffer);
}

/*
    the thread that looks after recording.
*/
void
psl_device_read_thread (psl_device *dev) {
    void   *left;
    void   *right;
    void  (*convert_func) (void *, void *, int);
    void  (*split_func) (void *, void *, void *, int);

    if (dev->ch[0] == NULL) return;     /* any recording happening? */

    pthread_mutex_lock (&dev->read_thread_active);

    if (dev->split_stereo_in) {
        if (dev->ch[1] == NULL /* this is a bug */) {
            fprintf (stderr, "psl_device_read_thread: bug code #1\n");
            pthread_mutex_unlock (&dev->read_thread_active);
            return;     
        }
        if (!dev->ch[0]->active && !dev->ch[1]->active) {
            pthread_mutex_unlock (&dev->read_thread_active);
            return;
        }

        if (!dev->ch[0]->active)
            dev->ch[0]->conversion = dev->ch[1]->conversion;
        if (!dev->ch[1]->active)
            dev->ch[1]->conversion = dev->ch[0]->conversion;

        split_func = psl_split_func (&dev->current,
                                     &dev->ch[0]->conversion,
                                     &dev->ch[1]->conversion);

        if (dev->ch[0]->active)
            left = dev->ch[0]->buffer;
        else
            left = NULL;    /* the SPLIT convert functions check for NULL */
        if (dev->ch[1]->active)
            right = dev->ch[1]->buffer;
        else
            right = NULL;

        while (psl_active) {
            if (dev->ch[0]->active) psl_wait_begin (dev->ch[0]);
            if (dev->ch[1]->active) psl_wait_begin (dev->ch[1]);

            psl_block_read (dev, dev->in_buffer);
            split_func (dev->in_buffer, left, right, dev->frag_size);

            if (dev->ch[0]->active) psl_signal_complete (dev->ch[0]);
            if (dev->ch[1]->active) psl_signal_complete (dev->ch[1]);
        }
    } else {
        /* not split stereo */
        if (!dev->ch[0]->active) {
            pthread_mutex_unlock (&dev->read_thread_active);
            return;
        }
        if (dev->current.stereo == PSL_STEREO) {
            if (dev->ch[0]->conversion.stereo == PSL_STEREO)
                convert_func = psl_convert_func (&dev->current,
                                                 &dev->ch[0]->conversion,
                                                 STEREO_STEREO);
            else
                convert_func = psl_convert_func (&dev->current,
                                                 &dev->ch[0]->conversion,
                                                 MONO_STEREO);
        } else {
            /* device doesn't support stereo */
            if (dev->ch[0]->conversion.stereo == PSL_STEREO) {
                fprintf (stderr, "hmmm - this channel isn't using stereo.  "
                                 "Should I emulate? (i.e. [l+r]/2)\n");
                pthread_mutex_unlock (&dev->read_thread_active);
                return;
            }
            convert_func = psl_convert_func (&dev->current,
                                             &dev->ch[0]->conversion,MONO_MONO);
        }

        while (psl_active) {
            psl_wait_begin (dev->ch[0]);

            if (convert_func == NULL) {
                psl_block_read (dev, dev->ch[0]->buffer);
            } else {
                psl_block_read (dev, dev->in_buffer);
                convert_func (dev->in_buffer, dev->ch[0]->buffer,
                              dev->frag_size);
            }

            psl_signal_complete (dev->ch[0]);
        }
    }

    pthread_mutex_unlock (&dev->read_thread_active);
}

/*
    the thread that looks after playback.
*/
void
psl_device_write_thread (psl_device *dev) {
    void   *left;
    void   *right;
    void  (*convert_func) (void *, void *, int);
    void  (*join_func) (void *, void *, void *, int);
    
    if (dev->ch[2] == NULL) return;     /* any playback happening? */

    pthread_mutex_lock (&dev->write_thread_active);

    if (dev->split_stereo_out) {
        if (dev->ch[2] == NULL /* this is a bug */) {
            fprintf (stderr, "psl_device_write_thread: bug code #2\n");
            pthread_mutex_unlock (&dev->write_thread_active);
            return;     
        }
        if (!dev->ch[2]->active && !dev->ch[3]->active) {
            pthread_mutex_unlock (&dev->write_thread_active);
            return;
        }

        if (!dev->ch[2]->active)
            dev->ch[2]->conversion = dev->ch[3]->conversion;
        if (!dev->ch[3]->active)
            dev->ch[3]->conversion = dev->ch[2]->conversion;
        
        join_func = psl_join_func (&dev->ch[2]->conversion,
                                   &dev->ch[3]->conversion,
                                   &dev->current);
        
        if (dev->ch[2]->active)
            left = dev->ch[2]->buffer;
        else
            left = silence;

        if (dev->ch[3]->active)
            right = dev->ch[3]->buffer;
        else
            right = silence;

        while (psl_active) {
            if (dev->ch[2]->active) psl_wait_begin (dev->ch[2]);
            if (dev->ch[3]->active) psl_wait_begin (dev->ch[3]);

            join_func (left, right, dev->out_buffer, dev->frag_size);
            psl_block_write (dev, dev->out_buffer);

            if (dev->ch[2]->active) psl_signal_complete (dev->ch[2]);
            if (dev->ch[3]->active) psl_signal_complete (dev->ch[3]);
        }
    } else {
        /* not split stereo */
        if (!dev->ch[2]->active) {
            pthread_mutex_unlock (&dev->write_thread_active);
            return;
        }
        if (dev->current.stereo == PSL_STEREO) {
            if (dev->ch[2]->conversion.stereo == PSL_STEREO)
                convert_func = psl_convert_func (&dev->ch[2]->conversion,
                                                 &dev->current, STEREO_STEREO);
            else
                convert_func = psl_convert_func (&dev->ch[2]->conversion,
                                                 &dev->current, MONO_STEREO);
        } else {
            /* device doesn't support stereo */
            if (dev->ch[2]->conversion.stereo == PSL_STEREO) {
                fprintf (stderr, "hmmm - this channel isn't using stereo.  "
                                 "Should I emulate? (i.e. [l+r]/2)\n");
                pthread_mutex_unlock (&dev->write_thread_active);
                return;
            }
            convert_func = psl_convert_func (&dev->ch[2]->conversion,
                                             &dev->current, MONO_MONO);
        }

        while (psl_active) {
            psl_wait_begin (dev->ch[2]);

            if (convert_func == NULL) {
                psl_block_write (dev, dev->ch[2]->buffer);
            } else {
                convert_func (dev->ch[2]->buffer, dev->out_buffer,
                              dev->frag_size);
                psl_block_write (dev, dev->out_buffer);
            }

            psl_signal_complete (dev->ch[2]);
        }
    }
    pthread_mutex_unlock (&dev->write_thread_active);
}

void
psl_start_transfer (psl_channel *ch) {
    pthread_mutex_lock (&ch->transfer_begin_mutex);
    if (ch->buffer_waiting) {
        pthread_mutex_unlock (&ch->transfer_begin_mutex);
        return;
    }
 
    ch->buffer_waiting = 1;
    pthread_cond_broadcast (&ch->transfer_begin_cond);
    pthread_mutex_unlock (&ch->transfer_begin_mutex);
}

void
psl_end_transfer (psl_channel *ch) {
    pthread_mutex_lock (&ch->transfer_complete_mutex);
    while (!ch->buffer_complete)
        pthread_cond_wait (&ch->transfer_complete_cond,
                           &ch->transfer_complete_mutex);
    ch->buffer_complete = 0;
    pthread_mutex_unlock (&ch->transfer_complete_mutex);
}

void
psl_complete_transfer (psl_channel *ch) {
    psl_start_transfer (ch);
    psl_end_transfer (ch);
}

static char *
psl_channel_name (char *base, char *append) {
    char       *result;

    result = (char *) malloc (strlen (base) + strlen (append) + 2);
    if (result == NULL) return NULL;

    sprintf (result, "%s %s", base, append);

    return result;
}

/*
    dir = record / play / configable
    side = left or stereo (0) / right (1)   (only used for naming it)
*/
static psl_channel *
psl_channel_new (int channel_no, psl_device *dev, psl_direction dir,
                 int side) {
    psl_channel    *ch;

    ch = psl_channel_alloc ();
    if (ch == NULL) return NULL;

    ch->device      = dev;
    ch->channel_no  = channel_no;

    ch->current     = dev->current;
    ch->current.dir = dir;
    ch->conversion  = ch->current;

    ch->frag_size   = dev->frag_size;
    ch->frag_count  = dev->frag_count;
    ch->active      = 0;

    pthread_mutex_init (&ch->transfer_begin_mutex, NULL);
    pthread_mutex_init (&ch->transfer_complete_mutex, NULL);
    pthread_cond_init (&ch->transfer_begin_cond, NULL);
    pthread_cond_init (&ch->transfer_complete_cond, NULL);
    
    if (dir == PSL_DIR_RECORD) {
        if (dev->split_stereo_in) {
            ch->name = psl_channel_name (dev->name,
                                         side? "right in" : "left in");
            ch->current.stereo = PSL_MONO;
        } else {
            ch->name = psl_channel_name (dev->name, "stereo in");
            ch->current.stereo = PSL_STEREO;
        }
        if (ch->name == NULL) {
            free (ch);
            return NULL;
        }
    }

    if (dir == PSL_DIR_PLAY) {
        if (dev->split_stereo_out) {
            ch->name = psl_channel_name (dev->name,
                                         side? "right out" : "left out");
            ch->current.stereo = PSL_MONO;
        } else {
            ch->name = psl_channel_name (dev->name, "stereo out");
            ch->current.stereo = PSL_STEREO;
        }
        if (ch->name == NULL) {
            free (ch);
            return NULL;
        }
    }

    return ch;
}

/*
    Adds all of the channels for a pcm device, splitting full duplex and
    optionally stereo channels.  Returns number of channels created.
*/
static int
psl_device_add_channels (psl_device *dev) {
    int                 i;
    int                 ch_count;
    int                 base_no;

    base_no = dev->channel_base_no;

    if (dev->split_duplex) {
        /* split into two channels: one RECORD, one PLAY */
        
        if (dev->split_stereo_in) {
            /* split left/right input */
            dev->ch[0] = psl_channel_new (base_no, dev, PSL_DIR_RECORD, 0);
            if (dev->ch[0] == NULL) return 0;
            dev->ch[1] = psl_channel_new (base_no+1, dev, PSL_DIR_RECORD,1);
            if (dev->ch[1] == NULL) return 0;
        } else {
            /* don't split left/right input */
            dev->ch[0] = psl_channel_new (base_no, dev, PSL_DIR_RECORD, 0);
            if (dev->ch[0] == NULL) return 0;
        }

        if (dev->split_stereo_out) {
            /* split left/right output */
            dev->ch[2] = psl_channel_new (base_no+2, dev, PSL_DIR_PLAY, 0);
            if (dev->ch[2] == NULL) return 0;
            dev->ch[3] = psl_channel_new (base_no+3, dev, PSL_DIR_PLAY, 1);
            if (dev->ch[3] == NULL) return 0;
        } else {
            /* don't split left/right output */
            dev->ch[2] = psl_channel_new (base_no+2, dev, PSL_DIR_PLAY, 0);
            if (dev->ch[2] == NULL) return 0;
        }
    } else {
        /* single channel */
        if (dev->split_stereo_in || dev->split_stereo_out) {
            /* split left/right */
            dev->ch[0] = psl_channel_new (base_no, dev,
                                          dev->current.dir, 0);
            if (dev->ch[0] == NULL) return 0;
            dev->ch[1] = psl_channel_new (base_no+1, dev,
                                          dev->current.dir, 1);
            if (dev->ch[1] == NULL) return 0;
        } else {
            /* don't split left/right */
            dev->ch[0] = psl_channel_new (base_no, dev, dev->current.dir, 0);
            if (dev->ch[0] == NULL) return 0;
        }
    }

    ch_count=0;
    for (i=0; i<4; i++) {
        if (dev->ch[i] == NULL) continue;

        ch_count++;
        psl_channel_add (dev->ch[i]);
    }

    return ch_count;
}

/*
    restores a device into the state that psl_device_new left it in.
*/
void
psl_device_clean (psl_device *dev) {
    int     i;
    
    for (i=0; i<4; i++) {
        if (dev->ch[i] != NULL) psl_channel_destroy (dev->ch[i]);
        dev->ch[i] = NULL;
    }

    if (dev->in_buffer != NULL) free (dev->in_buffer);
    if (dev->out_buffer != NULL) free (dev->out_buffer);
}

/*
    configures the device with flags, and returns number of channels in the
    reconfigured (or initially configured) device
*/
int
psl_device_configure (psl_device *dev, psl_params *best_params,
                      psl_priority *params_priority, int flags) {
    int                 priority_level;
    psl_sample_encoding encoding;
    int                 found;

    if (psl_active) return 0;

    psl_device_clean (dev);

/* default everything to minimum */
    dev->current            = dev->min;
    dev->split_duplex       = 0;
    dev->split_stereo_in    = 0;
    dev->split_stereo_out   = 0;

    for (priority_level=0; priority_level<PSL_PRIORITY_COUNT; priority_level++){
        switch (params_priority[priority_level]) {
        case PSL_PREFER_DIR:
            dev->current.dir = best_params->dir;
            if (!psl_device_validate (dev)) {
                dev->current.dir = dev->max.dir;
                if (!psl_device_validate (dev))
                    dev->current.dir = dev->min.dir;
            }
            break;

        case PSL_PREFER_FREQ:
            dev->current.freq = best_params->freq;
            if (!psl_device_validate (dev)) {
                dev->current.freq = dev->max.freq;
                if (!psl_device_validate (dev))
                    dev->current.freq = dev->min.freq;
            }
            break;

        case PSL_PREFER_STEREO:
            dev->current.stereo = best_params->stereo;
            if (!psl_device_validate (dev)) {
                dev->current.stereo = dev->max.stereo;
                if (!psl_device_validate (dev))
                    dev->current.stereo = dev->min.stereo;
            }
            break;

        case PSL_PREFER_SAMPLE_SIZE:
            found = 0;
            for (dev->current.sample_size = best_params->sample_size;
                 dev->current.sample_size >= dev->min.sample_size;
                 dev->current.sample_size--) {

                dev->current.sample_size = best_params->sample_size;

                for (dev->current.encoding = PSL_ENCODING_BEST;
                     dev->current.encoding >= PSL_ENCODING_WORST;
                     dev->current.encoding--)
                    if (psl_device_validate (dev)) {
                        found = 1;
                        break;
                    }

                if (found) break;
            }
            break;
/*
        case PSL_PREFER_ENCODING:
            dev->current.encoding       = best_params->encoding;
            dev->current.sample_size    = best_params->sample_size;
            if (!psl_device_validate (dev)) {
                dev->current.encoding       = dev->max.encoding;
                dev->current.sample_size    = dev->max.sample_size;
                if (!psl_device_validate (dev)) {
                    dev->current.encoding       = dev->min.encoding;
                    dev->current.sample_size    = dev->min.sample_size;
                }
            }
*/
            break;
        }
    }

    if (dev->current.dir == PSL_DIR_DUPLEX) dev->split_duplex = 1;

    if (dev->current.stereo == PSL_STEREO) {
        if (dev->current.dir == PSL_DIR_RECORD || dev->split_duplex)
            if (flags & PSL_SPLIT_STEREO_IN) dev->split_stereo_in = 1;

        if (dev->current.dir == PSL_DIR_PLAY || dev->split_duplex)
            if (flags & PSL_SPLIT_STEREO_OUT) dev->split_stereo_out = 1;
    }

/* allocate buffers */
    dev->in_buffer
        = (void *) malloc (psl_buffer_size (&dev->current, dev->frag_size));
    if (dev->in_buffer == NULL) return 0;
 
    dev->out_buffer
       = (void *) malloc (psl_buffer_size (&dev->current, dev->frag_size));
    if (dev->out_buffer == NULL) return 0;

    return psl_device_add_channels (dev);
}

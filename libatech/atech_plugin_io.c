


gint 
plugin_open_track (PluginLink *link, Track *track, gint flags)
{
  gint fd;

  /* should use at_region stuff.. with locking */

  fd = open (track->filename, flags);
  
  return (fd);
}

plugin_write_track (PluginLink *link, gint fd, at_sample *buf, gint count)
{
  write (fd, buf, count);
}

plugin_read_track (PluginLink *link, gint fd, at_sample *buf, gint count)
{
  read (fd, buf, count);
}



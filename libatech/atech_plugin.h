#ifndef __ATECH_PLUGIN_H__
#define __ATECH_PLUGIN_H__

typedef gint PluginType;

#define PLUGIN_TYPE_REALTIME      (1 << 0)
#define PLUGIN_TYPE_SAVE          (1 << 1)
#define PLUGIN_TYPE_LOAD          (1 << 2)
#define PLUGIN_TYPE_NON_REALTIME  (1 << 3)


typedef struct _PluginLink
{
  gchar *plugin_argv;
  gint plugin_argc;
  PluginType type;
  gchar *menu_location;
  gchar *description;
  IpcProcess *main_module;

} PluginLink;

typedef struct _PluginRegister
{
  PluginType type;
  gchar *menu_location;
  gchar *description;
} PluginRegister;


PluginLink *
plugin_init (gint *argc, gchar ***argv, PluginRegister *reg);



#endif /* __ATECH_PLUGIN_H__ */



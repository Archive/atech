/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __GTK_WAVE_H__
#define __GTK_WAVE_H__


#include <gdk/gdk.h>
#include <gtk/gtkdrawingarea.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_WAVE(obj)        GTK_CHECK_CAST (obj, gtk_wave_get_type (), GtkWave)
#define GTK_WAVE_CLASS(klass) GTK_CHECK_CLASS_CAST (klass, gtk_wave_get_type, GtkWaveClass)
#define GTK_IS_WAVE(obj)     GTK_CHECK_TYPE (obj, gtk_wave_get_type ())

#define MAXIMUM_WAVE_DISPLAYS 10


typedef struct _GtkWave	GtkWave;
typedef struct _GtkWaveClass	GtkWaveClass;

typedef enum
{
  GTK_WAVE_TYPE_POINTS,	/* draw only single points of data */
  GTK_WAVE_TYPE_LINE	/* connect data points via lines */
} GtkWaveType;

struct _GtkWave
{
  GtkDrawingArea graph;

  gulong position;   /* location of vertical bar */
  gint cursor_type;
  gint y_range;
  GdkPixmap *pixmap;
  GtkWaveType type;
  gint grab_point;              /* point currently grabbed */
  gint height;             /* real height of allocation */
  gint width;              /* real width of allocation */
  gint selection_start;
  gint selection_end;
  GtkStyle *colored_style;

  GdkGC *position_gc;
  gint position_gc_set;
  GdkColor *position_color;

  GdkGC *wave_gc[MAXIMUM_WAVE_DISPLAYS];
  gint wave_gc_set[MAXIMUM_WAVE_DISPLAYS];
  GdkColor *wave_gc_color[MAXIMUM_WAVE_DISPLAYS];

  /* set of data points to make up graph */
  gfloat *data_points[MAXIMUM_WAVE_DISPLAYS];
  gulong data_count[MAXIMUM_WAVE_DISPLAYS];
};

struct _GtkWaveClass
{
  GtkDrawingAreaClass parent_class;

  void (* wave_position_changed) (GtkWave *wave,
				  gint position);
  void (* wave_selection_changed) (GtkWave *wave,
				   gint start_selection,
				   gint end_selection);
  void (* wave_button_event) (GtkWave *wave,
			      GdkEventButton *bevent);
			      
};


guint		gtk_wave_get_type	(void);
GtkWidget*	gtk_wave_new		(void);
void            gtk_wave_set_data       (GtkWave *wave, gint wave_number, 
					 gfloat *data, gulong sample_count);
void            gtk_wave_set_display_type   (GtkWave *wave, GtkWaveType type);
void            gtk_wave_set_vertical_range (GtkWave *wave, gint y_range);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_WAVE_H__ */











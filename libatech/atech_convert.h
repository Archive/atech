#ifndef __ATECH_CONVERT_H__
#define __ATECH_CONVERT_H__

#include <gtk/gtk.h>
#include "libatech.h"


/******************************************************************
 Sound Data Conversion Constants
 ******************************************************************/

#define AT_MAX_PCM_32  2147483647
#define AT_MIN_PCM_32 -2147483647
#define AT_MAX_PCM_24  8388607
#define AT_MIN_PCM_24 -8388607
#define AT_MAX_PCM_20  524287
#define AT_MIN_PCM_20 -524287
#define AT_MAX_PCM_16  32767
#define AT_MIN_PCM_16 -32767
#define AT_MAX_PCM_8   127
#define AT_MIN_PCM_8  -127



/******************************************************************
 * 
 * Sound Data Conversion Functions
 * 
 * From Atech Float Format (AFF) to various rates/sample sizes 
 * and vice-versa.
 * 
 ******************************************************************/

typedef gfloat at_sample;

/* This needs to be done properly, but is ok for now. */
#define AT_SAMPLE_SIZE 4

/* LINEAR FORMATS */

at_sample at_convert_32_bit_PCM_to_AFF(gint32 value);
at_sample at_convert_24_bit_PCM_to_AFF(gint32 value);
at_sample at_convert_20_bit_PCM_to_AFF(gint32 value);
at_sample at_convert_16_bit_PCM_to_AFF(gint16 value);
at_sample at_convert_8_bit_PCM_to_AFF(gint8 value);

gint32 at_convert_AFF_to_32_bit_PCM(at_sample value);
gint32 at_convert_AFF_to_24_bit_PCM(at_sample value);
gint32 at_convert_AFF_to_20_bit_PCM(at_sample value);
gint16 at_convert_AFF_to_16_bit_PCM(at_sample value);
gint8 at_convert_AFF_to_8_bit_PCM(at_sample value);

/* LOGARITHMIC FORMATS */

at_sample at_convert_8_bit_MULAW_to_AFF(gint8 value);
at_sample at_convert_8_bit_ALAW_to_AFF(gint8 value);

gint8 at_convert_AFF_to_8_bit_MULAW(at_sample value);
gint8 at_convert_AFF_to_8_bit_ALAW(at_sample value);

#endif /* __ATECH_CONVERT_H__ */


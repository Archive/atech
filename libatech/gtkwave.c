/* GTK - The GIMP Toolkit
 * Copyright (C) 1997 David Mosberger
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "gtkwave.h"
#include <gtk/gtkdrawingarea.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkradiobutton.h>
#include <gtk/gtksignal.h>
#include <gtk/gtktable.h>
#include <gtk/gtkgc.h>

#define BOUNDS(a,x,y)	(((a) < (x)) ? (x) : (((a) > (y)) ? (y) : (a)))

#define WAVE_MASK	(GDK_EXPOSURE_MASK |		\
			 GDK_POINTER_MOTION_MASK |	\
			 GDK_POINTER_MOTION_HINT_MASK |	\
			 GDK_ENTER_NOTIFY_MASK |	\
			 GDK_BUTTON_PRESS_MASK |	\
			 GDK_BUTTON_RELEASE_MASK |	\
			 GDK_BUTTON1_MOTION_MASK)

#define MINIMUM_WAVE_HEIGHT 30
#define MINIMUM_WAVE_WIDTH 30
/* amount of play to give when starting selections vs. moving the position marker */
#define WAVE_SELECTION_PLAY 2


/* Signals */
enum
{
  POSITION_CHANGED,
  SELECTION_CHANGED,
  BUTTON_EVENT,
  LAST_SIGNAL
};

typedef void (*GtkWaveSignal1) (GtkObject * object,
				gint arg1,
				gpointer data);

typedef void (*GtkWaveSignal2) (GtkObject * object,
				gint arg1,
				gint arg2,
				gpointer data);

typedef void (*GtkWaveSignal3) (GtkObject * object,
				GdkEventButton *arg1,
				gpointer data);


/* Signal Marshalers */
static void
gtk_wave_marshal_signal_1 (GtkObject * object,
			    GtkSignalFunc func,
			    gpointer func_data,
			    GtkArg * args);
static void
gtk_wave_marshal_signal_2 (GtkObject * object,
			   GtkSignalFunc func,
			   gpointer func_data,
			   GtkArg * args);

static void
gtk_wave_marshal_signal_3 (GtkObject * object,
			   GtkSignalFunc func,
			   gpointer func_data,
			   GtkArg * args);


static GtkDrawingAreaClass *parent_class = NULL;
static gint wave_signals[LAST_SIGNAL] = {0};

static GdkColor gtk_wave_position_color = { 0, 0xffff, 0x0000, 0x0000 };

/* default colors for waves */
static GdkColor gtk_wave_0_color = { 0, 0x7000, 0x2000, 0x2000 };
static GdkColor gtk_wave_1_color = { 0, 0x2000, 0x7000, 0x2000 };
static GdkColor gtk_wave_2_color = { 0, 0x7000, 0x7000, 0x2000 };
static GdkColor gtk_wave_3_color = { 0, 0x2000, 0x7000, 0x7000 };
static GdkColor gtk_wave_4_color = { 0, 0x7000, 0x2000, 0x7000 };
static GdkColor gtk_wave_5_color = { 0, 0x0000, 0x0000, 0x0000 };
static GdkColor gtk_wave_6_color = { 0, 0x0000, 0x0000, 0x0000 };
static GdkColor gtk_wave_7_color = { 0, 0x0000, 0x0000, 0x0000 };
static GdkColor gtk_wave_8_color = { 0, 0x0000, 0x0000, 0x0000 };
static GdkColor gtk_wave_9_color = { 0, 0x0000, 0x0000, 0x0000 };


static void gtk_wave_class_init (GtkWaveClass *class);
static void gtk_wave_init (GtkWave *wave);
static void gtk_wave_finalize (GtkObject *object);
static void gtk_wave_realize (GtkWidget *widget);


guint
gtk_wave_get_type (void)
{
  static guint wave_type = 0;

  if (!wave_type) {
    GtkTypeInfo wave_info = {
      "GtkWave",
      sizeof (GtkWave),
      sizeof (GtkWaveClass),
      (GtkClassInitFunc) gtk_wave_class_init,
      (GtkObjectInitFunc) gtk_wave_init,
    };
    
    wave_type = gtk_type_unique (gtk_drawing_area_get_type (), &wave_info);
  }
  return wave_type;
}

static void
gtk_wave_class_init (GtkWaveClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  widget_class = (GtkWidgetClass*) class;
  parent_class = gtk_type_class (gtk_drawing_area_get_type ());
  object_class = (GtkObjectClass *) class;

  wave_signals[POSITION_CHANGED] =
    gtk_signal_new ("position_changed", GTK_RUN_FIRST, object_class->type,
		    GTK_SIGNAL_OFFSET (GtkWaveClass, wave_position_changed),
		    gtk_wave_marshal_signal_1, GTK_TYPE_NONE, 1, GTK_TYPE_INT);

  wave_signals[SELECTION_CHANGED] =
    gtk_signal_new ("selection_changed", GTK_RUN_FIRST, object_class->type,
		    GTK_SIGNAL_OFFSET (GtkWaveClass, wave_selection_changed),
		    gtk_wave_marshal_signal_2, GTK_TYPE_NONE, 2, GTK_TYPE_INT,
		    GTK_TYPE_INT);

  wave_signals[BUTTON_EVENT] =
    gtk_signal_new ("button_event", GTK_RUN_FIRST, object_class->type,
		    GTK_SIGNAL_OFFSET (GtkWaveClass, wave_button_event),
		    gtk_wave_marshal_signal_3, GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

  gtk_object_class_add_signals (object_class, wave_signals, LAST_SIGNAL);

  object_class->finalize = gtk_wave_finalize;
  widget_class->realize = gtk_wave_realize;
}

static void
gtk_wave_init (GtkWave *wave)
{
  gint i;

  wave->cursor_type = GDK_TOP_LEFT_ARROW;
  wave->pixmap = NULL;
  wave->type = GTK_WAVE_TYPE_LINE;
  wave->height = 0;
  wave->y_range = 4;
  
  for (i = 0; i < MAXIMUM_WAVE_DISPLAYS; i++) {
    wave->data_points[i] = NULL;
    wave->data_count[i] = 0;
  }

  wave->position = 1;
  wave->selection_start = -1;
  wave->selection_end = -1;
  wave->grab_point = -1;
  
  /* deal with gc's */
  wave->position_gc = NULL;
  wave->position_gc_set = TRUE;
  wave->position_color = &gtk_wave_position_color;

  wave->wave_gc_color[0] = &gtk_wave_0_color;
  wave->wave_gc_set[0] = TRUE;
  wave->wave_gc_color[1] = &gtk_wave_1_color;
  wave->wave_gc_set[1] = TRUE;
  wave->wave_gc_color[2] = &gtk_wave_2_color;
  wave->wave_gc_set[2] = TRUE;
  wave->wave_gc_color[3] = &gtk_wave_3_color;
  wave->wave_gc_set[3] = TRUE;
  wave->wave_gc_color[4] = &gtk_wave_4_color;
  wave->wave_gc_set[4] = TRUE;
  wave->wave_gc_color[5] = &gtk_wave_5_color;
  wave->wave_gc_set[5] = TRUE;
  wave->wave_gc_color[6] = &gtk_wave_6_color;
  wave->wave_gc_set[6] = TRUE;
  wave->wave_gc_color[7] = &gtk_wave_7_color;
  wave->wave_gc_set[7] = TRUE;
  wave->wave_gc_color[8] = &gtk_wave_8_color;
  wave->wave_gc_set[8] = TRUE;
  wave->wave_gc_color[9] = &gtk_wave_9_color;
  wave->wave_gc_set[9] = TRUE;
  
}


static void
gtk_wave_marshal_signal_1 (GtkObject * object,
			    GtkSignalFunc func,
			    gpointer func_data,
			    GtkArg * args)
{
  GtkWaveSignal1 rfunc;

  rfunc = (GtkWaveSignal1) func;

  (*rfunc) (object, GTK_VALUE_INT (args[0]), func_data);
}

static void
gtk_wave_marshal_signal_2 (GtkObject * object,
			    GtkSignalFunc func,
			    gpointer func_data,
			    GtkArg * args)
{
  GtkWaveSignal2 rfunc;

  rfunc = (GtkWaveSignal2) func;

  (*rfunc) (object, GTK_VALUE_INT (args[0]), 
	    GTK_VALUE_INT (args[1]), func_data);
}

static void
gtk_wave_marshal_signal_3 (GtkObject * object,
			    GtkSignalFunc func,
			    gpointer func_data,
			    GtkArg * args)
{
  GtkWaveSignal3 rfunc;

  rfunc = (GtkWaveSignal3) func;

  (*rfunc) (object, GTK_VALUE_POINTER (args[0]), func_data);
}

static void
gtk_wave_set_gc_fg (GtkWidget *widget, GdkGC *gc, GdkColor *color)
{
  GdkColorContext *cc;
  gint n;
  
  cc = gdk_color_context_new (gtk_widget_get_visual (widget),
			      gtk_widget_get_colormap (widget));
  
  n = 0;

  gdk_color_context_get_pixels (cc, &color->red, &color->green, &color->blue,
				1, &color->pixel, &n);
  gdk_gc_set_foreground (gc, color);
}

static void
gtk_wave_draw (GtkWave *wave)
{
  GtkStateType state;
  GtkStyle *style;
  gint i, wave_num;
  gint height, width;
  gint last_x, last_y;
  GdkGC *gc;
  gint sel_start, sel_end;
  
  if (!wave->pixmap || !GTK_WIDGET(wave)->window)
    return;

  style = GTK_WIDGET (wave)->style;

  height = wave->height;
  width = wave->width;
  
  state = GTK_STATE_NORMAL;
  if (!GTK_WIDGET_IS_SENSITIVE (GTK_WIDGET (wave)))
    state = GTK_STATE_INSENSITIVE;

  /* check if gc has changed */
  if (wave->position_gc_set) {
    gtk_wave_set_gc_fg (GTK_WIDGET (wave), 
			wave->position_gc, wave->position_color);
    wave->position_gc_set = FALSE;
  }
  
  /* check wave gc's */
  for (i=0; i < MAXIMUM_WAVE_DISPLAYS; i++) {
    if ((wave->wave_gc_set[i]) && (wave->data_count[i] > 0)) {
      gtk_wave_set_gc_fg (GTK_WIDGET (wave),
			  wave->wave_gc[i], wave->wave_gc_color[i]);
      wave->wave_gc_set[i] = FALSE;
    }
  }

  /* clear the pixmap: */
  gdk_draw_rectangle (wave->pixmap, style->bg_gc[state], TRUE,
		      0, 0, width, height);

  if (wave->selection_start > wave->selection_end) {
    sel_start = wave->selection_end;
    sel_end = wave->selection_start;
  } else {
    sel_start = wave->selection_start;
    sel_end = wave->selection_end;
  }

  /* draw selection first */
  if (wave->selection_start != -1) {
    gdk_draw_rectangle (wave->pixmap, style->bg_gc[GTK_STATE_SELECTED], TRUE,
			sel_start, 0, sel_end - sel_start, height);
  }

  /* draw the grid lines */
  for (i = 0; i <= wave->y_range + 1; i++) {
    gint y;
    
    y = (height / (wave->y_range)) * i;
    
    gdk_draw_line (wave->pixmap, style->dark_gc[GTK_STATE_NORMAL],
		   0, y, width, y);
  }

  /* draw the vertical position line */
  gdk_draw_line (wave->pixmap, wave->position_gc, wave->position, 
		   0, wave->position, height);
    
  for (wave_num = 0; wave_num < MAXIMUM_WAVE_DISPLAYS; wave_num++) {
    
    if (wave->data_count[wave_num] <= 0)
      continue;

    last_x = 0;
    last_y = wave->height / 2;
    
    for (i = 0; i <= wave->data_count[wave_num]; i++) {
      gint x, y;
      
      x = i;
      y = (wave->data_points[wave_num][i] + wave->y_range / 2) * (height / wave->y_range);
      
      gc = style->fg_gc[state];
            
      if (wave->type == GTK_WAVE_TYPE_LINE) {
	gdk_draw_line (wave->pixmap, wave->wave_gc[wave_num],
		       last_x, last_y, x, y);
      }
      
      if (wave->type == GTK_WAVE_TYPE_POINTS) {
	gdk_draw_point (wave->pixmap, wave->wave_gc[wave_num], 
			x, y);
      }
      
      last_x = x;
      last_y = y;
      
    }
  }

  gdk_draw_pixmap (GTK_WIDGET (wave)->window, style->fg_gc[state], wave->pixmap,
		   0, 0, 0, 0, width, height);
}
 
static gint
gtk_wave_graph_events (GtkWidget *widget, GdkEvent *event, GtkWave *wave)
{
  GdkCursorType new_type = wave->cursor_type;
  GdkEventButton *bevent;
  GdkEventMotion *mevent;
  GtkWidget *w;
  gint tx, ty;
  gint x, y, width, height;
  
  w = GTK_WIDGET (wave);
  wave->width = width = w->allocation.width;

  if (w->allocation.height < 1000) {
    wave->height = height = w->allocation.height;
  } else {
    height = wave->height = w->requisition.height;
  }

  if (width < MINIMUM_WAVE_WIDTH)
    width = wave->width = w->requisition.width;


  /*  get the pointer position  */
  gdk_window_get_pointer (w->window, &tx, &ty, NULL);
  x = BOUNDS ((tx), 0, width);
  y = BOUNDS ((ty), 0, height);

  switch (event->type)
    {
    case GDK_CONFIGURE:
      if (wave->pixmap)
	gdk_pixmap_unref (wave->pixmap);
      wave->pixmap = NULL;
      /* fall through */
    case GDK_EXPOSE:
      if (!wave->pixmap) {
	wave->pixmap = gdk_pixmap_new (w->window,
				       width, height, -1);
      }

      gtk_wave_draw (wave);
      break;

    case GDK_BUTTON_PRESS:

      gtk_grab_add (widget);

      bevent = (GdkEventButton *) event;
      new_type = GDK_TCROSS;

      gtk_signal_emit (GTK_OBJECT (wave), wave_signals[BUTTON_EVENT], bevent);
      
      /* if it's the first mouse button, start a grab */
      if (bevent->button == 1) 
	wave->grab_point = x;
      
      gtk_wave_draw (wave);
      break;

    case GDK_BUTTON_RELEASE:
      
      bevent = (GdkEventButton *) event;

      gtk_signal_emit (GTK_OBJECT (wave), wave_signals[BUTTON_EVENT], bevent);

      /* if it's the first mouse button  */

      if (bevent->button == 1) {
	
	if ( (x <= wave->grab_point + WAVE_SELECTION_PLAY) && 
	     (x >= wave->grab_point - WAVE_SELECTION_PLAY)) {
	  /* just moved the position marker */
	  wave->position = x;
	  gtk_signal_emit (GTK_OBJECT (wave), wave_signals[POSITION_CHANGED], 
			   wave->position);
	  
	  if (wave->selection_start != -1) {
	    /* there is now no selection where there used to be.  Give notification */
	    gtk_signal_emit (GTK_OBJECT (wave), wave_signals[SELECTION_CHANGED], 
			     -1, -1);
	  }
	  
	  wave->selection_start = -1;
	  wave->selection_end = -1;
	
	} else {
	  /* selection has been made */
	  if (wave->selection_start != -1) {
	    wave->selection_start = wave->grab_point;
	    wave->selection_end = x;
	    gtk_signal_emit (GTK_OBJECT (wave), wave_signals[SELECTION_CHANGED], 
			     wave->selection_start, wave->selection_end);
	  }
	}
	gtk_wave_draw (wave);

      }
      new_type = GDK_FLEUR;
      wave->grab_point = -1;

      gtk_grab_remove (widget);

      break;

    case GDK_MOTION_NOTIFY:
      mevent = (GdkEventMotion *) event;
      
      /* check for an ongoing selection */
      if (wave->grab_point != -1) {
	if ( (x >= wave->grab_point + WAVE_SELECTION_PLAY) || 
	     (x <= wave->grab_point - WAVE_SELECTION_PLAY)) {
	  wave->selection_start = wave->grab_point;
	  wave->selection_end = x;
	  gtk_wave_draw (wave);
	}
      }

      break;

    default:
      break;
    }
  
  return FALSE;
}

static void
gtk_wave_size_graph (GtkWave *wave)
{
  gint width = 0;
  gint height;
  gint i;

  /* determine length of longest wave */
  for (i = 0; i < MAXIMUM_WAVE_DISPLAYS; i++)
    if (wave->data_count[i] > width)
      width = wave->data_count[i];


  if (width < MINIMUM_WAVE_WIDTH)
    width = MINIMUM_WAVE_WIDTH;
  
  wave->width = width;

  if (wave->height)
    height = wave->height;
  else
    height = wave->height = MINIMUM_WAVE_HEIGHT;
  
  gtk_drawing_area_size (GTK_DRAWING_AREA (wave),
			 width, height);
  
  gtk_widget_set_usize (GTK_WIDGET (wave), width, height);

}


void
gtk_wave_set_vertical_range (GtkWave *wave, gint y_range)
{
  wave->y_range = y_range * 2;
  gtk_wave_draw (wave);
}

void gtk_wave_set_data (GtkWave *wave, gint wave_number, 
			gfloat *data, gulong sample_count)
{
  wave->data_points[wave_number] = data;
  wave->data_count[wave_number] = sample_count;

  gtk_wave_size_graph (wave);
  gtk_wave_draw (wave);
}

void
gtk_wave_set_display_type (GtkWave *wave, GtkWaveType type)
{
  wave->type = type;
  gtk_wave_draw (wave);
}

GtkWidget*
gtk_wave_new (void)
{
  GtkWave *wave;
  gint old_mask;

  
  wave = gtk_type_new (gtk_wave_get_type ());

  old_mask = gtk_widget_get_events (GTK_WIDGET (wave));
  gtk_widget_set_events (GTK_WIDGET (wave), old_mask | WAVE_MASK);
  gtk_signal_connect (GTK_OBJECT (wave), "event",
		      (GtkSignalFunc) gtk_wave_graph_events, wave);

  return GTK_WIDGET (wave);
}

static void
gtk_wave_realize (GtkWidget *widget)
{
  GdkWindowAttr attributes;
  GtkWave *wave;
  gint attributes_mask;
  gint i;

  
  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_WAVE (widget));

  wave = GTK_WAVE (widget);
  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);
  attributes.event_mask = gtk_widget_get_events (widget);

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

  widget->window = gdk_window_new (gtk_widget_get_parent_window (widget), 
				   &attributes, attributes_mask);
  gdk_window_set_user_data (widget->window, wave);

  widget->style = gtk_style_attach (widget->style, widget->window);
  gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL);

  wave->position_gc = gdk_gc_new (widget->window);

  for (i=0; i < MAXIMUM_WAVE_DISPLAYS; i++)
    wave->wave_gc[i] = gdk_gc_new (widget->window);

  /* works better doing the resize here */
  gtk_wave_size_graph (wave);
}

static void
gtk_wave_finalize (GtkObject *object)
{
  GtkWave *wave;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GTK_IS_WAVE (object));

  wave = GTK_WAVE (object);
  if (wave->pixmap)
    gdk_pixmap_unref (wave->pixmap);

  (*GTK_OBJECT_CLASS (parent_class)->finalize) (object);
}














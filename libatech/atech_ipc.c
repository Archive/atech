#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <signal.h>
#include <stdarg.h>

#include <gtk/gtk.h>

#include "atech_ipc.h"
#include "atech_misc.h"

#define MAX_CHILDREN 256
#define MAX_HANDLERS 256

typedef struct
{
    gint tag;
    gchar *name;
    IpcSignalFunc function;
    void *data;
    IpcProcess *ipcp;
} IpcSignal;

static IpcSignal *handler[MAX_HANDLERS];
    
static void at_ipc_signal_dispatch (gpointer data, gint source,
				   GdkInputCondition condition);
static void at_ipc_input_add (IpcProcess *ipcp);

/********************************************************************
 * fork() with pipes.
 ********************************************************************
 */


/* fork a new process and return a newly allocated IpcProcess structure.
 * Pipes are setup for communication, and the function passed as the
 * sole argument is called in the child process.  */

IpcProcess* at_ipc_fork(void (*start_routine)(IpcProcess *))
{
    int p0[2];
    int p1[2];
    int pid;
    IpcProcess *ipcp;
    
    ipcp = g_malloc (sizeof(IpcProcess));
    
    pipe(p0);
    pipe(p1);
    
        
    pid = fork();
    
    if (pid < 0) {
	g_error("fork: %s\n", g_strerror(errno));
	return (NULL);
    }
    
    if (pid == 0) {
	ipcp->inpipe = p0[0];
	ipcp->outpipe = p1[1];
	ipcp->pid = getppid();
	ipcp->tag = -1;
	ipcp->active = TRUE;
	
	start_routine(ipcp);
	exit(0);
    }

    /* parent process */
    ipcp->pid = pid;
    
    ipcp->inpipe = p1[0];
    ipcp->outpipe = p0[1];
    ipcp->tag = -1;
    ipcp->active = TRUE;
    
    return (ipcp);
}

/* fork and exec a new process specifed as the sole argument.  The 
 * new process will have to call ipc_init() to set up the pipes etc */
IpcProcess* at_ipc_fork_exec(const char *file)
{
    IpcProcess *ipcp;
    int p0[2];
    int p1[2];
    int pid;
    char *argv[3];
    char buf[5];
    ipcp = g_malloc (sizeof(IpcProcess));
    
    pipe(p0);
    pipe(p1);
    
        
    pid = fork();
    
    if (pid < 0) {
	g_error ("fork: %s\n", g_strerror(errno));
    }
    
    if (pid == 0) {
	
	/* argv is used to pass in the pipe file descriptor
	 * numbers */
	argv[0] = g_strdup(file);
	
	sprintf(buf, "%d", p0[0]);
	argv[1] = g_strdup(buf);
	
	sprintf(buf, "%d", p1[1]);
	argv[2] = g_strdup(buf);
	
	argv[3] = NULL;
	
	/* close off unused ends */
	close (p1[0]);
	close (p0[1]);
	
	if ( (execvp(file, argv)) == -1)
		g_error("execvp: %s\n", g_strerror(errno));
	_exit(0);
    }

    /* parent process */
    ipcp->pid = pid;
    
    ipcp->inpipe = p1[0];
    ipcp->outpipe = p0[1];
    
    ipcp->tag = -1;
    ipcp->active = TRUE;
    
    close (p1[1]);
    close (p0[0]);
    
    return (ipcp);
}

/* called by a new process created with ipc_fork_exec().  This sets
 * up the pipes from the calling process for read and write.  The 
 * arguments are argc and argv as comes from main() */

IpcProcess* at_ipc_init(int argc, char *argv[])
{    
    IpcProcess *ipcp;
    
    ipcp = g_malloc (sizeof(IpcProcess));
    
    ipcp->inpipe = atoi(argv[1]);
    ipcp->outpipe = atoi(argv[2]);

    ipcp->pid = getppid();
    
    ipcp->tag = -1;
    ipcp->active = TRUE;
    
    return(ipcp);
}

/* close off the file descriptors, and remove the gdk_input_add. */

void at_ipc_close (IpcProcess *ipcp)
{
    if (ipcp->active == FALSE)
	    return;
    
    close (ipcp->inpipe);
    close (ipcp->outpipe);
    
    if (ipcp->tag != -1)
	    gdk_input_remove (ipcp->tag);
    
    ipcp->active = FALSE;
    
}

/********************************************************************
 * This begins the section on sending and recieving primitaves
 ********************************************************************
 */

/* sends a string to the specified process.  Automatically calculates
 * length.  Must be received with at_ipc_recv_string (). */
int at_ipc_send_string (IpcProcess *ipcp, char const *string)
{
    int len;
    
    if (ipcp->active == FALSE)
	    return (-1);
    
    len = strlen(string) + 1;

    if (write(ipcp->outpipe, &len, sizeof(int)) < 0)
	    g_error ("Writing to pipe: %s", g_strerror(errno));
    
    if (write(ipcp->outpipe, string, len) < 0)
	    g_error ("Writing to pipe: %s", g_strerror(errno));

    return(0);
}

/* used to receive a string form the specifed process.. memory is allocated
 * and the string is returned. */
gchar* at_ipc_recv_string (IpcProcess *ipcp)
{
    int len;
    char *string;

    if (read (ipcp->inpipe, &len, sizeof(int)) < 0)
	    g_error("Reading from pipe: %s", g_strerror(errno));
    
    string = g_malloc (len + 1);
    
    if (read (ipcp->inpipe, string, len) <0)
	    g_error("Reading from pipe: %s", g_strerror(errno));
    
    string[len] = '\0';
    
    return(string);
}

/* sends a message of size 'size' at_ipc_recv_msg () or
 * at_ipc_recv_msg_p () . */
int at_ipc_send_msg (IpcProcess *ipcp, void const *msg, gint size)
{
    if (write(ipcp->outpipe, &size, sizeof(int)) < 0)
	    g_error("Writing to pipe: %s", g_strerror(errno));
    
    if (write(ipcp->outpipe, msg, size) < 0)
	    g_error("Writing to pipe: %s", g_strerror(errno));
    
    return(0);
}

/* used to receive a msg from the specifed process.. memory is allocated
 * and the msg is returned. */
void *at_ipc_recv_msg_p (IpcProcess *ipcp)
{
    int size;
    char *msg;
    
    if (read (ipcp->inpipe, &size, sizeof(int)) < 0)
	    g_error("Reading from pipe: %s", g_strerror(errno));
    
    msg = g_malloc (size + 1);
    
    if (read (ipcp->inpipe, msg, size) <0)
	    g_error("Reading from pipe: %s", g_strerror(errno));
    
    return(msg);
}

/* used to receive a msg from the specifed process.. Message is copied
 * into the 'msg' arg.  You must have enough memory allocated for the
 * incoming object.  No memory is allocated. */
void at_ipc_recv_msg (IpcProcess *ipcp, void *msg)
{
    int size;
    
    if (read (ipcp->inpipe, &size, sizeof(int)) < 0)
	    g_error("Reading from pipe: %s", g_strerror(errno));
    
    if (read (ipcp->inpipe, msg, size) <0)
	    g_error("Reading from pipe: %s", g_strerror(errno));
}

gint at_ipc_signal_connect (IpcProcess *ipcp,
                            const gchar *name,
                            IpcSignalFunc func,
                            gpointer func_data)
{
    gint sig_num = 0;
    
    if (ipcp->active == FALSE)
	    return(-1);
    
    at_ipc_input_add (ipcp);
    
    /* find first free slot */
    for (sig_num=0;;sig_num++) {
	if (handler[sig_num] == NULL)
		break;
    }
    
    handler[sig_num] = g_malloc (sizeof (IpcSignal));
    
    g_print ("connecting signal '%s' as sig number %d\n",
	     name, sig_num);
    
    handler[sig_num]->name = g_strdup (name);
    handler[sig_num]->function = func;
    handler[sig_num]->data = func_data;
    handler[sig_num]->ipcp = ipcp;
    
    return (sig_num);
}

/* check if the process is already monitored by GDK, if not,
 * add it in. */
static void at_ipc_input_add (IpcProcess *ipcp)
{
    if (ipcp->tag == -1) {
	gdk_input_add (ipcp->inpipe, GDK_INPUT_READ,
		       at_ipc_signal_dispatch, ipcp);
    }
}

void at_ipc_signal_destroy (IpcProcess *ipcp)
{
    if (ipcp->active == FALSE)
	    return;
    
    at_ipc_signal_emit (ipcp, "destroy");
    at_ipc_close (ipcp);
}

/* when data arrives on the pipe, this function gets called.  It then
 * parses the info, and dispatches the appropriate signal handler */

static void at_ipc_signal_dispatch (gpointer data, gint source,
				    GdkInputCondition condition)
{
    gint i = 0;
    gchar *sig_name;
    IpcProcess *ipcp;
    
    ipcp = data;
    
    sig_name = at_ipc_recv_string (ipcp);
    
    for (i=0; handler != NULL; i++) {
	if (!strcmp (handler[i]->name, sig_name)) {
	    g_free (sig_name);
	    (* handler[i]->function) (ipcp, handler[i]->data);
	    return;
	}
    }
    
    g_free (sig_name);
}
    
/* breaks down, and sends a 'command' - more like a function to the
 * given process.  Must be received with at_ipc_recv_command, and
 * then, at_ipc_recv_args (if needed). */

void at_ipc_signal_emit (IpcProcess *ipcp, gchar *command, ...)
{
    va_list ap;
    gchar *command_str;
    gint i;
    gint len;
    
    double floater;
    gfloat real_float;
    gint integer;
    void *pointer;
    gchar *string;

    if (ipcp->active == FALSE)
	    return;
        
    /* get the 'function' name */
    len = strlen(command);
    for (i=0; (i <= len) && ( command[i] != '(' ); i++) {}
    
    command_str = g_malloc (i + 1);
    strncpy (command_str, command, i);
    command_str[i] = '\0';
    remove_whitespace (command_str);

    at_ipc_send_string(ipcp, command_str);

    
    /* if no '(' was found, return here and skip parsing the rest. */
    if (i >= len)
	    return;
    
    command += i;
    
    va_start (ap, command);

    len = strlen(command);
    
    while (*command) {
	switch (*command) {
	case 's':

	    if (!strncmp(command, "string", strlen("string"))) {
		string = va_arg (ap, char *);
		at_ipc_send_string(ipcp, string);
		
		command += strlen("string") - 1;
		break;
	    }

	case 'p':
	    
	    if (!strncmp(command, "pointer", strlen("pointer"))) {
		pointer = va_arg (ap, char *);
		integer = va_arg (ap, gint);

		at_ipc_send_msg(ipcp, pointer, integer);

		command += strlen("pointer") - 1;
		break;
		
	    }

	case 'i':
	    
	    if (!strncmp(command, "integer", strlen("integer"))) {
	        integer = va_arg (ap, gint);
		at_ipc_send_msg(ipcp, &integer, sizeof(gint));
		command += strlen("integer") - 1;
	    }

	case 'f':
	    
	    /* for va_args, all types are promoted, so a float becomes
	       a double when called. */
	    if (!strncmp(command, "float", strlen("float"))) {
	        floater = va_arg (ap, double);
		real_float = floater;
		at_ipc_send_msg(ipcp, &real_float, sizeof(gfloat));
		
		command += strlen("float") - 1;
	    }
	    break;
	    	
	default:
	}
    
	command++;
    }
    
    va_end(ap);
}

void at_ipc_signal_recv_args (IpcProcess *ipcp, gchar *arg_string, ...)
{
    va_list ap;
    gint alloc = FALSE;
    
    gfloat *floater;
    gint *integer;
    void *pointer;
    void **alloc_pointer;
    gchar *string;
    gchar **alloc_string;
    
    va_start (ap, arg_string);

    while (*arg_string) {
	switch (*arg_string) {

	case 'a':
	    if (!strncmp(arg_string, "alloc", strlen("alloc"))) {
		alloc = TRUE;
		arg_string += strlen("alloc") - 1;
		break;
	    }
	    
	case 's':
	    if (!strncmp(arg_string, "string", strlen("string"))) {
		if (alloc) {
		    alloc_string = va_arg (ap, char **);
		    *alloc_string = at_ipc_recv_string(ipcp);
		    alloc = FALSE;
		} else {
		    string = va_arg (ap, char *);
		    at_ipc_recv_msg(ipcp, string);
		}
		arg_string += strlen("string") - 1;
		break;
	    }

	case 'p':
	    if (!strncmp(arg_string, "pointer", strlen("pointer"))) {
		if (alloc) {
		    alloc_pointer = va_arg (ap, char **);
		    *alloc_pointer = at_ipc_recv_msg_p(ipcp);
		    alloc = FALSE;
		} else {
		    pointer = va_arg (ap, char *);
		    at_ipc_recv_msg(ipcp, pointer);
		}
		arg_string += strlen("pointer") - 1;
		break;
	    }

	case 'i':
	    if (!strncmp(arg_string, "integer", strlen("integer"))) {
	        integer = va_arg (ap, gint *);
		at_ipc_recv_msg(ipcp, integer);
		arg_string += strlen("integer") - 1;
		break;
	    }

	case 'f':
	    if (!strncmp(arg_string, "float", strlen("float"))) {
	        floater = va_arg (ap, gfloat *);
		at_ipc_recv_msg(ipcp, floater);
		arg_string += strlen("float") - 1;
		break;
	    }

	default:
	}
	
    arg_string++;
    }
    
    va_end(ap);
}



#include <stdlib.h>

#include <glib.h>

#include "atech_misc.h"

gint match (gchar *string1, gchar *string2)
{
    return (!strcmp (string1, string2));
}

gchar *
atech_username(void)
{
    static char *username;
    static int init = 1;
    
    if (init) {
	username = getenv("USER");
	init = 0;
    }
    
    return(username);
}

/* strips any whitespace (tabs or spaces or '\n') from 'string' */
	
gchar *
remove_whitespace(gchar *string)
{
    gint string_len;
    gint i, a;
    
    string_len = strlen(string);
    
    for (i=0; i < string_len; i++) {
	if(whitespace(string[i])) {
	    for (a=i; a < string_len; a++) {
		string[a] = string[a + 1];
	    }
	    string_len--;
	    i--;
	}
    }
    
    string[string_len] = '\0';
    
    return(string);
}

gint
whitespace(gchar character)
{
    if ( (character == ' ') || (character == '\t') || (character == '\n') )
	    return (TRUE);
    else
	    return (FALSE);
}

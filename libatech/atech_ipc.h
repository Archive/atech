#ifndef __ATECH_IPC_H__
#define __ATECH_IPC_H__

typedef struct _IpcProcess
{
    gint inpipe;
    gint outpipe;
    gint pid;    /* process you're communicating with */
    gint tag;    /* tag for gdk_idle_add () - initialized to -1 */
    gint active; /* if the connection is active - boolean */

} IpcProcess;

typedef void (*IpcSignalFunc) (IpcProcess *ipcp, gpointer data);

/**************************************************
 * 
 * Signal API - This is the one I intend for use.
 *
 * ***********************************************/
	     
IpcProcess* at_ipc_init(int argc, char *argv[]);

gint at_ipc_signal_connect (IpcProcess *ipcp,
                            const gchar *name,
                            IpcSignalFunc func,
                            gpointer func_data);

void at_ipc_signal_emit (IpcProcess *ipcp, gchar *sig_name, ...);

void at_ipc_signal_recv_args (IpcProcess *ipcp, gchar *arg_string, ...);

/* send the 'destroy' signal to the process, and close off all 
 * file descriptors to it. */
void at_ipc_signal_destroy (IpcProcess *ipcp);

/* Process creation and destruction functions */
IpcProcess* at_ipc_fork(void (*start_routine)(IpcProcess *));
IpcProcess* at_ipc_fork_exec(const char *file);
gint at_ipc_kill (IpcProcess *ipcp);


/* anything below here is not recommended, but is usable
 * so long as you know what you're doing */

/* sending and receiving messages */
int at_ipc_send_string (IpcProcess *ipcp, char const *string);
char *at_ipc_recv_string (IpcProcess *ipcp);
int at_ipc_send_msg (IpcProcess *ipcp, void const *msg, gint size);
void at_ipc_recv_msg (IpcProcess *ipcp, void *msg);
void *at_ipc_recv_msg_p (IpcProcess *ipcp);
void at_ipc_close (IpcProcess *ipcp);

#endif /* __ATECH_IPC_H__ */

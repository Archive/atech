#ifndef __ATECH_PLUGIN_IO_H__
#define __ATECH_PLUGIN_IO_H__

gint 
plugin_open_track (PluginLink *link, Track *track, gint flags);

plugin_write_track (PluginLink *link, gint fd, at_sample *buf, gint count);

plugin_read_track (PluginLink *link, gint fd, at_sample *buf, gint count);

#endif /* __ATECH_PLUGIN_IO_H__ */

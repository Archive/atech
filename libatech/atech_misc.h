#ifndef __ATECH_MISC_H__
#define __ATECH_MISC_H__

gchar *
atech_username(void);

gchar *
remove_whitespace(gchar *string);

gint
whitespace(gchar character);

gint match (gchar *string1, gchar *string2);

#endif /* __ATECH_MISC_H__ */

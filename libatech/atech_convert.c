#include <stdlib.h>
#include <glib.h>

#include "atech_convert.h"

/******************************************************************
 Sound Data Conversion Functions 
 ******************************************************************/

/* LINEAR FORMATS */

at_sample at_convert_32_bit_PCM_to_AFF(gint32 value) 
{
  return (at_sample)((at_sample)value/(at_sample)AT_MAX_PCM_32);
}

at_sample at_convert_24_bit_PCM_to_AFF(gint32 value) 
{
  return (at_sample)((at_sample)value/(at_sample)AT_MAX_PCM_24);
}

at_sample at_convert_20_bit_PCM_to_AFF(gint32 value) 
{
  return (at_sample)((at_sample)value/(at_sample)AT_MAX_PCM_20);
}

at_sample at_convert_16_bit_PCM_to_AFF(gint16 value) 
{
  return (at_sample)((at_sample)value/(at_sample)AT_MAX_PCM_16);
}

at_sample at_convert_8_bit_PCM_to_AFF(gint8 value) 
{
  return (at_sample)((at_sample)value/(at_sample)AT_MAX_PCM_8);
}


gint32 at_convert_AFF_to_32_bit_PCM(at_sample value) 
{
  if (value >= -1.0 && value <= 1.0)
    return (gint32)rint(value*AT_MAX_PCM_32);
  if (value < 0.0) {
    return AT_MIN_PCM_32; /* Clipped */
  } else {
    return AT_MAX_PCM_32; /* Clipped */
  }
}

gint32 at_convert_AFF_to_24_bit_PCM(at_sample value) 
{
  if (value >= -1.0 && value <= 1.0)
    return (gint32)rint(value*AT_MAX_PCM_24);
  if (value < 0.0) {
    return AT_MIN_PCM_24; /* Clipped */
  } else {
    return AT_MAX_PCM_24; /* Clipped */
  }
}

gint32 at_convert_AFF_to_20_bit_PCM(at_sample value) 
{
  if (value >= -1.0 && value <= 1.0)
    return (gint32)rint(value*AT_MAX_PCM_20);
  if (value < 0.0) {
    return AT_MIN_PCM_20; /* Clipped */
  } else {
    return AT_MAX_PCM_20; /* Clipped */
  }
}

gint16 at_convert_AFF_to_16_bit_PCM(at_sample value) 
{
  if ((value >= -1.0) && (value <= 1.0))
    return (gint16)rint(value*AT_MAX_PCM_16);
  if (value < 0.0) {
    return AT_MIN_PCM_16; /* Clipped */
  } else {
    return AT_MAX_PCM_16; /* Clipped */
  }
}

/* yeeg, I'm scaring myself :) */
gint8 at_convert_AFF_to_8_bit_PCM(at_sample value) 
{
  guint8 pcm;
  if ((value >= -1.0) && (value <= 1.0)) {
    value = value + 1.0;
    pcm = rint (value * (at_sample) AT_MAX_PCM_8);

    return (gint8) pcm;
  }

  if (value < 0.0) {
    return AT_MIN_PCM_8; /* Clipped */
  } else {
    return AT_MAX_PCM_8; /* Clipped */
  }
}


/* LOGARITHMIC FORMATS */

at_sample at_convert_8_bit_MULAW_to_AFF(gint8 value) 
{

}

at_sample at_convert_8_bit_ALAW_to_AFF(gint8 value) 
{

}

gint8 at_convert_AFF_to_8_bit_MULAW(at_sample value) 
{

}

gint8 at_convert_AFF_to_8_bit_ALAW(at_sample value) 
{

}

void at_convert_test_harness()
{

  /* Test the conversion routines by enumerating PCM values, and
     testing for any deviations. This is an extensive test and could
     definitely takes some time. */

  gint8 test1, tmp1;
  gint16 test2, tmp2;
  gint32 test3, tmp3;
  at_sample sample;

  guint32 sample_count = 0;
  guint32 sample_errors = 0;

  test1 = 0;
  for (test1 = AT_MIN_PCM_8; test1 < AT_MAX_PCM_8; test1++) {
    sample = at_convert_8_bit_PCM_to_AFF (test1);
    tmp1 = at_convert_AFF_to_8_bit_PCM(sample);

    /* test1 and tmp1 should now be equal, if not, tally differences */
    if (test1 != tmp1) {
      g_print("8 Difference %d %f %d = %d\n", test1, sample, tmp1, 
	      test1-tmp1);
      sample_errors++;
    }
    sample_count++;
  }
     
  g_print("8bit PCM, samples=%d, errors=%d\n", sample_count, sample_errors);

  sample_errors = 0;
  sample_count = 0;
  for (test2 = AT_MIN_PCM_16; test2 < AT_MAX_PCM_16; test2++) {
    sample = at_convert_16_bit_PCM_to_AFF(test2);
    tmp2 = at_convert_AFF_to_16_bit_PCM(sample);

    /* test2 and tmp2 should now be equal, if not, tally differences */
    if (test2 != tmp2) {
      g_print("16 bit Difference %d %f %d = %d\n", test2, sample, tmp2, 
	      test2-tmp2);
      sample_errors++;
    }
    sample_count++;
  }
     
  g_print("16bit PCM, samples=%d, errors=%d\n", sample_count, sample_errors);


  sample_errors = 0;
  sample_count = 0;
  for (test3 = AT_MIN_PCM_20; test3 < AT_MAX_PCM_20; test3++) {
    sample = at_convert_20_bit_PCM_to_AFF (test3);
    tmp3 = at_convert_AFF_to_20_bit_PCM(sample);

    /* test3 and tmp3 should now be equal, if not, tally differences */
    if (test3 != tmp3) {
      g_print("20 bit Difference %d %f %d = %d\n", test3, sample, tmp3, 
	      test3-tmp3);
      sample_errors++;
    }
    sample_count++;
  }
     
  g_print("20bit PCM, samples=%d, errors=%d\n", sample_count, sample_errors);


  sample_errors = 0;
  sample_count = 0;
  for (test3 = AT_MIN_PCM_24; test3 < AT_MAX_PCM_24; test3++) {
    sample = at_convert_24_bit_PCM_to_AFF (test3);
    tmp3 = at_convert_AFF_to_24_bit_PCM(sample);

    /* test3 and tmp3 should now be equal, if not, tally differences */
    if (test3 != tmp3) {
      g_print("24 bit Difference %d %f %d = %d\n", test3, sample, tmp3, 
	      test3-tmp3);
      sample_errors++;
    }
    sample_count++;
  }
     
  g_print("24bit PCM, samples=%d, errors=%d\n", sample_count, sample_errors);


  sample_errors = 0;
  sample_count = 0;
  for (test3 = AT_MIN_PCM_32; test3 < AT_MAX_PCM_32; test3++) {
    sample = at_convert_32_bit_PCM_to_AFF (test3);
    tmp3 = at_convert_AFF_to_32_bit_PCM(sample);

    /* test3 and tmp3 should now be equal, if not, tally differences */
    if (test3 != tmp3) {
      g_print("32 bit Difference %d %f %d = %d\n", test3, sample, tmp3, 
	      test3-tmp3);
      sample_errors++;
    }
    sample_count++;
  }
     
  g_print("32bit PCM, samples=%d, errors=%d\n", sample_count, sample_errors);

}











#ifndef __ATECH_PLUGIN_TRACK__
#define __ATECH_PLUGIN_TRACK__

#include "atech_plugin.h"

typedef enum
{
  PLUGIN_TYPE_REALTIME      = 1 << 0,
  PLUGIN_TYPE_SAVE          = 1 << 1,
  PLUGIN_TYPE_LOAD          = 1 << 2,
  PLUGIN_TYPE_NON_REALTIME  = 1 << 3
} PluginType;


typedef struct PluginTrack
{
  gchar *filename;
  gint ID;
}


PluginTrack *
plugin_track_get_composite (PluginLink *link);

#endif /* __ATECH_PLUGIN_TRACK__ */

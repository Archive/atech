#ifndef __AT_FILEOP_H__
#define __AT_FILEOP_H__

void  at_fileop_saveas_cmd (GtkWidget *, gpointer);
void  at_fileop_open_cmd (GtkWidget *, gpointer);

#endif /* __AT_FILEOP_H__ */

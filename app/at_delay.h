#ifndef __AT_DELAY_H__
#define __AT_DELAY_H__

#include <gtk/gtk.h>

gint
at_delay (gint msecs, GtkFunction *function, gpointer data);

#endif /* __AT_DELAY_H__ */



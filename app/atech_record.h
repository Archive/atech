#ifndef __ATECH_RECORD_H__
#define __ATECH_RECORD_H__

typedef struct
{
    gchar device[256];
    gint rate;
    gint bits;
    gint channels;
} AtechRecordAttr;

#endif /* __ATECH_RECORD_H__ */

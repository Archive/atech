#ifndef __AT_COM_H__
#define __AT_COM_H__

void at_com_init (void);
void at_com_cleanup(void);

void at_com_play_configure (GtkWidget *widget, gpointer data);
void at_com_play_start (GtkWidget *widget, gpointer data);
void at_com_play_stop (GtkWidget *widget, gpointer data);

void at_com_record_configure (GtkWidget *widget, gpointer data);
void at_com_record_start (GtkWidget *widget, gpointer data);
void at_com_record_stop (GtkWidget *widget, gpointer data);

#endif /* __AT_COM_H__ */

#include <gtk/gtk.h>
#include "libatech.h"

gfloat *wave_data[3];
GtkWidget *wave;

void change_range (GtkWidget *widget, gpointer *data)
{
  static gint range = 1;
  gtk_wave_set_vertical_range (GTK_WAVE (wave), range++);
}

void position_change (GtkWidget *widget, gint data, gpointer user_data)
{
  g_print ("position changed to %d\n", data);
}

void selection_change (GtkWidget *widget, gint start_selection, 
		       gint end_selection, gpointer user_data)
{
  g_print ("selection changed to %d to %d\n", start_selection, end_selection);
}

void button_event (GtkWidget *widget, GdkEventButton *bevent, gpointer data)
{
  gchar *state = NULL;
  
  if (bevent->type == GDK_BUTTON_PRESS)
    state = g_strdup ("pressed");
  if (bevent->type == GDK_BUTTON_RELEASE)
    state = g_strdup ("released");

  if (!state)
    state = g_strdup ("uuuuhhhhh");
  
  g_print ("button %d was %s on wave display\n", 
	   bevent->button, state);
  
  g_free (state);

}


void change_something (GtkWidget *widget, gpointer *data)
{
  /* changing to points */
  gtk_wave_set_display_type (GTK_WAVE (wave), GTK_WAVE_TYPE_POINTS);
}

void delete_event (GtkWidget *widget, GdkEvent *event, gpointer *data)
{
  gtk_main_quit ();
}

gint timeout_test (gpointer data) 
{
  gint i, j;
  
  for (j=0; j < 3; j++) {
    for (i=0; i < 640; i++) {
      wave_data[j][i] += 0.01 + j / 10;
      
      if (wave_data[j][i] > 2.0)
	wave_data[j][i] = -2.0;
    }
  }
  gtk_wave_set_data (GTK_WAVE (wave), 0, wave_data[0], 500);
  gtk_wave_set_data (GTK_WAVE (wave), 1, wave_data[1], 500);
  gtk_wave_set_data (GTK_WAVE (wave), 2, wave_data[2], 500);


  return (TRUE);
}
 

int main (int argc, char *argv[])
{
  /* GtkWidget is the storage type for widgets */
  GtkWidget *window;
  GtkWidget *button;
  GtkWidget *scrolled_window;
  GtkWidget *box1;
  GtkWidget *frame;
  GtkWidget *list;
  GtkWidget *item;
  GList *items = NULL;
  gint i, j;

  /* this is called in all GTK applications.  arguments are parsed from
   * the command line and are returned to the application. */
  gtk_init (&argc, &argv);

  /* create a new window */
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), "GtkWave demo");
  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		      GTK_SIGNAL_FUNC (delete_event), NULL);
  gtk_container_border_width (GTK_CONTAINER (window), 10);
  gtk_widget_set_usize (GTK_WIDGET (window), 400, 400);
  
  /* scrolled_window */
  scrolled_window = gtk_scrolled_window_new(NULL, NULL);
  gtk_container_add (GTK_CONTAINER (window), scrolled_window);
  gtk_widget_show (scrolled_window);

  /* main vbox */
  box1 = gtk_vbox_new(FALSE, 0);
  gtk_widget_show (box1);

  /* button */
  button = gtk_button_new_with_label ("Change Range");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (change_range), (gpointer) "button 1");
  gtk_box_pack_start(GTK_BOX(box1), button, FALSE, FALSE, 0);
  gtk_widget_show(button);

  /* button */
  button = gtk_button_new_with_label ("Change to points");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (change_something), (gpointer) "button 2");
  gtk_box_pack_start(GTK_BOX(box1), button, FALSE, FALSE, 0);
  gtk_widget_show (button);

  /* list */
  list = gtk_list_new ();
  gtk_container_add (GTK_CONTAINER (scrolled_window), list); 
  gtk_list_set_selection_mode (GTK_LIST (list),
			       GTK_SELECTION_BROWSE);
  gtk_widget_show (list);

  
  /* frame for wave */
  frame = gtk_frame_new("Wave");
  gtk_container_border_width (GTK_CONTAINER (frame), 10);
  gtk_box_pack_start (GTK_BOX (box1), frame, TRUE, TRUE, 0);
  gtk_widget_show (frame);

  for (i=0; i<3; i++)
    wave_data[i] = g_malloc (1000 * sizeof (gfloat));
  
  for (j=0; j < 3; j++) {
    for (i=0; i < 500; i++) {
      wave_data[j][i] = tan ((gfloat) i / 640.0) + j;
    }
  }
  /* wave */
  wave = gtk_wave_new ();
  gtk_wave_set_data (GTK_WAVE (wave), 0, wave_data[0], 500);
  gtk_wave_set_data (GTK_WAVE (wave), 1, wave_data[1], 500);
  gtk_wave_set_data (GTK_WAVE (wave), 2, wave_data[2], 500);

  gtk_container_add (GTK_CONTAINER (list), wave);
  gtk_widget_set_usize (GTK_WIDGET (wave), 200, 150);
  
  gtk_signal_connect (GTK_OBJECT (wave), "position_changed",
		      GTK_SIGNAL_FUNC (position_change), NULL);

  gtk_signal_connect (GTK_OBJECT (wave), "button_event",
		      GTK_SIGNAL_FUNC (button_event), NULL);

  gtk_signal_connect (GTK_OBJECT (wave), "selection_changed",
		      GTK_SIGNAL_FUNC (selection_change), NULL);

  gtk_wave_set_vertical_range (GTK_WAVE (wave), 2);
  
  
  gtk_widget_show (wave);

  /* list item */
  item = gtk_list_item_new ();
  gtk_container_add (GTK_CONTAINER (item), frame);
  gtk_widget_show (item);
  items = g_list_prepend (items, item);
  gtk_list_clear_items (GTK_LIST (list), 0, -1);
  gtk_list_append_items (GTK_LIST (list), items);
  
  
  gtk_widget_show (window);

  gtk_timeout_add(500, timeout_test, wave);

  gtk_container_add (GTK_CONTAINER (scrolled_window), box1);

  /* rest in gtk_main and wait for the fun to begin! */
  gtk_main ();

  return 0;
}













/* Audiotechque -- Multitrack Mixer and Audio manipulation program
 * Copyright (C) 1997-1998 Ian Main
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>
#include <signal.h>
#include <sys/types.h>

#include "at_com.h"
#include "at_track.h"
#include "at_view.h"
#include "at_interface.h"
#include "libatech.h"

/* IPC connections */
IpcProcess *play_module;
IpcProcess *record_module;

static Clip *current_record_clip;
static Clip *current_play_clip;

static void 
at_com_playing_completed (IpcProcess *ipcp, gpointer data);

static void 
at_com_recording_completed (IpcProcess *ipcp, gpointer data);



/* any initialization that needs to be performed here.  This will
 * be called at startup */
void at_com_init (void)
{
    /* just exec our play and record modules */
    /* if you want to try the alsa drivers, change it here */
    record_module = at_ipc_fork_exec ("./atech_record_oss");
    play_module = at_ipc_fork_exec ("./atech_play_oss");

    at_ipc_signal_connect (record_module, "recording_completed", 
			   at_com_recording_completed, NULL);

    at_ipc_signal_connect (play_module, "playing_completed", 
			   at_com_playing_completed, NULL);

}

void at_com_cleanup(void)
{
    at_ipc_signal_destroy (play_module);
    at_ipc_signal_destroy (record_module);
}

static void 
at_com_playing_completed (IpcProcess *ipcp, gpointer data)
{
    g_print ("done playing\n");
    at_clip_play_done (current_play_clip);    
    current_play_clip = NULL;
}

static void 
at_com_recording_completed (IpcProcess *ipcp, gpointer data)
{
    /* In the future, we may want to use this for error
       reporting as well. */
    g_print ("recording completed.\n");
    at_clip_record_done (current_record_clip);    
    current_play_clip = NULL;
}

/* configure record server */
void at_com_record_configure (GtkWidget *widget, gpointer data)
{
    at_ipc_signal_emit (record_module, "configure");
}

/* begin recording track. */
void at_com_record_start (GtkWidget *widget, gpointer data)
{
    View *view = (View *) data;
    Clip *clip = view->clip;
    
    if (at_clip_recording (clip))
        return;
    at_ipc_signal_emit (record_module, "start_recording (string)", 
			at_track_get_record_track());
    /* just to keep tabs on the recording */
    at_clip_record_start (clip);
    current_record_clip = clip;
}

void at_com_record_stop (GtkWidget *widget, gpointer data)
{
    View *view = (View *) data;
    Clip *clip = view->clip;
    
    if (!at_clip_recording (clip))
        return;
    g_print ("emitting stop_recording signal\n");
    at_ipc_signal_emit (record_module, "stop_recording");
}

void at_com_play_configure (GtkWidget *widget, gpointer data)
{
    at_ipc_signal_emit (play_module, "configure");
}

/* Actually play the current track(s), selection etc. */
void at_com_play_start (GtkWidget *widget, gpointer data)
{
    View *view = (View *) data;
    Clip *clip = view->clip;
    
    if (at_clip_playing(clip))
      return;
    at_ipc_signal_emit (play_module, "start_playing (string, float)",
			at_clip_get_play_file(clip), 
			clip->composite_track->volume);

    g_print ("clip->composite_track->volume is %f\n", clip->composite_track->volume);

    /* keep track of playing */
    at_clip_play_start (clip);
    current_play_clip = clip;
}

void at_com_play_stop (GtkWidget *widget, gpointer data)
{
    View *view = (View *) data;
    Clip *clip = view->clip;
    
    if (!at_clip_playing (clip))
      return;
    at_ipc_signal_emit (play_module, "stop_playing");
    at_clip_play_done (clip);
}















#ifndef __AT_CLIP_H__
#define __AT_CLIP_H__

#include <glib.h>
#include "region.h"
#include "at_track.h"
#include "at_type.h"
#include "at_enums.h"

typedef struct _Clip Clip;
typedef struct _Mix Mix;
typedef struct _Guide Guide;

/* ugly hack because this header depends on at_track.h (and now at_mix.h) 
and vice-versa */
typedef struct _Track Track_;

struct _Mix
{
    MixType mix_type;            /* type of mix being performed */
    gint position;               /* current position in track */
    Track_ *current_track;       /* current track being mixed in */
    GSList *track_list;          /* tracks that require mixing */
    GSList *list;                /* for walking through the above list */
    gint record_tag;             /* idle tag */
    gint track_count;            /* number of tracks to mix */
    TmpRegion *track_region;     /* mmap'd data of current track */
    TmpRegion *composite_region; /* mmap'd data of composite */
    gint bps;                    /* bytes per sample of mix */
};


struct _Guide
{
  int position;
};

struct _Clip
{
  gchar *title;               /* name of the Clip         */
  gchar *filename;            /* orignal filename         */
  gint has_filename;          /* has a valid filename     */

  size_t length;              /* the length of the clip   */
  gint sample_rate;           /* the clips sample rate    */
  gint base_type;             /* the base_type            */

  AudioRegion *composite;     /* the current composition  */
  AudioRegion *selection;     /* the current selection    */ 
  Track_ *composite_track;    /* composite track for display */
  gint dirty;
  gint undo_on;               /* is undo enabled?         */
  
  Mix mix;                    /* mixing information for idle mixing */
  gint mixing;                /* are we currently mixing ? */
    
  gint instance_count;        /* number of instances      */
  gint ref_count;             /* number of references     */

  gint ID;                    /* unique ID                */
  GSList *guides;             /* guides                   */

  /* --- record/playback checks --- */
  gint recording;
  gint playing;

  /* --- track/channel attributes --- */
  GSList *tracks;             /* the tracks               */
  GSList *channels;           /* clip level masks         */
  GSList *track_stack;        /* tracks in MRU order      */

  gint active_track;
  gint active_channel;

  /* --- Undo info                --- */
  GSList *undo_stack;          /* undo operations          */
  GSList *redo_stack;          /* redo operations          */
  gint undo_bytes;             /* bytes in the undo stack  */
  gint undo_levels;            /* levels in the undo stack */
  gint pushing_undo_group;     /* undo group status flag   */

  int mute[MAX_CHANNELS];    /* mute channels            */
  int active[MAX_CHANNELS];  /* active channels          */
};


/* access functions */
Clip *            at_clip_new                  (size_t, gint, gint);
void              at_clip_delete               (Clip *);
Clip *            at_clip_get_named            (char *);
Clip *            at_clip_get_ID               (int);
void              at_clip_resize               (Clip *, size_t, size_t);

void              at_clip_mix_invalidate       (Clip *);
void              at_clip_set_active_track     (Clip *, gint);
gint              at_clip_get_track_index      (Clip *, gint);

gint              at_clip_raise_track          (Clip *, gint);
gint              at_clip_lower_track          (Clip *, gint);

void              at_clip_record_done          (Clip *clip);
void              at_clip_record_start         (Clip *clip);
gint              at_clip_recording            (Clip *clip);

void              at_clip_play_done            (Clip *clip);
void              at_clip_play_start           (Clip *clip);
gint              at_clip_playing              (Clip *clip);

void              at_clip_update               (Clip *clip);
gint              at_clip_delayed_update       (gpointer data);
void              at_clip_stop_idle_mix        (Clip *clip);

gint              at_clip_get_composite_length (Clip *clip);


void              at_clip_new_cmd_callback     (GtkWidget *widget, gpointer data);
void              at_clip_open_cmd_callback     (GtkWidget *widget, gpointer data);
void              at_clip_save_cmd_callback     (GtkWidget *widget, gpointer data);

gchar *           at_clip_get_play_file        (Clip *clip);
Clip *atf_load (char *filename);
gint atf_save (Clip *, char *filename);

#endif /* __AT_CLIP_H__ */










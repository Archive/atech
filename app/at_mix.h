#ifndef __AT_MIX_H__
#define __AT_MIX_H__

#include <glib.h>
#include "at_clip.h"
#include "at_track.h"
#include "at_type.h"
#include "at_enums.h"

#include <sys/types.h>


/* offset and length are currently ignored. */ 
void              at_mix_tracks_in_clip       (Clip *clip, size_t offset, size_t length);

void              at_mix_stop_idle_mix        (Clip *clip);

#endif /* __AT_MIX_H__ */













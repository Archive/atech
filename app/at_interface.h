#ifndef __AT_INTERFACE_H__
#define __AT_INTERFACE_H__

/* initialize atech GUI */
void at_interface_init(void);

/* create a button with a pixmap and a label */
GtkWidget *at_interface_pixmapped_button_new (gchar *xpm_filename, 
					    gchar *label_text);
/* create a pixmap. */
GtkWidget *at_interface_pixmap_new (GtkWidget *parent, gchar *xpm_filename);


/* to print out debug/error messages */
void at_print(gchar *format, ...);


#endif /* __AT_INTERFACE_H__ */

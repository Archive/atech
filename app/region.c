/* Audiotechque -- Multitrack Mixer and Audio manipulation program
 * Copyright (C) 1997-1998 Larry Ewing and Ian Main
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <glib.h>
#include "region.h"
#include "at_type.h"

AudioRegion *
region_new (size_t length, gint bps)
{
  AudioRegion *new;
  gchar c = 2;
  gint fd;

  new = (AudioRegion *) g_malloc (sizeof (AudioRegion));

  new->type = REGION_MMAP;
  new->filename = tempnam ("./", "atech");
  new->offset = 0;
  new->bps = bps;
  new->length = length;

  new->tmp_region_ref = 0;
  new->tmp_region = NULL;

  if ((fd = open(new->filename, O_RDWR | O_CREAT | O_TRUNC, 
		 S_IRUSR | S_IWUSR)) < 0)
    g_error ("Unable to create create '%s' for writting: %s", 
	     new->filename,
	     g_strerror (errno));
  
  if (length > 0) {
      if (lseek(fd, length * bps -1, SEEK_SET) == -1)
	  g_warning ("lseek: %s", g_strerror (errno));
      if (write(fd, &c, sizeof (gchar)) != 1)
	  g_warning ("write: %s", g_strerror (errno));
  }

  new->fd = fd;

  g_print ("new region - bps = %d, filename = '%s'\n", new->bps, new->filename);

  return new;
}


AudioRegion *
region_new_from_file (char *filename, gint bps) 
{
  struct stat statbuf;
  AudioRegion *new = NULL;
  
  new = (AudioRegion *) g_malloc (sizeof (AudioRegion));

   
  new->type = REGION_MMAP;      
  new->filename = g_strdup (filename);
  new->offset = 0;
  new->bps = bps;

  new->tmp_region_ref = 0;
  new->tmp_region = NULL;

  if ((new->fd = open(new->filename, O_RDWR, 
		 S_IRUSR | S_IWUSR)) < 0)
    {
      g_warning ("can't create %s for writting: %s", 
		 new->filename,
		 g_strerror (errno));

      g_free (new->filename);
      g_free (new);
      return (NULL);
    }

  if (fstat(new->fd, &statbuf) < 0)
    {
      g_warning ("fstat error");
      g_free (new->filename);
      g_free (new);
      return (NULL);
    }
  new->length = statbuf.st_size / bps; 

  g_print ("region_new_from_file (char *filename = '%s', gint bps = %d) - size = %ld\n",
	   filename, bps, statbuf.st_size);

  return (new);

}

  
void 
region_delete (AudioRegion *del)
{
  g_return_if_fail (del != NULL);

  g_print ("region_delete - filename is '%s'\n", del->filename);

  if (del->tmp_region_ref > 0) {
    g_warning ("Deleting audio region with referenced tmp_regions!!!!\n");

    /* kill all references */
    del->tmp_region_ref = 1;
    
    /* kill tmp region */
    tmp_region_destroy (del->tmp_region);
  }
    
  if (close (del->fd) == -1) 
    g_warning ("failed to close file: %s : %s", 
	       del->filename, g_strerror (errno));
  if (unlink (del->filename) == -1)
    g_warning ("failed to unlink file: %s : %s",
	       del->filename, g_strerror (errno));

  g_free (del->filename);
  g_free (del);
}


void
region_sync (AudioRegion *dest)
{
  if (dest->tmp_region)
    tmp_region_sync (dest->tmp_region);
}

void 
tmp_region_sync (TmpRegion *tmp)
{
  if (msync (tmp->data, tmp->length * tmp->bps, 
	     MS_SYNC | MS_INVALIDATE) == -1)
    g_warning ("msync: %s", g_strerror (errno));
} 


void
region_resize (AudioRegion *src, size_t offset, size_t length)
{
    char c = 2;  /* my lucky number :-) */

    g_return_if_fail (offset >= 0);

    if (src->tmp_region_ref != 0) {
  	g_warning("Resizing a mmap-ed region!  Playing with fire!");
  	g_warning("fd: %ld, file: %s", (off_t)src->fd, src->filename);
    }


    if (offset == 0) { 
	if (length > src->length ) {
	    if (lseek (src->fd, length * src->bps - 1, SEEK_SET) == -1)
		g_warning ("lseek: %s", g_strerror (errno));
	    
	    /* it's necessary to write to the end of the file
	       to get it to actually fill in that space. 
	       Otherwise you get segfaults as soon as you go
	       beyond the length of the file.  - Ian */
	    if (write (src->fd, &c, sizeof (gchar)) == -1)
		g_warning ("write: %s", g_strerror (errno));

	} else {

	    if ( (ftruncate (src->fd, length * src->bps) == -1) )
		g_warning ("ftruncate: %s", g_strerror (errno));
	}
	src->length = length;
    }
}

/* fixme - use tmp_region_new() */
void 
tmp_region_zero (TmpRegion *tmp)
{
  memset (tmp->data, 0, tmp->length * tmp->bps);
}


/* all mmap access to files should be done using this function. */

TmpRegion *
tmp_region_new (AudioRegion *src,
		size_t offset,
		size_t length)
{
  TmpRegion *new;
  static void *last_address = NULL;

  g_print ("--> requesting region from file '%s' - ref %d - length %d - size %d", 
	   src->filename, src->tmp_region_ref, length, length * src->bps);

  /* check if we already have a tmp_region active */
  if ( (src->tmp_region_ref > 0) && (src->tmp_region != NULL)) {
    src->tmp_region_ref++;
    g_print ("tmp_region_ref is %d - returning new copy\n",
	     src->tmp_region_ref);
    return (src->tmp_region);
  }
  new = g_malloc (sizeof (TmpRegion));

  src->tmp_region_ref++;

  new->parent = src;

  new->bps = src->bps;
  
  new->type = src->type;

  new->length = length;

  new->data = mmap (NULL, length * new->bps, PROT_READ | PROT_WRITE,
		    MAP_SHARED, src->fd, 
		    src->offset + (offset * src->bps));
  
  last_address = new->data;

  g_print (" - size %d\n", length * new->bps);

  if (new->data == (void *) -1) {
    g_warning ("Error mmap'ing file: %s", g_strerror (errno));
    g_free (new);
    return (NULL);
  }

  return (new);
}

void
tmp_region_destroy (TmpRegion *del)
{
  AudioRegion *parent;

  
  if (!del)
    {
      g_warning ("destroying a null region");
      return;
    }

  parent = del->parent;

  g_print ("--> requesting delete of region from file '%s' - ref %d\n", 
	   parent->filename, parent->tmp_region_ref);


  if ( (parent->tmp_region_ref == 1) && (del->data) ) {
    if (msync (del->data, del->length * del->bps, 
	       MS_SYNC | MS_INVALIDATE) == -1)
      g_warning ("msync: %s", g_strerror (errno));
    
    if (munmap (del->data, del->length) == -1)
      g_warning ("munmap: %s", g_strerror (errno));
    
    del->data = NULL;
    parent->tmp_region = NULL;
    g_free (del);
  }
  
  parent->tmp_region_ref--;
}










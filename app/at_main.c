/* Audiotechque -- Multitrack Mixer and Audio manipulation program
 * Copyright (C) 1997-1998 Ian Main
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>

#include <signal.h>

#include "at_main.h"
#include "at_interface.h"
#include "at_com.h"
#include "at_track.h"
#include "at_plugin.h"

int main (int argc, char *argv[])
{
    struct sigaction sig;
    
    sig.sa_handler = SIG_IGN;
    sig.sa_flags = SA_NOCLDSTOP | SA_NOMASK;
    sigaction(SIGCHLD, &sig, NULL);

    bindtextdomain (PACKAGE, GNOMELOCALEDIR);
    textdomain (PACKAGE);
 
    gnome_init ("atech", NULL, argc, argv, 0, NULL);

    /* initialize play/record server interface */
    at_com_init();

    /* initilize the plugin interface */
    at_plugin_interface_init();
    
    /* initialize user interface */
    at_interface_init();

    gtk_main ();

    at_com_cleanup();
    
    return 0;
}









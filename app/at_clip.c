/* Audiotechque -- Multitrack Mixer and Audio manipulation program
 * Copyright (C) 1997-1998 Larry Ewing and Ian Main
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glib.h>
#include "at_track.h"
#include "at_clip.h"
#include "at_view.h"
#include "at_type.h"
#include "at_mix.h"
#include "at_atf.h"

/* static variables */
GSList *clip_list = NULL;
gint global_clip_ID = 1;


static Clip *
at_clip_create (void)
{
  Clip *clip;
 
  clip = (Clip *) g_malloc (sizeof (Clip));

  clip->title = NULL;
  clip->filename = NULL;
  clip->has_filename = FALSE;
  
  clip->composite = NULL;
  clip->selection = NULL;
  
  clip->dirty = FALSE;
  clip->undo_on = TRUE;
  
  clip->instance_count = 0;
  clip->ref_count = 0;
  
  clip->ID = global_clip_ID++;
  clip->guides = NULL;

  clip->tracks = NULL;
  clip->channels = NULL;
  clip->track_stack = NULL;
  clip->active_track = 0;
  clip->undo_stack = NULL;
  clip->redo_stack = NULL;
  clip->undo_bytes = 0;
  clip->undo_levels = 0;
  clip->pushing_undo_group = 0;

  clip->recording = FALSE;
  clip->playing = FALSE;
  
  clip_list = g_slist_append (clip_list, clip);

  return clip;
}
  
Clip *
at_clip_new (size_t length, gint base_type, gint sample_rate)
{
  Clip *clip;
  int i;

  clip = at_clip_create ();
  
  clip->filename = NULL;
  clip->length = length;
  
  clip->base_type = base_type;
  clip->sample_rate = sample_rate;
  clip->active_track = -1;
  clip->active_channel = -1;

  for (i = 0; i < MAX_CHANNELS; i++)
    {
      clip->mute[i] = FALSE;
      clip->active[i] = TRUE;
    }   
  
  /* FIXME base type is simply not correct here */
  clip->composite = region_new (length, at_type_bytes (base_type));
  /* FIXME This is wrong too I think.... */
  clip->selection  = region_new (length, at_type_bytes (AT_MONO));

  return clip;

}

Clip *
at_clip_get_named (char *name)
{
  GSList *current = clip_list;
  Clip *clip;

   while (current) 
     {
       clip = (Clip *) (current->data);
       if (strcmp (clip->title, name) == 0)
	 return clip;

       current = current->next;
     }

   return NULL;  
}

Clip *
at_clip_get_ID (int clip_ID)
{
  GSList *current = clip_list;
  Clip *clip;

   while (current) 
     {
       clip = (Clip *) (current->data);
       if (clip->ID == clip_ID)
	 return clip;

       current = current->next;
     }

   return NULL;
}

/* hmm.. is this useful still ? - Ian */

void 
at_clip_resize (Clip *clip, size_t new_length, size_t new_offset)
{
  GSList *list = clip->tracks;
  Track *track;

  if (new_length <= 0)
    {
      g_warning ("at_clip_resize: length must be positive");
      return;
    }

  /* Push the undo mod onto the stack */
  /* 
     undo_push_group_start (clip, CLIP_MOD_UNDO);
     undo_push_clip_mod (clip);
     */

  g_print ("resizing clip - new_length = %d\n", new_length);

  clip->length = new_length;

  /* resize the channels and the selection mask here */
  if (clip->composite)
    region_resize (clip->composite, new_offset, new_length);

  /* we shouldn't be resizing the selection should we ? - Ian */
  if (clip->selection)
    region_resize (clip->selection, new_offset, new_length);
  
  /* translate all the tracks to the new start offset */
  while (list)
    {
      track = (Track *) list->data;
      at_track_translate (track, new_offset);
      list = list->next;
    }

  /* undo_push_group_end */
  /* at_clip_invalidate_thumbnails (clip);
  at_composite_update_full (clip->ID);*/
}


Track *
at_clip_add_track (Clip *clip, Track *track,  int position)
{
  if (track->clip_ID != 0 && track->clip_ID != clip->ID)
    {
      g_warning ("at_clip_add_track: attempt to add track to wrong clip");
      return NULL;
    }

  /* FIXME we need to deal with undo here */

  if (position == -1)
    position = at_clip_get_track_index (clip, clip->active_track);

  if (position != -1)
    clip->tracks = g_slist_insert (clip->tracks, track, position);  
  else
    clip->tracks = g_slist_prepend (clip->tracks, track);
  
  clip->track_stack = g_slist_prepend (clip->track_stack, track);
 
  /* update the active track to the new track */
  at_clip_set_active_track (clip, track->ID);

  return track;
}


Track *
at_clip_remove_track (Clip *clip, gint track_ID)
{
  Track *track;

  if ((track = at_track_get_ID (track_ID))) {
      /* FIXME undo.... */
      
      clip->tracks = g_slist_remove (clip->tracks, track);
      clip->track_stack = g_slist_remove (clip->track_stack, track);
      
      if (track_ID == clip->active_track) {
	  if (clip->tracks) {
	      clip->active_track = ((Track *) clip->track_stack->data)->ID;
	  } else {
	      clip->active_track = -1;
	  }
      }

      /* at_views_update_area (clip->ID, track->start_offset, track->length); 
       */
      
      clip->dirty = TRUE;
      at_clip_update (clip);
      
      return NULL;
  }
  else 
      return NULL;

}

gint
at_clip_raise_track (Clip *clip, gint track_ID)
{
  GSList *list = clip->tracks;
  GSList *prev = NULL;
  gint    pos = 0;
  Track  *track = NULL;

  while (list)
    {
      track = list->data;
      if (track->ID == track_ID)
	break;
      prev = list;
      list = list->next;
      pos++;
    }

  clip->tracks = g_slist_remove (clip->tracks, list->data);
  clip->tracks = g_slist_insert (clip->tracks, (gpointer)track, ++pos);
  
  return pos;
}

gint
at_clip_lower_track (Clip *clip, gint track_ID)
{
  GSList *list = clip->tracks;
  GSList *prev = NULL;
  gint    pos = 0;
  Track  *track = NULL;

  while (list)
    {
      track = list->data;
      if (track->ID == track_ID)
	break;
      prev = list;
      list = list->next;
      pos++;
    }

  clip->tracks = g_slist_remove (clip->tracks, list->data);
  clip->tracks = g_slist_insert (clip->tracks,
				 (gpointer)track,
				 --pos < 0 ? 0 : pos);
  
  return pos;
}
void
at_clip_set_active_track (Clip *clip, gint track_ID) 
{
  clip->active_track = track_ID;

  /* FIXME this needs to update any previews etc... */

}

Track *
at_clip_get_active_track (Clip *clip)
{
  return at_track_get_ID (clip->active_track);
}

/**************************************************
 * Keep tabs on recording and playback of clips
 *************************************************/

void
at_clip_record_start (Clip *clip)
{
  if (clip) {
      clip->recording = TRUE;
  }
}

void
at_clip_record_done (Clip *clip)
{
  if (clip) {
      clip->recording = FALSE;
  }
  
  at_track_record_done (clip);
  at_clip_update (clip);
}

gint
at_clip_recording (Clip *clip)
{
  return (clip->recording);
}

void
at_clip_play_start (Clip *clip)
{
  if (clip) {
      clip->playing = TRUE;
  }
}

void
at_clip_play_done (Clip *clip)
{
  if (clip) {
      clip->playing = FALSE;
  }
}

gint
at_clip_playing (Clip *clip)
{
  return (clip->playing);
}


gint
at_clip_get_track_index (Clip *clip, gint track_ID)
{
  Track *track;
  GSList *tracks = clip->tracks;
  int index = 1;

  while (tracks)
    {
      track = (Track *) tracks->data;
      if (track->ID == track_ID)
	return index;
      
      index++;
      tracks = tracks->next;
    }
  
  return -1;
}

void
at_clip_update (Clip *clip) 
{
    /* TODO:
       the clip-length is no longer valid since it is
       determied at mixing time. */
    at_mix_tracks_in_clip (clip, 0, clip->length);
} 

/* called from gtk_timeout_add ()'s */
gint 
at_clip_delayed_update (gpointer data)
{
  Clip *clip = data;
  at_clip_update (clip);

  return (FALSE);
}

/* returns the length of the longest track */
gint
at_clip_get_composite_length (Clip *clip)
{
    GSList *list;
    Track *track;
    gint clip_length = 0;

    list = clip->tracks;

    while (list) {
	track = (Track *) list->data;

	if (!track->is_composite) {
	    if (track->length > clip_length) {
		clip_length = track->length;
	    }
	}

	list = list->next;
    }

    return (clip_length);
}

void
at_clip_new_cmd_callback (GtkWidget *widget, gpointer data)
{
  Clip *clip;
  View *view;
  Track *track;

  clip = at_clip_new (3200, AT_RL, 44100);
  view = at_view_new (clip);

  /* create a track to view the composite on */
  track = at_track_new (view->clip->ID, 0, 0, AT_RL, "Composite", 1.0, 0);
  at_clip_add_track (view->clip, track, 
		     at_clip_get_track_index (view->clip,
					      view->clip->active_track));

  /* delete the region created with the track */
  region_delete (track->data);
  /* set it to the composite data */
  track->data = clip->composite;
  clip->composite_track = track;
  track->is_composite = TRUE;
  
  /* create a first track for recording on */
  track = at_track_new (view->clip->ID, 0, 0, AT_RL, "New Track", 1.0, 0);
  at_clip_add_track (view->clip, track, 
		     at_clip_get_track_index (view->clip,
					      view->clip->active_track));

  at_views_update (view->clip->ID);
}

gchar *
at_clip_get_play_file (Clip *clip)
{  
  region_sync (clip->composite);
  return clip->composite->filename;
}

		      
















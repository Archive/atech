/* Audiotechque -- Multitrack Mixer and Audio manipulation program
 * Copyright (C) 1997-1998 Ian Main
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>

GList *delay_list;

typedef struct _Delay Delay;

struct _Delay
{
  gint tag;
  gint delay_tag;
  GtkFunction function;
};

static gint
at_delay_done (gpointer data);


/* There is a slight possibility for a race condition here, 
   but I think it is *very* unlikely, and even if it did occur, 
   I don't think any damage would be done (except that the timeout
   function could be called twice. */

gint
at_delay (gint msecs, GtkFunction function, gpointer data)
{
  GList *list;
  Delay *delay;

  list = delay_list;


  while (list) {
    delay = list->data;
    
    if (delay->function == function) {
      /* remove old timeout */
      gtk_timeout_remove (delay->tag);
      gtk_timeout_remove (delay->delay_tag);
      delay_list = g_list_remove (delay_list, delay);
      g_free (delay);
    }

    list = list->next;
  }
  
  delay = g_malloc (sizeof (Delay));
  
  delay->function = function;
  delay_list = g_list_append (delay_list, delay);

  delay->tag = gtk_timeout_add (msecs, (GtkFunction) function, data);
  delay->delay_tag = gtk_timeout_add (msecs, at_delay_done, delay);

  return (delay->tag);
}

static gint
at_delay_done (gpointer data)
{
  Delay *delay = data;
  
  delay_list = g_list_remove (delay_list, delay);
  g_free (delay);
  
  return (FALSE);
}






#include <glib.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

/* alsa's sound.h */
#include <sys/ioctl.h>
/* this is the correct location really.. */
/* #include <linux/sound.h> */
#include "sound.h"

#include <gtk/gtk.h>

#include "libatech.h"
#include "atech_play.h"

#define DSP_BUF_SIZE 4096


/* our lifeline to the main module */
IpcProcess *main_module;

/* are we currently playing ?  TRUE/FALSE */
gint playing;

/* for gdk_input_add */
gint play_tag;

/* file descriptors */
gint srcfd;
gint dspfd;

/* getting the feeling this should all go in a struct :) */

/* this will be a linked list of files to mix in the future */
gchar *output_file;		    

/* one global struct to keep state info for us - mostly used
 * for IPC */
AtechPlayAttr play_info;


static void set_ALSA_dsp (gint fd, gint rate,
			  gint bits, gint channels);

static void start_playing(IpcProcess *ipcp, gpointer data);

static void stop_playing(IpcProcess *ipcp, gpointer data);

static void play_data (gpointer data, gint source, 
		       GdkInputCondition condition);

static void stop_playing_real (void);

static void configure (IpcProcess *ipcp, gpointer data);

static void play_quit(IpcProcess *ipcp, gpointer data);

static void AFF_to_native (at_sample *sample, guint16 *buf, gint size);

gint main (gint argc, gchar *argv[])
{
        
    gtk_init(&argc, &argv);
    
    /* obviously we're not playing when we start */
    playing = FALSE;

    /* initialize connection to main module */
    main_module = at_ipc_init(argc, argv);

    /* setup sane defaults.. this will probably be loaded from
     * a config file in the future. */
    play_info.rate = 44100;
    play_info.bits = 16;
    play_info.channels = 2;
    play_info.volume = 1.0;

    strcpy (play_info.device, "/dev/sndpcm01");

    
    at_ipc_signal_connect (main_module, "start_playing", 
			   start_playing, NULL);
    at_ipc_signal_connect (main_module, "stop_playing", 
			   stop_playing, NULL);
    at_ipc_signal_connect (main_module, "configure",
			   configure, NULL);
    at_ipc_signal_connect (main_module, "destroy",
			   play_quit, NULL);
    
    gtk_main();
    
    return(0);
}

static void start_playing(IpcProcess *ipcp, gpointer data)
{
    at_ipc_signal_recv_args(main_module, "alloc string, float",
			    &output_file, &play_info.volume);

    /* if we're already playing, just leave */
    if (!playing) {
	playing = TRUE;
    } else {
	return;
    }
    
    if ( (dspfd = open(play_info.device, O_WRONLY)) < 0) {
	g_warning("Opening %s: %s\n", play_info.device, g_strerror(errno));
	stop_playing_real();
	return;
    }

    if ( (srcfd = open(output_file, O_RDONLY)) < 0) {
	g_warning("Opening %s: %s\n", output_file, g_strerror(errno));
	stop_playing_real();
	return;
    }

    g_print ("playing to device %s - rate %d - bits %d - channels %d\n",
	     play_info.device, play_info.rate, play_info.bits,
	     play_info.channels);   
    
    set_ALSA_dsp (dspfd, play_info.rate, 
		 play_info.bits, play_info.channels);
    
    play_tag = gdk_input_add (dspfd, GDK_INPUT_WRITE, play_data, NULL);
}

static void stop_playing(IpcProcess *ipcp, gpointer data)
{
    stop_playing_real();
}

static void stop_playing_real (void)
{
    playing = FALSE;
    gdk_input_remove (play_tag);
    close (dspfd);
    close (srcfd);
    
    at_ipc_signal_emit (main_module, "playing_completed");
    
    g_print ("done playing.\n");
}

static void play_data (gpointer data, gint source, 
		       GdkInputCondition condition)
{
    at_sample sample[DSP_BUF_SIZE];
    gint nread;
    gint i;
    gint16 buf[DSP_BUF_SIZE];
    
    nread = read(srcfd, sample, DSP_BUF_SIZE * AT_SAMPLE_SIZE);
    
    /* error occured */
    if (nread < 0) {
	g_warning("Reading from %s: %s\n", output_file,
		  g_strerror(errno));
    }

    /* check if we're done yet. */
    if (nread == 0) {
	stop_playing_real();
	return;
    }

    /* adjust volume */
    for (i=0; i < DSP_BUF_SIZE; i++)
      sample[i] *= play_info.volume;


    AFF_to_native (sample, buf, DSP_BUF_SIZE);
    
    /* adjustment for float to int */
    if (play_info.bits == 8) {
      nread = nread / AT_SAMPLE_SIZE * sizeof (guint8);
    } else {
      nread = nread / AT_SAMPLE_SIZE * sizeof (gint16);
    }

    if ( (write(dspfd, buf, nread)) < 0) {
	g_warning("Writing to %s: %s\n", play_info.device, g_strerror(errno));
	stop_playing_real();
    }
}

static void AFF_to_native (at_sample *sample, guint16 *buf, gint count)
{
    gint i;
    guint8 *buf8 = (guint8 *) buf;

    if (play_info.bits == 8) {
        for (i=0; i < count; i++) {
	  /* it has to be doubled to make up for lost space */
	  buf8[i] = at_convert_AFF_to_8_bit_PCM (sample[i]);
	}
    } else {

	for (i=0; i < count; i++) {
	    buf[i] = at_convert_AFF_to_16_bit_PCM (sample[i]);
	}
    }
}
    


static void set_ALSA_dsp (gint fd, gint rate, 
			 gint bits, gint channels)
{
    snd_pcm_format_t pcm_fmt;

    /* set format of buffer */
    if (bits == 16) {
	pcm_fmt.format = SND_PCM_SFMT_S16_LE;
	/* pcm_fmt.format = SND_PCM_SFMT_U16_LE; */
    } else {
	pcm_fmt.format = SND_PCM_SFMT_S8;
    }
    pcm_fmt.rate = rate;
    pcm_fmt.channels = channels;

    if ( ioctl( fd, SND_PCM_IOCTL_PLAYBACK_FORMAT, &pcm_fmt ) < 0 ) { 
	g_warning ("Could not set play variables: %s",
		   g_strerror(errno));
    }
}

static void play_quit(IpcProcess *ipcp, gpointer data)
{
    gtk_main_quit();
}

/******************************************************
 * 
 * This begins the user interface section.  It only does
 * the configuration for the play server.
 *
 ******************************************************/

GtkWidget *config_window;
GtkWidget *device_entry;
gint config_window_hidden;

static void config_hide (GtkWidget *widget, gpointer data)
{
    gchar *device_text;
    
    gtk_widget_hide (config_window);
    config_window_hidden = TRUE;
    
    device_text = gtk_entry_get_text (GTK_ENTRY (device_entry));
    
    strcpy (play_info.device, device_text);
}

static void set_play_bits (GtkWidget *widget, gpointer data)
{
    gint bits = (gint) data;
    
    play_info.bits = bits;
}

static void set_play_rate (GtkWidget *widget, gpointer data)
{
    gint rate = (gint) data;
    
    play_info.rate = rate;
}

static void set_play_channels (GtkWidget *widget, gpointer data)
{
    gint channels = (gint) data;
    
    play_info.channels = channels;
}


static void configure (IpcProcess *ipcp, gpointer data)
{
    GtkWidget *box1;
    GtkWidget *button;
    GtkWidget *hbox;
    GtkWidget *vbox;
    GtkWidget *frame;
    
    if (config_window_hidden) {
	gtk_widget_show (config_window);
	config_window_hidden = FALSE;
	return;
    }
    
    /* configuration window */
    config_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_signal_connect(GTK_OBJECT(config_window), "delete_event",
		       GTK_SIGNAL_FUNC (config_hide), NULL);
    gtk_signal_connect(GTK_OBJECT(config_window), "destroy",
		       GTK_SIGNAL_FUNC (config_hide), NULL);
    gtk_window_set_title (GTK_WINDOW(config_window), "Play Server Config");
    gtk_container_border_width (GTK_CONTAINER(config_window), 5);

    /* main vbox */
    box1 = gtk_vbox_new (FALSE, 5);
    gtk_container_border_width (GTK_CONTAINER (box1), 10);
    gtk_container_add(GTK_CONTAINER (config_window), box1);
    gtk_widget_show(box1);
    
    /* radio buttons for bits - 16 or 8 */
    /* start with frame */
    frame = gtk_frame_new ("Sample Size");
    gtk_box_pack_start (GTK_BOX (box1), frame, TRUE, TRUE, 0);
    gtk_widget_show (frame);

    vbox = gtk_vbox_new (FALSE, 5);
    gtk_container_border_width (GTK_CONTAINER (vbox), 5);
    gtk_container_add (GTK_CONTAINER (frame), vbox);
    gtk_widget_show (vbox);

    /* 16 bit */
    button = gtk_radio_button_new_with_label (NULL, "16 bit");
    gtk_signal_connect (GTK_OBJECT (button), "clicked", 
			GTK_SIGNAL_FUNC (set_play_bits), (gpointer)16);
    gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (button), TRUE);
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_widget_show (button);
    
    /* 8 bit */
    button = gtk_radio_button_new_with_label (
					      gtk_radio_button_group (GTK_RADIO_BUTTON (button)),
					      "8 bit");
    gtk_signal_connect (GTK_OBJECT (button), "clicked", 
			GTK_SIGNAL_FUNC (set_play_bits), (gpointer)8);
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_widget_show (button);
    
    /* buttons for sample frequency - 44100Khz, 22050Khz, 11025khz*/
    /* radio buttons for bits - 16 or 8 */
    frame = gtk_frame_new ("Sample Frequency");
    gtk_box_pack_start (GTK_BOX (box1), frame, TRUE, TRUE, 0);
    gtk_widget_show (frame);

    vbox = gtk_vbox_new (FALSE, 5);
    gtk_container_border_width (GTK_CONTAINER (vbox), 5);
    gtk_container_add (GTK_CONTAINER (frame), vbox);
    gtk_widget_show (vbox);

    /* 44100 khz */
    button = gtk_radio_button_new_with_label (NULL, "44100 Khz");
    gtk_signal_connect (GTK_OBJECT (button), "clicked", 
			GTK_SIGNAL_FUNC (set_play_rate), (gpointer)44100);
    gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (button), TRUE);
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_widget_show (button);
    
    /* 22050 Khz */
    button = gtk_radio_button_new_with_label (
		      gtk_radio_button_group (GTK_RADIO_BUTTON (button)),
		      "22050 Khz");
    
    gtk_signal_connect (GTK_OBJECT (button), "clicked", 
			GTK_SIGNAL_FUNC (set_play_rate), (gpointer)22050);
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_widget_show (button);

    /* 11025 Khz */
    button = gtk_radio_button_new_with_label (
		      gtk_radio_button_group (GTK_RADIO_BUTTON (button)),
		      "11025 Khz");
    gtk_signal_connect (GTK_OBJECT (button), "clicked", 
			GTK_SIGNAL_FUNC (set_play_rate), (gpointer)11025);
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_widget_show (button);

    /* radio buttons for channels */
    /* start with frame */
    frame = gtk_frame_new ("Channels");
    gtk_box_pack_start (GTK_BOX (box1), frame, TRUE, TRUE, 0);
    gtk_widget_show (frame);

    vbox = gtk_vbox_new (FALSE, 5);
    gtk_container_border_width (GTK_CONTAINER (vbox), 5);
    gtk_container_add (GTK_CONTAINER (frame), vbox);
    gtk_widget_show (vbox);

    /* Stereo */
    button = gtk_radio_button_new_with_label (NULL, "Stereo");
    gtk_signal_connect (GTK_OBJECT (button), "clicked", 
			GTK_SIGNAL_FUNC (set_play_channels), (gpointer)2);
    gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (button), TRUE);
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_widget_show (button);
    
    /* Mono */
    button = gtk_radio_button_new_with_label (
		      gtk_radio_button_group (GTK_RADIO_BUTTON (button)),
		      "Mono");
    gtk_signal_connect (GTK_OBJECT (button), "clicked", 
			GTK_SIGNAL_FUNC (set_play_channels), (gpointer)1);
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_widget_show (button);

    
    /* frame for device entry */
    frame = gtk_frame_new ("Playback device");
    gtk_box_pack_start (GTK_BOX (box1), frame, TRUE, TRUE, 0);
    gtk_widget_show (frame);

    vbox = gtk_vbox_new (FALSE, 5);
    gtk_container_border_width (GTK_CONTAINER (vbox), 5);
    gtk_container_add (GTK_CONTAINER (frame), vbox);
    gtk_widget_show (vbox);
    
    /* device entry */
    device_entry = gtk_entry_new();
    gtk_box_pack_end(GTK_BOX(vbox), device_entry, FALSE, FALSE, 6);
    gtk_entry_set_text(GTK_ENTRY(device_entry), play_info.device);
    gtk_widget_show(device_entry);
        
    /* hbox for close and ___ buttons */
    hbox = gtk_hbox_new (FALSE, 1);
    gtk_box_pack_start(GTK_BOX (box1), hbox, FALSE, FALSE, 0);
    gtk_widget_show (hbox);
    
    /* close button */
    button = gtk_button_new_with_label("Close");
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
			GTK_SIGNAL_FUNC (config_hide), 
			NULL);
    gtk_box_pack_start(GTK_BOX (hbox), button, TRUE, TRUE, 6);
    GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
    gtk_widget_grab_default (button);
    gtk_widget_show (button);

    /* show the window */
    gtk_widget_show (config_window);
}
















#ifndef __AT_VIEW_H__
#define __AT_VIEW_H__
#include <glib.h>
#include <gtk/gtk.h>
#include "at_clip.h"

typedef struct _View View;

struct _View 
{
  Clip *clip;
  
  GtkWidget *toplevel;
  GtkWidget *vbox;
  GtkWidget *track_vbox;
  GtkWidget *track_list;
  GtkWidget *radio_button;
  GtkWidget *toolbar;
  GtkWidget *track_ops_menu;
  GtkWidget *menubar;

  GList *track_items;
};

View *       at_view_new              (Clip *);
void         at_view_delete           (View *);
void         at_view_update           (View *);
void         at_views_flush           ();
void         at_views_update          (gint);

View *       at_view_active           ();

#endif /* __AT_VIEW_H__ */






















#ifndef __ATECH_PLAY_H__
#define __ATECH_PLAY_H__

typedef struct
{
    gchar device[256];
    gint rate;
    gint bits;
    gint channels;
    gfloat volume;
} AtechPlayAttr;

#endif /* __ATECH_PLAY_H__ */

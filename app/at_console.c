#include <gtk/gtk.h>

#include "at_com.h"
#include "at_interface.h"
#include "at_quit.h"
#include "at_console.h"

static gint at_console_delete(GtkWidget *widget, gpointer data);


static gint at_console_delete(GtkWidget *widget, gpointer data)
{
    /* don't let delete destroy the window */
    return (FALSE);
}

void at_console_init(void)
{
  GtkWidget *window;
  GtkWidget *button;
  GtkWidget *hbox;
  
  /* console window */
    window = gtk_window_new (GTK_WINDOW_DIALOG);
    gtk_container_border_width(GTK_CONTAINER(window), 5);
    gtk_signal_connect(GTK_OBJECT(window), "destroy",
		       GTK_SIGNAL_FUNC(at_quit), NULL);
    gtk_signal_connect(GTK_OBJECT(window), "delete_event",
		       GTK_SIGNAL_FUNC(at_console_delete), NULL);
    gtk_window_set_title (GTK_WINDOW(window), "Atech Console");
    gtk_window_position (GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    
    /* hbox for buttons */
    hbox = gtk_hbox_new(TRUE, 1);
    gtk_container_add (GTK_CONTAINER (window), hbox);
    gtk_widget_show (hbox);

    /* Play Button */
    button = at_interface_pixmapped_button_new ("play.xpm", NULL);
    gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, TRUE, 0);
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
			GTK_SIGNAL_FUNC (at_com_play_start), NULL);
    gtk_widget_show (button);
    
    /* Record Button */
    button = at_interface_pixmapped_button_new ("record.xpm", NULL);
    gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
			GTK_SIGNAL_FUNC (at_com_record_start), NULL);
    gtk_widget_show (button);

    /* Stop Button (Now stops both, play and record) */
    button = at_interface_pixmapped_button_new ("stop.xpm", NULL);
    gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
			GTK_SIGNAL_FUNC (at_com_play_stop), NULL);
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
			GTK_SIGNAL_FUNC (at_com_record_stop), NULL);
    gtk_widget_show (button);

    /* show window */
    gtk_widget_show (window);
}










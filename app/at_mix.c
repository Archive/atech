/* Audiotechque -- Multitrack Mixer and Audio manipulation program
 * Copyright (C) 1997-1998 Ian Main
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>
#include "region.h"
#include "at_type.h"
#include "at_track.h"
#include "at_view.h"
#include "at_clip.h"
#include "libatech.h"
#include "at_mix.h"

#define MIX_CHUNK_SIZE 512

static gint
at_idle_mix (gpointer data);

static gint
at_idle_mix_streamlined (gpointer data);

static void 
at_mix_complete (Clip *clip);

static void
at_mix_determine_mix_type (Clip *clip);


typedef struct _Mixinfo Mixinfo;

/* thie "pile 'o' logic function" that attempts to determine the best
   mixing technique.  Note that these only work if a single track has been
   modified.  */

static void
at_mix_determine_mix_type (Clip *clip)
{
    Track *track;
    GSList *list = clip->tracks;
    gint track_count = 0;
    Mix *mix_data = &clip->mix;

    while (list) {
	track = (Track *) list->data;

	/* just ignore muted tracks for now */

	if (track->dirty && !track->is_composite) {
	    
	    switch (track->change) {
	    
	    case AT_TRACK_CHANGED_NONE :
		g_warning ("dirty track with no change marker!\n");
		break;
		
	    case AT_TRACK_CHANGED_NEW_TRACK :
		/* This is a new track that has just been recorded to for
		   the first time. */
		mix_data->mix_type = AT_MIX_ADD_TRACK;
		break;

	    case AT_TRACK_CHANGED_VOLUME :
		mix_data->mix_type = AT_MIX_VOLUME_CHANGED;
		break;
		
	    case AT_TRACK_CHANGED_MULTIPLE :
		mix_data->mix_type = AT_MIX_REMIX_ALL;
		break;

	    case AT_TRACK_CHANGED_MUTED :
		mix_data->mix_type = AT_MIX_SUB_TRACK;
		break;

	    case AT_TRACK_CHANGED_UNMUTED :
		mix_data->mix_type = AT_MIX_ADD_TRACK;
		break;

	    case AT_TRACK_CHANGED_DELETED :
		mix_data->mix_type = AT_MIX_SUB_TRACK;
		if (!track->delete_me)
		    g_warning ("Track marked for deletion, but not queued");
		break;

	    default :
		g_warning ("Unknown track modification type");
		break;
		
	    }

	    mix_data->current_track = track;

	    track_count++;
	    g_print ("DIRTY TRACK - CONDITION %d - TRACK ID %d\n", 
		     track->change, track->ID);
	}
	list = list->next;
    }

    if (track_count > 1)
	mix_data->mix_type = AT_MIX_REMIX_ALL;
}



/* Yes, I know, there is nothing in here with offsets.  I'll have to add
 * it in later, but it works much better than the old code IMHO */

void
at_mix_tracks_in_clip (Clip *clip, size_t offset, size_t length)
{
    Track  *track;
    size_t  x1, x2;
    GSList  *list = clip->tracks;
    GSList  *track_list = NULL;
    gint track_count = 0;
    TmpRegion *mix_dest;
    Mix *mix_data = &clip->mix;
    gint mix_needed = FALSE;
    gint clip_length;

    /* check for dirty tracks to see if we require remixing. */
    while (list) {
	track = (Track *) list->data;

	/* we ignore the composite track.. */
	if (track->dirty && !track->is_composite)
	    mix_needed = TRUE;

	list = list->next;
    }


    if (!mix_needed && !clip->dirty)
	return;

    if (clip->dirty)
	g_print ("clip has been marked dirty...\n");

    /* if we're already in the process of mixing, kill it,
       and restart. */
    if (clip->mixing)
	at_mix_stop_idle_mix(clip);

    clip->mixing = TRUE;

    /* get clip length, and set it.  This is the only place
       the clip length should be set because we're using an
       idle mix, and we may be resizing an mmap'd region */
    clip_length = at_clip_get_composite_length (clip);

    /* resize the region, and set clip->length to match */
    region_resize (clip->composite, 0, clip_length);
    clip->length = clip->composite->length;

    /* map the clip composite track */
    mix_dest = tmp_region_new (clip->composite, 0, clip->length);


    /* ---- Check if we can use streamlined mixing --- */

    /* if there are dirty tracks, determine if we can use one of the 
       shortcut mixing methods. */
    at_mix_determine_mix_type(clip);
    
    /* if there haven't been multiple changes, do the streamlined mix */
    if ( (mix_data->mix_type != AT_MIX_REMIX_ALL) && !clip->dirty) {

	g_print ("Streamlined mix....\n");


	mix_data->track_count = 1;
	mix_data->track_list = NULL;
	mix_data->list = NULL;
	mix_data->position = 0;
	mix_data->composite_region = mix_dest;
	mix_data->track_region = tmp_region_new (mix_data->current_track->data, 0,
						 mix_data->current_track->length);
	mix_data->bps = clip->composite->bps;

	/* set up the idle to mix the tracks */
	mix_data->record_tag = gtk_idle_add (at_idle_mix_streamlined, clip);

	return;
    }


    /* -- Streamlined mixing is not possible, mix all tracks ----- */

			
    g_print ("mixing all tracks.... \n");
				     
    /* Create a list of all tracks that need mixing */
    list = clip->tracks;
    while (list) {
	track = (Track *) list->data;
	
	if (!track->mute && !track->is_composite) {
	    track_list = g_slist_prepend (track_list, track);
	    track_count ++;
	}
	
	list = list->next;
    }

    /* zero out the composite */
    tmp_region_zero (mix_dest);

    mix_data->track_count = track_count;
    mix_data->track_list = track_list;
    mix_data->list = track_list;
    mix_data->position = 0;
    mix_data->current_track = NULL;
    mix_data->composite_region = mix_dest;
    mix_data->track_region = NULL;
    mix_data->bps = clip->composite->bps;

    /* set up the idle to mix the tracks */
    mix_data->record_tag = gtk_idle_add (at_idle_mix, clip);
}

/* this mixes all tracks by adding one at a time to the composite track. */

static gint
at_idle_mix (gpointer data)
{
    Clip *clip = data;
    Mix *mix = &clip->mix;
    Track *track = NULL;
    gint track_done = FALSE;
    gint count, i;
    atdata *dest_data, *src_data;
    gint track_size;
    
    /* check if there's a track on the queue */
    if (!mix->current_track) {

	/* if we're at the end of the track list, 
	   we're done mixing */
	if (mix->list == NULL) {
	    /* we have now completed the mixing */
	    dest_data = mix->composite_region->data;
	    track_size = mix->composite_region->bps * mix->composite_region->length / 
		sizeof (atdata);
	    
	    g_print ("Idle mixing completed... \n");
	    
	    g_slist_free (mix->track_list);
	    tmp_region_destroy (mix->composite_region);
      
	    at_mix_complete(clip);
      
	    gtk_idle_remove (mix->record_tag);
	    return (FALSE);
	}

	/* get next track from linked list */
	mix->current_track = mix->list->data;

	track = mix->current_track;

	g_print ("queuing track '%s' for mixing ..\n", track->data->filename);

	/* map track */
	mix->track_region = tmp_region_new (track->data, 0,
					    track->length);
	/* since this is a new track, set the position to 0 */
	mix->position = 0;
	mix->list = mix->list->next;
    }
    
    track = mix->current_track;

    track_size = track->data->bps * track->length / sizeof (atdata);

    count = MIX_CHUNK_SIZE * mix->bps;

    /*
      g_print ("Mixing - chunk count %d - position %d - track size %d\n", 
      count, mix->position, track_size);
    */
    if (count + mix->position > track_size) {
	count = track_size - mix->position;
	track_done = TRUE;
    }

    dest_data = mix->composite_region->data;
    dest_data += mix->position;
    src_data = mix->track_region->data;
    src_data += mix->position;
  
    mix->position += count;

    /* perform the actual mixing */
    /* FIXME: handle number of channels correctly */
    for (i=0; i < count; i++) {
	dest_data[i] += (src_data[i] * track->volume);
    }


    if (track_done) {
	tmp_region_destroy (mix->track_region);
	track->dirty = FALSE;
	track->change = AT_TRACK_CHANGED_NONE;
	mix->current_track = NULL;
	mix->track_region = NULL;
    }

    return (TRUE);
}

/* this only performs a specialized mixing type - eg, adding one newly
   recorded track to the composite, or subtracting a track from the 
   composite that will be deleted etc. */

static gint
at_idle_mix_streamlined (gpointer data)
{
    Clip *clip = data;
    Mix *mix = &clip->mix;
    Track *track = mix->current_track;
    gint count, i;
    atdata *dest_data, *src_data;
    gint mix_done = FALSE;
    gint track_size;

    track_size = track->data->bps * track->length / sizeof (atdata);
    count = MIX_CHUNK_SIZE * mix->bps;

    if (count + mix->position > track_size) {
	count = track_size - mix->position;
	mix_done = TRUE;
    }
    
    dest_data = mix->composite_region->data;
    dest_data += mix->position;
    src_data = mix->track_region->data;
    src_data += mix->position;
  
    mix->position += count;

    /* perform the actual mixing */
    switch (mix->mix_type) {
	
    case AT_MIX_ADD_TRACK :
	/* simply add the new track data to the composite */
	for (i=0; i < count; i++) {
	    dest_data[i] += (src_data[i] * track->volume);
	}
	break;

    case AT_MIX_SUB_TRACK :
	/* subtrack track from composite */
	for (i=0; i < count; i++) {
	    dest_data[i] -= (src_data[i] * track->volume);
	}
	break;

    case AT_MIX_VOLUME_CHANGED :
	/* subtrack the old track * volume from the composite, and then
	   add the new track * volume. */
	for (i=0; i < count; i++) {
	    dest_data[i] -= (src_data[i] * track->old_volume);
	    dest_data[i] += (src_data[i] * track->volume);
	}
	break;

    default:
	g_warning ("Request to streamline mix an unknown type");
    }


    if (mix_done) {

	tmp_region_destroy (mix->track_region);
	tmp_region_destroy (mix->composite_region);
	mix->track_region = NULL;
	mix->composite_region = NULL;

	/* check if this track is queued for deletion */
	if (mix->current_track->delete_me)
	    at_track_delete (mix->current_track);


	track->dirty = FALSE;
	track->change = AT_TRACK_CHANGED_NONE;

	mix->current_track = NULL;
	
	at_mix_complete(clip);
      
	gtk_idle_remove (mix->record_tag);
	return (FALSE);

    }


    return(TRUE);
}



void
at_mix_stop_idle_mix(Clip *clip)
{
    Mix *mix;
    mix = &clip->mix;

    g_print ("KILLING IDLE MIX \n");

    if (clip->mixing == FALSE)
	return;
  
    clip->mixing = FALSE;

    /* since mixing was not complete, we have to remark 
       the entire clip as dirty */
    clip->dirty = TRUE;

    /* delete track if necessary. */
    if (mix->current_track->delete_me)
	at_track_delete (mix->current_track);
    else if (mix->track_region)
	tmp_region_destroy (mix->track_region);

    if (mix->composite_region)
	tmp_region_destroy (mix->composite_region);



    if (mix->track_list)
	g_slist_free (mix->track_list);

    gtk_idle_remove (mix->record_tag);
}


static void 
at_mix_complete (Clip *clip)
{
    clip->composite_track->length = clip->composite->length;
    at_track_build_preview (clip->composite_track);
    at_track_adjust_volume_of_preview (clip->composite_track, 1.0, 
				       clip->composite_track->volume); 
    at_track_update_preview (clip->composite_track);
    clip->mixing = FALSE;
    clip->dirty = FALSE;
}






/* Audiotechque -- Multitrack Mixer and Audio manipulation program
 * Copyright (C) 1997-1998 Larry Ewing
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "glib.h"
#include "at_type.h"

gint
at_type_channels (gint type)
{
  switch (type)
    {	
    case AT_MONO:
      return 1;	
      break;	
    case AT_MONOA:
    case AT_RL:
      return 2;      
      break;
    case AT_RLA:
      return 3;
      break;
    case AT_RLFR:
      return 4;
      break;
    case AT_RLFRA:
      return 5;
      break;
    default:
      g_warning ("type_channels: Unknown type");
      break;
    }
  return 0;
}

gint 
at_type_bytes (gint type)
{		
  gint size = sizeof (atdata);
  gint num_channels;
  
  num_channels = at_type_channels (type);

  return num_channels * size;
}

gint
at_type_alpha (gint type)
{
  switch (type)
    {
    case AT_MONO:
    case AT_RL:
    case AT_RLFR:
      return 0;	
      break;	
    case AT_MONOA:
      return 1;      
      break;
    case AT_RLA:
      return 2;
      break;
      return 3;
      break;
    case AT_RLFRA:
      return 4;
      break;
    default:
      g_warning ("type_channels: Unknown type");
      break;
    }
  return 0;
}




/* Audiotechque -- Multitrack Mixer and Audio manipulation program
 * Copyright (C) 1997-1998 Larry Ewing and Ian Main
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include "region.h"
#include "at_type.h"
#include "at_track.h"
#include "at_view.h"
#include "at_delay.h"
#include "libatech.h"

/* I don't like this.. it needs to be in the clip structure and the clip
   needs to be obtained elsewhere... */
static gchar *record_track = NULL;

static void at_track_mute_cmd_callback(GtkWidget *widget, gpointer *data);

/* imported from at_interface.c. */
extern GtkWidget *scrolled_window_vbox;

/* imported from at_clip.c */
extern int global_clip_ID;


GSList *track_list = NULL;

Track *
at_track_new (gint clip_ID,
	      size_t offset,
	      size_t length, 	      
	      gint type,
	      gchar *name, 
	      gfloat volume,
	      gint mode)
{
  Track *new_track;
  
  new_track = g_malloc0 (sizeof (Track));
    
  new_track->name = g_strdup (name);
  new_track->mute = FALSE;

  /* the track adjustments */
  new_track->adjustments = NULL;

  new_track->length = length;
  new_track->type = type;
  new_track->start_offset = offset;

  new_track->bytes = at_type_bytes (type);
  new_track->volume = volume;
  new_track->mode = mode;
  
  new_track->ID = global_clip_ID++;  
  new_track->clip_ID = clip_ID;

  new_track->data = region_new (length, new_track->bytes);

  /* preview stuff for wave widget */
  new_track->preview[0] = NULL;
  new_track->preview[1] = NULL;
  new_track->preview_len = 0;
  new_track->preview_compression = 800;
  new_track->preview_dirty = FALSE;

  new_track->dirty = FALSE;
  new_track->change = AT_TRACK_CHANGED_NONE;
  new_track->is_composite = FALSE;

  track_list = g_slist_append (track_list, new_track);

  return new_track;
}  

void
at_track_delete (Track *track)
{
    g_free (track->name);
    
    region_delete (track->data);
    /*
      while (track->adjuments)
      {
      adjustment_delete (track->adjustment);
      }
    */
    track_list = g_slist_remove (track_list, track);
    g_free (track);
}


Track *
at_track_get_ID (gint ID)
{
  GSList *tmp = track_list;
  Track *track;

  while (tmp) 
    {
      track = (Track *) tmp->data;
      if (track->ID == ID)
	return track;

      tmp = tmp->next;
    }

  return NULL;
}

void
at_track_translate (Track *track, size_t new_offset)
{
  track->start_offset += new_offset;
  at_track_mark_dirty (track, AT_TRACK_CHANGED_OFFSET); 
  at_track_invalidate_thumbnail (track->ID);
}

void
at_track_invalidate_thumbnail (int track_ID)
{
  /* FIXME: should really do something here :) */
}

gint 
at_track_has_alpha (Track *track)
{
  if (track->type == AT_RLA || 
      track->type == AT_MONOA || 
      track->type == AT_RLFRA)
    return TRUE;
  else
    return FALSE;
}

void
at_track_mark_dirty (Track *track, TrackChange change)
{
    if ( (track->change != AT_TRACK_CHANGED_NONE) && 
	 (track->change != change) ) {
	track->change = AT_TRACK_CHANGED_MULTIPLE;
    }

    if (track->change == AT_TRACK_CHANGED_NONE)
	track->change = change;

    track->dirty++;
}


void
at_track_build_preview (Track *track)
{
  gint i;
  gint preview_len;
  atdata *track_data;
  gint num_channels;
  TmpRegion *region;

  /* this isn't working - segv *shrug* */
  /* track_data = at_track_get_track_data (track); */

  region = tmp_region_new (track->data, 0, track->data->length);

  track_data = region->data;

  if (track->preview[0])
    g_free(track->preview[0]);
  
  if (track->preview[1])
    g_free(track->preview[1]);
  
  track->preview[0] = NULL;
  track->preview[1] = NULL;
  track->preview_len = 0;

  if (track_data == NULL) 
    return;

  preview_len = track->length / track->preview_compression;


  num_channels = at_type_channels (track->type);
  g_print ("preview_len is %d, compression is %d, track len is %d, num_channels is %d\n", 
	   preview_len, track->preview_compression, track->length, num_channels);

  track->preview_len = preview_len;


  if (num_channels == 2) {

    track->preview[0] = g_malloc (preview_len * sizeof (gfloat)); 
    track->preview[1] = g_malloc (preview_len * sizeof (gfloat)); 
    
    for (i=0; i < preview_len; i++) {
      track->preview[0][i] = track_data[i * track->preview_compression];
      track->preview[1][i] = track_data[i * track->preview_compression + sizeof (gfloat)];
    }
  } else {
    
    track->preview[0] = g_malloc (preview_len * sizeof (gfloat)); 

    for (i=0; i < preview_len; i++) {
      track->preview[0][i] = track_data[i * track->preview_compression];
    }
  }

  tmp_region_destroy (region);
}

void
at_track_update_preview (Track *track)
{
  /* left and right previews */
  if (track->preview[0])
    gtk_wave_set_data (GTK_WAVE (track->wave_widget), 0, 
		       track->preview[0], track->preview_len);  
  if (track->preview[1])
    gtk_wave_set_data (GTK_WAVE (track->wave_widget), 1, 
		       track->preview[1], track->preview_len);  
}

void
at_track_new_cmd_callback (GtkWidget *widget, gpointer data)
{
  View  *view;
  Track *track;

  /* FIXME this is ugly, but it's relatively sane from the moment I think
     god I am writting some ugly code right now... --Larry */
  if (data != NULL)
    view = (View *)data;
  else
    view = at_view_active ();

  /* FIXME: need to make sure we handle the pathological cases... */
  /* FIXME: we need to build a new_track dialog here... :) */
  track = at_track_new (view->clip->ID, 0, 0, AT_RL, "New Track", 1.0, 0);
  
  at_clip_add_track (view->clip, track, 
		     at_clip_get_track_index (view->clip,
					      view->clip->active_track));
  at_views_update (view->clip->ID);
}

/* TODO: I'm going to create a at_mix.c because the amount of
   logic is going to grow immensly with smart mixing.
   One of the things I'm going to do is just have this track
   marked for deletion.  That way we can just subtract it
   from the composite, and then actually deleted it... much
   faster than remixing all the tracks. */

void
at_track_delete_cmd_callback (GtkWidget *widget, gpointer data)
{
    View *view;
    Track *track = NULL;
    
    /* FIXME this is ugly, but it's relatively sane from the moment I think.
       god I am writting some ugly code right now... --Larry */
    if (data != NULL)
	view = (View *)data;
    else
	view = at_view_active ();
    
    if ((track = at_track_get_ID (view->clip->active_track))) {
	    /* can't delete the composite */
	    if (track->is_composite)
		return;

	    at_track_mark_dirty (track, AT_TRACK_CHANGED_DELETED);
	    
	    /* we just remove it from the view here.  We don't actually
	       delete it till we're done subtracting it from the composite. */
	    at_clip_remove_track (view->clip, view->clip->active_track);
	}
    
    at_views_update (view->clip->ID);
}


void 
at_track_raise_cmd_callback (GtkWidget *widget, gpointer data)
{
  View  *view;

  if (data != NULL)
    view = data;
  else
    view = at_view_active ();
  
  at_clip_raise_track (view->clip, view->clip->active_track);
  at_views_update (view->clip->ID);
}
  
void
at_track_lower_cmd_callback (GtkWidget *widget, gpointer data)
{
  View *view;
  
  if (data != NULL)
    view = data;
  else
    view = at_view_active ();
  
  at_clip_lower_track (view->clip, view->clip->active_track);
  at_views_update (view->clip->ID);
}


gchar *
at_track_get_record_track(void)
{
  /* FIXME I don't like the record_track temp storage... grrr... */
  /* 
   * eventually the record stuff should probably go into a floating selection 
   */
  if (record_track)
    {
      g_free (record_track);
      record_track = NULL;
    }

  /* I changed this to /tmp for now.. *shrug* */
  return (record_track = tempnam ("./", "atrec"));
}

/* adjust the preview based on a new volume */
void
at_track_adjust_volume_of_preview (Track *track, gfloat old_volume, gfloat new_volume)
{
  gfloat diff;
  gint i;

  diff = new_volume / old_volume;

  for (i=0; i < track->preview_len; i++) {
    track->preview[0][i] *= diff;
    track->preview[1][i] *= diff;
  }
}

void
at_track_record_done (Clip *clip)
{
    Track *track;
    /* the default */
    TrackChange change = AT_TRACK_CHANGED_MULTIPLE;

    /* ignore most of the crap in here, it's just test stuff */
    track = at_track_get_ID (clip->active_track);
    
    if (track) {
	AudioRegion *old;
	
	old = track->data;

	/* If the track was of 0 length before, we can just add it to the composite. */
	if (old->length == 0)
	    change = AT_TRACK_CHANGED_NEW_TRACK;

	track->data = region_new_from_file (record_track,
					    at_type_bytes (track->type));
	if (!track->data) {
	    track->data = old;
	    g_warning ("why no file????");
	} else {
	    track->length = track->data->length;
	    g_print ("new length = %d, old->length = %d track_ID %d\n", track->length, 
		     old->length, track->ID);
	    region_delete (old);
	}
    } else {
	g_warning ("screwed up record track");
    }
    
    /* track->preview_dirty = TRUE; */
    at_track_mark_dirty (track, change);
    at_track_build_preview (track);
    at_track_update_preview (track);
}


/* GUI section - from here on, it's all callbacks and GUI code. */
static void 
at_track_mute_cmd_callback (GtkWidget *widget, gpointer *data)
{
    gint track_ID = (gint) data;
    Track *track;
    Clip *clip;
    gint mute;

    track = at_track_get_ID (track_ID);

    if (track->is_composite) 
	return;
    
    if (GTK_TOGGLE_BUTTON (widget)->active) {
      g_print ("muting track %d\n", track_ID);
      mute = TRUE;
    } else {
      g_print ("un-muting track %d\n", track_ID);
      mute = FALSE;
    }

    if (mute == TRUE)
	at_track_mark_dirty (track, AT_TRACK_CHANGED_MUTED);
    else
	at_track_mark_dirty (track, AT_TRACK_CHANGED_UNMUTED);

    clip = at_clip_get_ID (track->clip_ID);

}

static void 
at_track_volume_changed_cmd_callback (GtkWidget *widget, gpointer *data)
{
    gint track_ID = (gint) data;
    gint range;
    gfloat volume;
    Track *track;
    GtkWidget *spinner;
    Clip *clip;

    track = at_track_get_ID (track_ID);
    
    spinner = track->spinner;
    
    range = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (spinner));

    volume = (gfloat) range / 100.0;
    
    g_print ("track->volume = %f, spinner volume = %f", track->volume, volume/ 100);
    /* rebuild the preview by adjusting from new volume */
    at_track_adjust_volume_of_preview (track, track->volume, volume);
    at_track_update_preview (track);

    /* we're doing a bit of a performance hack here.
       If the volume has already been changed, then
       track->changed will be AT_TRACK_CHANGED_VOLUME, 
       so we just leave it, and wait for the idle mix 
       to be called. */
    if (track->change != AT_TRACK_CHANGED_VOLUME) {
	track->old_volume = track->volume;
	at_track_mark_dirty(track, AT_TRACK_CHANGED_VOLUME);
    }

    track->volume = volume;

    clip = at_clip_get_ID (track->clip_ID);

    /* a delayed update, so mixing does not cause poor interactivity. */
    if (!track->is_composite) 
      at_delay (1000, at_clip_delayed_update, clip);
}

GtkWidget *at_track_gui_new (View *view, gint id)
{
    GtkWidget *main_hbox;
    GtkWidget *button;
    GtkWidget *frame;
    GtkWidget *hbox;
    GtkWidget *vbox;
    GtkWidget *spinner;
    GtkAdjustment *adj;
    gchar buf[128];
    Track *track;

    track = at_track_get_ID (id);
    /* first, the top hbox */
    main_hbox = gtk_hbox_new (FALSE, 5);
    gtk_container_border_width (GTK_CONTAINER (main_hbox), 5);
    
    /* frame for the whole track. */
    sprintf(buf,"%s (%d)", track->name, track->ID);
    frame = gtk_frame_new (buf);
    gtk_box_pack_start (GTK_BOX (main_hbox), frame, TRUE, TRUE, 0);

    
    hbox = gtk_hbox_new (FALSE, 5);
    gtk_container_border_width (GTK_CONTAINER (hbox), 5);
    gtk_container_add (GTK_CONTAINER (frame), hbox);

    vbox = gtk_vbox_new (FALSE, 5);
    gtk_container_border_width (GTK_CONTAINER (vbox), 5);
    gtk_box_pack_start (GTK_BOX (hbox), vbox, FALSE, FALSE, 2);

    /* the mute button */
    button = gtk_toggle_button_new_with_label ("Mute");
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
			GTK_SIGNAL_FUNC (at_track_mute_cmd_callback),
			(gpointer) id);
    gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (button),
				 track->mute ? TRUE: FALSE);
    gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 2);
    
    /* spin button for wave range */
    adj = (GtkAdjustment *) gtk_adjustment_new (track->volume * 100, 
						5.0, 800.0, 5.0, 
						5.0, 0.0);
    spinner = track->spinner = gtk_spin_button_new (adj, 0, 0);
    gtk_box_pack_start (GTK_BOX (vbox), spinner, FALSE, TRUE, 0);
    gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
			GTK_SIGNAL_FUNC (at_track_volume_changed_cmd_callback),
			(gpointer) id);

    /* wave widget */
    track->wave_widget = gtk_wave_new ();
    gtk_widget_set_usize (GTK_WIDGET(track->wave_widget), -1, 100);
    gtk_wave_set_vertical_range (GTK_WAVE (track->wave_widget), 2);
    gtk_box_pack_start (GTK_BOX (hbox), track->wave_widget, TRUE, TRUE, 2);

    at_track_update_preview (track);

    gtk_object_set_user_data (GTK_OBJECT (main_hbox), (gpointer) id);

    gtk_widget_show_all (main_hbox);
    return (main_hbox);
}















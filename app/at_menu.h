#ifndef __AT_MENU_H__
# define __AT_MENU_H__

void at_menu_new (GtkWidget **menubar, GtkAccelGroup **table, 
		  GtkMenuEntry *m_items, gint nm_items);
void at_menu_create (GtkMenuFactory *, GtkMenuEntry *entries, int nmenu_entries);
    
#endif /* __AT_MENU_H__ */

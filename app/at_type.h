
#ifndef __AT_TYPE_H__
#define __AT_TYPE_H__
#include <glib.h>

typedef gfloat atdata;

/* the clip types */

#define AT_RL        0
#define AT_RLA       1 
#define AT_MONO      2
#define AT_MONOA     3
#define AT_RLFR      4
#define AT_RLFRA     5

#define MAX_CHANNELS 16

gint at_type_bytes (gint);
gint at_type_channels (gint);
gint at_type_alpha (gint);

#endif /* __AT_TYPE_H__ */



















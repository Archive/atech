#include <gtk/gtk.h>
#include "at_track.h"
#include "at_view.h"
#include "at_clip.h"

static void   fileop_saveas_okay   (GtkWidget *, GtkWidget *);
static void   fileop_saveas_cancel (GtkWidget *, GtkWidget *);
static void   fileop_open_okay   (GtkWidget *, GtkWidget *);
static void   fileop_open_cancel (GtkWidget *, GtkWidget *);

void
at_fileop_saveas_cmd (GtkWidget *widget, gpointer data)
{
  GtkWidget *filesel;
  View *view = data;

  if (!view)
    view = at_view_active ();

  filesel = gtk_file_selection_new ("Save As");
  
  gtk_object_set_user_data (GTK_OBJECT (filesel), view->clip);
  gtk_window_position (GTK_WINDOW (filesel), GTK_WIN_POS_MOUSE);

  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (filesel)->ok_button),
		      "clicked", GTK_SIGNAL_FUNC (fileop_saveas_okay),
		      (gpointer) filesel);
  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (filesel)->cancel_button),
		      "clicked", GTK_SIGNAL_FUNC (fileop_saveas_cancel),
		      (gpointer) filesel);
  gtk_widget_show (filesel);
}

static void 
fileop_saveas_okay (GtkWidget *button, GtkWidget *filesel)
{
  Clip *clip;
  gchar *filename =  gtk_file_selection_get_filename(GTK_FILE_SELECTION (filesel));
  
  clip = (Clip *) gtk_object_get_user_data (GTK_OBJECT (filesel));
  atf_save (clip, filename);

  gtk_widget_destroy (filesel);
}

static void
fileop_open_cancel (GtkWidget *button, GtkWidget *filesel)
{
  gtk_widget_destroy (filesel);
}

void 
at_fileop_open_cmd (GtkWidget *widget, gpointer data)
{
  View *view = data;
  GtkWidget *filesel;

  if (!view)
    view = at_view_active ();

  filesel = gtk_file_selection_new ("Open");
  
  gtk_window_position (GTK_WINDOW (filesel), GTK_WIN_POS_MOUSE);

  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (filesel)->ok_button),
		      "clicked", GTK_SIGNAL_FUNC (fileop_open_okay),
		      (gpointer) filesel);
  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (filesel)->cancel_button),
		      "clicked", GTK_SIGNAL_FUNC (fileop_open_cancel),
		      (gpointer) filesel);
  gtk_widget_show (filesel);
}  

static void 
fileop_open_okay (GtkWidget *button, GtkWidget *filesel)
{
  Clip *clip;
  gchar *filename =  gtk_file_selection_get_filename(GTK_FILE_SELECTION (filesel));
  
  if ((clip = atf_load (filename)))
    {
     at_view_new (clip);
     at_views_update (clip->ID);
    }
  
  gtk_widget_destroy (filesel);
}

static void
fileop_saveas_cancel (GtkWidget *button, GtkWidget *filesel)
{
  gtk_widget_destroy (filesel);
}








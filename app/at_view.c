/* Audiotechque -- Multitrack Mixer and Audio manipulation program
 * Copyright (C) 1997-1998 Larry Ewing and Ian Main
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glib.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include <libatech.h>
#include <gnome.h>
#include "at_menu.h"
#include "at_track.h"
#include "at_view.h"
#include "at_clip.h"
#include "region.h"
#include "at_com.h"
#include "at_interface.h"
#include "at_quit.h"
#include "at_fileop.h"

static GSList *view_list = NULL;
extern gint global_clip_ID;

static GnomeUIInfo clip_file_menu[] = {
  {GNOME_APP_UI_ITEM,
   N_("New Clip"),
   NULL, 
   at_clip_new_cmd_callback,
   NULL,
   NULL, 
   GNOME_APP_PIXMAP_STOCK,
   GNOME_STOCK_MENU_NEW,
   'N',
   GDK_CONTROL_MASK,
   NULL},

  {GNOME_APP_UI_ITEM,
   N_("Open..."),
   NULL,
   at_fileop_open_cmd,
   NULL,
   NULL,
   GNOME_APP_PIXMAP_STOCK, 
   GNOME_STOCK_MENU_OPEN, 
   'O', 
   GDK_CONTROL_MASK,
   NULL},
  
  {GNOME_APP_UI_SEPARATOR},
  
  /* {GNOME_APP_UI_ITEM, N_("Close"), NULL, } */

  {GNOME_APP_UI_ITEM,
   N_("Quit"),
   NULL, 
   at_quit,
   NULL, 
   NULL, 
   GNOME_APP_PIXMAP_STOCK,
   GNOME_STOCK_MENU_QUIT,
   'Q', 
   GDK_CONTROL_MASK, 
   NULL},

  {GNOME_APP_UI_ENDOFINFO}
};

static GnomeUIInfo clip_track_menu[] = {
  {GNOME_APP_UI_ITEM, N_("New Track"), NULL, at_track_new_cmd_callback, NULL,
   NULL, GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW, 'T',
   GDK_CONTROL_MASK, NULL},
  {GNOME_APP_UI_ITEM, N_("Raise"), NULL, at_track_raise_cmd_callback, NULL,
   NULL, GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK, 'R',
   GDK_CONTROL_MASK, NULL},
  {GNOME_APP_UI_ITEM, N_("Lower"), NULL, at_track_lower_cmd_callback, NULL,
   NULL, GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK, 'L',
   GDK_CONTROL_MASK, NULL}, 
  {GNOME_APP_UI_ITEM, N_("Delete"), NULL, at_track_delete_cmd_callback, NULL,
   NULL, GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK, 'D',
   GDK_CONTROL_MASK, NULL},
 
  {GNOME_APP_UI_ENDOFINFO}
};

static GnomeUIInfo clip_main_menu[] = {
  GNOMEUIINFO_SUBTREE(N_("File"), clip_file_menu),
  GNOMEUIINFO_SUBTREE(N_("Track"), clip_track_menu),
  GNOMEUIINFO_END 
};

gint nclip_file_menu = sizeof(clip_file_menu) / sizeof(clip_file_menu [0]);
gint nclip_track_menu = sizeof(clip_track_menu) / sizeof(clip_track_menu [0]);

static GtkItemFactoryEntry track_ops[] =
{
  { "/New Track", "<control>N", at_track_new_cmd_callback, 0 },
  { "/Raise Track", "<control>F", at_track_raise_cmd_callback, 0 },
  { "/Lower Track", "<control>B", at_track_lower_cmd_callback, 0 },
  { "/Delete Track", "<control>D", at_track_delete_cmd_callback, 0 }
};

static guint n_track_ops = sizeof (track_ops) / sizeof (track_ops[0]);
static GtkItemFactory *track_ops_factory = NULL;

static gint at_view_track_list_events (GtkWidget *, GdkEvent *); 

/*
 * This is broken because it is either just returning the the top
 * of the list or it is returning the view based on the parent of the
 * the current event which may or may not still be around.  the gimp's
 * version of this has the second problem, and it can be fatal.... gr....
 * it's possible to fix this by adding an enter notify to the view windows, 
 * I think, but that is not yet implemented...
 */

View *
at_view_active ()
{

  /* FIXME this should really be based on an active view setting... grr... */
  if (view_list)
    return (View *)view_list->data;
  else 
    return NULL;
}

gint
at_view_delete_callback (GtkWidget *toplevel, GdkEvent *e, gpointer data)
{
  View *view;

  view = (View *) gtk_object_get_user_data (GTK_OBJECT (toplevel));
  
  at_view_delete (view);
  
  return TRUE;
}

static void
at_view_stop_play_record_cmd (GtkWidget *widget, gpointer data)
{
    at_com_play_stop (NULL, data);
    at_com_record_stop (NULL, data);
}

static void
at_view_start_play_record_cmd (GtkWidget *widget, gpointer data)
{
    at_com_play_start (NULL, data);
    at_com_record_start (NULL, data);
}

static void
at_view_record_start_cmd (GtkWidget *widget, gpointer data)
{
    View *view = data;
    Track *track;
    
    /* TODO: Make a warning box come up - 
       'You cannot record to the composite track' */
    track = at_track_get_ID (view->clip->active_track);
    if (track->is_composite)
	return;

    at_com_record_start(NULL, data);
}

void 
at_view_create_dialog (View *view) 
{
  GtkWidget *window;
  /* GtkWidget *table; */
  GtkWidget *vbox;
  GtkWidget *toolbar;
  GtkWidget *menubar;
  GtkAccelGroup *accel;
  GtkWidget *scrolled_window;
  gint i;
  
  /* fill in the callback data */
  for (i = 0; i < nclip_file_menu; i++)
    clip_file_menu[i].user_data = (gpointer)view;

  for (i = 0; i < nclip_track_menu; i++)
    clip_track_menu[i].user_data = (gpointer)view;

  /* the toplevel window */
  /* view->toplevel = window = gtk_window_new (GTK_WINDOW_TOPLEVEL);*/
  view->toplevel = window = gnome_app_new ("Atech Clip", "TESTING");
  gtk_widget_set_usize (window, 500, 400);
  gtk_window_set_title (GTK_WINDOW (window), view->clip->title);
  gtk_window_set_wmclass (GTK_WINDOW (window), "audio_view", "atech");
  gtk_object_set_user_data (GTK_OBJECT (window), (gpointer) view);
  gtk_widget_set_events (GTK_WIDGET (window), GDK_POINTER_MOTION_MASK |
			 GDK_POINTER_MOTION_HINT_MASK);
  gtk_widget_show (view->toplevel);


  gtk_signal_connect (GTK_OBJECT (window), "delete_event", 
		      GTK_SIGNAL_FUNC (at_view_delete_callback), NULL);

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (at_view_delete_callback), NULL);

  /*  the vbox to separate the table and the list */
  vbox = gtk_vbox_new (FALSE, 1);
  /* gtk_container_add (GTK_CONTAINER (window), vbox);*/
  gnome_app_set_contents (GNOME_APP (window), vbox);
  gtk_widget_show (vbox);

  /* menubar */
  gnome_app_create_menus (GNOME_APP(window), clip_main_menu);

  /* popup menu */
  track_ops_factory = gtk_item_factory_new(GTK_TYPE_MENU, "<Popup>", NULL);
  gtk_item_factory_create_items_ac(track_ops_factory, n_track_ops, track_ops, NULL, 2);

  /* toolbar */
  view->toolbar = toolbar = gtk_toolbar_new (GTK_ORIENTATION_HORIZONTAL, GTK_TOOLBAR_BOTH);
  
  gtk_toolbar_append_item (GTK_TOOLBAR (toolbar),
			   "Play", "Start playing currently selected tracks", NULL,
			   at_interface_pixmap_new (view->toplevel, "play.xpm"),
			   (GtkSignalFunc) at_com_play_start, (gpointer) view);

  gtk_toolbar_append_item (GTK_TOOLBAR (toolbar),
			   "Record", "Start Recording", NULL,
			   at_interface_pixmap_new (view->toplevel, "record.xpm"),
			   (GtkSignalFunc) at_view_record_start_cmd, (gpointer) view);

  gtk_toolbar_append_item (GTK_TOOLBAR (toolbar),
			   "Stop", "Stop Playing and/or Recording", NULL,
			   at_interface_pixmap_new (view->toplevel, "stop.xpm"),
			   (GtkSignalFunc) at_view_stop_play_record_cmd, (gpointer) view);

  gtk_toolbar_append_item (GTK_TOOLBAR (toolbar),
			   "Play & Record", 
			   "For full duplex, simultaneously play mixed tracks, and record", 
			   NULL,
			   at_interface_pixmap_new (view->toplevel, "play_record.xpm"),
			   (GtkSignalFunc) at_view_start_play_record_cmd, (gpointer) view);
  gnome_app_set_toolbar (GNOME_APP (window), toolbar);

  gtk_widget_show (toolbar);

  /* scrolled_window */
  scrolled_window = gtk_scrolled_window_new(NULL, NULL);
  gtk_box_pack_start(GTK_BOX (vbox), scrolled_window, TRUE, TRUE, 0);
  gtk_widget_show (scrolled_window);

  /* list */
  view->track_list = gtk_list_new ();
  gtk_container_add (GTK_CONTAINER (scrolled_window), view->track_list); 
  gtk_list_set_selection_mode (GTK_LIST (view->track_list),
			       GTK_SELECTION_BROWSE);
  gtk_signal_connect (GTK_OBJECT (view->track_list), "event", 
		      GTK_SIGNAL_FUNC (at_view_track_list_events), NULL);
  gtk_widget_show (view->track_list);

  view->radio_button = gtk_radio_button_new_with_label (NULL, "record");
  
}

View *
at_view_new (Clip *clip)
{
  View *view;
  
  view = (View *) g_malloc (sizeof (View));
  view->clip = clip;
  view_list = g_slist_prepend (view_list, view);
  
  view->track_items = NULL;
    
  at_view_create_dialog (view);

  return view;
}

static gint
at_view_track_list_events (GtkWidget *widget,
			   GdkEvent  *event)
{
  GdkEventKey *kevent;
  GdkEventButton *bevent;
  GtkWidget *event_widget;
  View *view;

  /*
  if (GTK_IS_LIST (widget))
    g_warning ("this is a monkey");
    */

  event_widget = gtk_get_event_widget (event);

  if (GTK_IS_LIST_ITEM (event_widget))
    {
      
      view = (View *) gtk_object_get_user_data (GTK_OBJECT (event_widget));
      //g_warning ("is item widget %p, event_widget %p, view %p", widget, event_widget, view);
      
      switch (event->type)
	{
	case GDK_BUTTON_PRESS:
#if 0
	  bevent = (GdkEventButton *) event;
	  
	  if (bevent->button == 3)
	    gtk_menu_popup (GTK_MENU (view->track_ops_menu), NULL, NULL, NULL, NULL, 3, bevent->time);
	  break;
#endif 
	  
	case GDK_2BUTTON_PRESS:
	  bevent = (GdkEventButton *) event;
	  //layers_dialog_edit_layer_query (layer_widget);
	  return TRUE;
	  
	case GDK_KEY_PRESS:
	  kevent = (GdkEventKey *) event;
	  /*	
		switch (kevent->keyval)
		{
		case GDK_Up:
		printf ("up arrow\n");
		break;
		case GDK_Down:
		printf ("down arrow\n");
		break;
		default:
		return FALSE;
		}
		*/
	  return TRUE;

	default:
	  break;
	}
    }

  return FALSE;
}

void
at_view_delete (View *view)
{
  view_list = g_slist_remove (view_list, view);

  gtk_widget_destroy (view->toplevel);
 
  g_free (view);
}

void 
at_view_update (View *view) 
{
  at_clip_update (view->clip);
}

static void
track_select_update (GtkWidget *widget,
		     gpointer data)
{
  gint track_ID;
  View *view = (View *)data;

  if (widget->state != GTK_STATE_SELECTED)
    return;

  /* FIXME this should be handled with a data pack passed along with the
     the item, but I'm slow and lazy --Larry */
  track_ID = (gint) gtk_object_get_data (GTK_OBJECT (widget), "at_track(ID)"); 

  at_clip_set_active_track (view->clip, track_ID);
}

void
at_view_update_tracks (View *view)
{
  GList *items = NULL;
  GSList *list = NULL;
  GtkWidget *item;
  GtkWidget *selected = NULL;
  gint track_ID;

  list = view->clip->tracks;

  while (list) 
    {
      track_ID = ((Track *)list->data)->ID;
      item = gtk_list_item_new ();
      gtk_object_set_user_data (GTK_OBJECT (item), (gpointer)view);
      /* FIXME this is the wrong way, and it needs to be removed, additionally
	 we would need to remove the data so that we don't leak mem as well */
      gtk_object_set_data (GTK_OBJECT (item), "at_track(ID)",
				(gpointer)track_ID);
      
      if (track_ID == view->clip->active_track)
	selected = item;

      gtk_signal_connect (GTK_OBJECT (item), "select",
			  (GtkSignalFunc) track_select_update,
			  view);
      gtk_signal_connect (GTK_OBJECT (item), "deselect",
			  (GtkSignalFunc) track_select_update,
			  view);
      gtk_container_add (GTK_CONTAINER (item), 
			 at_track_gui_new (view, track_ID));

      gtk_widget_show (item);
      items = g_list_prepend (items, item);
     
      list = list->next;
    }

	
  gtk_list_clear_items (GTK_LIST (view->track_list), 0, -1);
  gtk_list_append_items (GTK_LIST (view->track_list), items);

  if (selected)
    gtk_list_select_child (GTK_LIST (view->track_list), 
			   selected);
}		   		   
  
void 
at_views_update (gint clip_ID)
{
  GSList *list = view_list;
  View *view;

  while (list)
    {
      view = (View *) list->data;
      
      if (view->clip->ID == clip_ID)
	at_view_update_tracks (view);

      list = list->next;
    }
}

void
at_views_flush () 
{
  GSList *list = view_list;
  View *view;

  while (list)
    {
      view = (View *) list->data;
      at_views_update (view->clip->ID);
      
      list = list->next;
    }
}


gint
at_view_list_events (GtkWidget *widget,
		     GdkEvent  *event)
{
 return 0;
}


  
















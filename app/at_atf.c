#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>

#include <glib.h>
#include <gtk/gtk.h>

#include "at_track.h"
#include "at_clip.h"
#include "region.h"
#include "at_enums.h"
#include "at_atf.h"


typedef enum
{
  PROP_END = 0,
  PROP_ACTIVE_TRACK = 1,
  PROP_ACTIVE_CHANNEL = 2,
  PROP_IS_COMPOSITE = 3,    /* this is an ugly mess */
  PROP_SELECTION = 4,
  PROP_VOLUME = 5,
  PROP_MODE = 6,
  PROP_MUTE = 7,
  PROP_OFFSET = 8,
  PROP_COMPRESSION = 9
} PropType;

typedef enum
{
  COMPRESS_NONE = 0,
  COMPRESS_ALL = 1
} CompressionType;

typedef union
{
  gfloat  f;
  guint32 i;
} AtfFloatBox;

static gint   atf_save_clip          (AtfInfo *info, Clip *clip);
static void   atf_save_clip_props    (AtfInfo *info, Clip *clip);
static void   atf_save_track_props   (AtfInfo *info, Clip *clip, Track *track);
static void   atf_save_prop          (AtfInfo *info, PropType pre_type, ...);
static void   atf_save_track         (AtfInfo *info, Clip *clip, Track *track);
static void   atf_save_hierarchy     (AtfInfo *info, AudioRegion *);

static Clip * atf_load_clip          (AtfInfo *info); 
static gint   atf_load_clip_props    (AtfInfo *info, Clip *clip);
static gint   atf_load_track_props   (AtfInfo *info, Clip *clip, Track *track);
static Track *atf_load_track         (AtfInfo *info, Clip *clip);
static gint   atf_load_hierarchy     (AtfInfo *info, AudioRegion *);
static gint   atf_load_prop          (AtfInfo *info, PropType *, guint32 *);

static void   atf_seek_pos           (AtfInfo *info, guint pos);
static void   atf_seek_end           (AtfInfo *info);

static guint  atf_read_int32         (FILE *fp, guint32 *data, gint count);
static guint  atf_read_int8          (FILE *fp, guint8 *data, gint count);
static guint  atf_read_string        (FILE *fp, gchar **data, gint count);
static guint  atf_read_float32       (FILE *fp, gfloat *data, gint count);

static guint  atf_write_int32        (FILE *fp, guint32 *data, gint count);
static guint  atf_write_int8         (FILE *fp, guint8 *data, gint count);
static guint  atf_write_string       (FILE *fp, gchar **data, gint count);
static guint  atf_write_float32      (FILE *fp, gfloat *data, gint count);

Clip *at_load_atf_file (char *filename);

Clip *
atf_load (char *filename) 
{
  AtfInfo info;
  Clip *clip = NULL;
  gint success = FALSE;

  info.fp = fopen (filename, "r");
  if (info.fp)
    {
      info.cp = 0;
      info.filename = filename;
      info.active_track = NULL;
      info.ref_count = NULL;
      info.compression = COMPRESS_NONE;

      success = TRUE;
  
      clip = atf_load_clip (&info);
      if (!clip)
	success = FALSE;
    
      fclose (info.fp);
    }

  if (success)
    return clip;
  else
    return NULL;

}

gint 
atf_save (Clip *clip, char *filename)
{
  AtfInfo info;
  gint success = FALSE;
  
  info.fp = fopen (filename, "w");
  if (info.fp)
    {
      info.cp = 0;
      info.filename = filename;
      info.active_track = 0;
      info.ref_count = NULL;
      info.compression = COMPRESS_NONE;
      info.file_version = 0;
      
      success = TRUE;
      
      if (atf_save_clip (&info, clip));
      {
	success = FALSE;
	g_warning ("had problems saving");
      }
      fclose (info.fp);
    }
  
  if (success)
    return TRUE;
  else
    return FALSE;
}


static gint
atf_save_clip  (AtfInfo *info,
		Clip    *clip)
{
  Track *track;
  Track *floating_track;
  guint32 saved_pos;
  guint32 offset;
  guint ntracks;
  guint nchannels;
  GSList *list;
  int have_selection;
  char version_tag[14];

  /* write out the tag information for the image */
  sprintf (version_tag, "atech atf v%03d", info->file_version);
  info->cp += atf_write_int8 (info->fp, (guint8*) &version_tag, 14);

  /* write out the length and clip information for the clip */
  info->cp += atf_write_int32 (info->fp, (guint32*) &clip->length, 1);
  info->cp += atf_write_int32 (info->fp, (guint32*) &clip->sample_rate, 1);
  info->cp += atf_write_int32 (info->fp, (guint32*) &clip->base_type, 1);

  /* determine the number of tracks and channels in the image */
  ntracks = (guint) g_slist_length (clip->tracks);
  nchannels = 0;

  /* save the clip property */
  atf_save_clip_props (info, clip);

  /* save the current file position as it is the start of where
   *  we place the track offset information.
   */
  saved_pos = info->cp;

  /* seek to after the offset lists */
  atf_seek_pos (info, info->cp + (ntracks + nchannels + 2) * 4);

  list = clip->tracks;
  while (list)
    {
      track = list->data;
      list = list->next;

      /* save the start offset of where we are writing
       *  out the next track.
       */
      offset = info->cp;

      /* write out the track. */
      atf_save_track (info, clip, track);

      /* seek back to where we are to write out the next
       *  track offset and write it out.
       */
      atf_seek_pos (info, saved_pos);
      info->cp += atf_write_int32 (info->fp, &offset, 1);

      /* increment the location we are to write out the
       *  next offset.
       */
      saved_pos = info->cp;

      /* seek to the end of the file which is where
       *  we will write out the next track.
       */
      atf_seek_end (info);
    }

  /* write out a '0' offset position to indicate the end
   *  of the track offsets.
   */
  offset = 0;
  atf_seek_pos (info, saved_pos);
  info->cp += atf_write_int32 (info->fp, &offset, 1);
  /*
   * FIXME we don't yet have anthing like channels.. though we proabably should   */

  return !ferror(info->fp);
}

static void
atf_save_clip_props (AtfInfo *info,
		     Clip    *clip)
{
  atf_save_prop (info, PROP_COMPRESSION, info->compression);
  atf_save_prop (info, PROP_END);
}

static void
atf_save_track_props (AtfInfo *info,
		      Clip    *clip,
		      Track   *track)
{
  if (track->ID == clip->active_track)
    atf_save_prop (info, PROP_ACTIVE_TRACK);
  
  if (track->is_composite)
    atf_save_prop (info, PROP_IS_COMPOSITE);

  atf_save_prop (info, PROP_VOLUME, &track->volume);
  atf_save_prop (info, PROP_MUTE, track->mute);
  atf_save_prop (info, PROP_OFFSET, track->start_offset);
  atf_save_prop (info, PROP_MODE, track->mode);
  atf_save_prop (info, PROP_END);
}

static void
atf_save_prop (AtfInfo  *info,
	       PropType prop_type,
	       ...)
{
  guint32 size;
  va_list args;

  va_start (args, prop_type);

  switch (prop_type)
    {
    case PROP_END:
      size = 0;
      
      info->cp += atf_write_int32 (info->fp, (guint32*) &prop_type, 1);
      info->cp += atf_write_int32 (info->fp, &size, 1);
      break;
    case PROP_ACTIVE_TRACK:
    case PROP_ACTIVE_CHANNEL:
    case PROP_IS_COMPOSITE:
      size = 0;
      
      info->cp += atf_write_int32 (info->fp, (guint32*) &prop_type, 1);
      info->cp += atf_write_int32 (info->fp, &size, 1);
      break;
    case PROP_MODE:
      {
	guint32 mode;
	
	mode = va_arg (args, gint32);
	size = 4;
	
	info->cp += atf_write_int32 (info->fp, (guint32*) &prop_type, 1);
	info->cp += atf_write_int32 (info->fp, &size, 1);
	info->cp += atf_write_int32 (info->fp, (guint32*) &mode, 1);
      }
      break;
    case PROP_MUTE:
      {
	guint mute;
	
	mute = va_arg (args, guint32);
	size = 4;

	info->cp += atf_write_int32 (info->fp, (guint32*) &prop_type, 1);
	info->cp += atf_write_int32 (info->fp, &size, 1);
	info->cp += atf_write_int32 (info->fp, &mute, 1);
      }
      break;
    case PROP_OFFSET:
      {
	guint offset;
	gint mute;

	mute = va_arg (args, guint32);
	size = 4;

	info->cp += atf_write_int32 (info->fp, (guint32*) &prop_type, 1);
	info->cp += atf_write_int32 (info->fp, &size, 1);
	info->cp += atf_write_int32 (info->fp, &offset, 1);
      }
      break;
    case PROP_VOLUME:
      {
	gfloat *volume;
	
        volume = va_arg (args, gfloat *);
	size = 4;

	info->cp += atf_write_int32 (info->fp, (guint32*) &prop_type, 1);
	info->cp += atf_write_int32 (info->fp, &size, 1);
	info->cp += atf_write_float32 (info->fp, volume, 1);
      }
      break;
    default:
      g_print("mode invalid or not yet implemented");
    }

  va_end (args);
}

static void
atf_save_track (AtfInfo *info,
		Clip    *clip,
		Track   *track)
{
  guint32 saved_pos;
  guint32 offset;

  /* FIXME if this is a floating track... blah */
  info->cp += atf_write_int32 (info->fp, (guint32*) &track->length, 1);
  info->cp += atf_write_int32 (info->fp, (guint32*) &track->type, 1);

  /* write out the track name */
  info->cp += atf_write_string (info->fp, (gchar**) &track->name, 1);
  
  /* write out the track props */
  atf_save_track_props (info, clip, track);

  saved_pos = info->cp;
  atf_seek_pos (info, saved_pos + 8);
  offset = info->cp;

  atf_save_hierarchy (info, track->data);
  
  atf_seek_pos (info, saved_pos);
  info->cp += atf_write_int32 (info->fp, &offset, 1);
  saved_pos = info->cp;
}

static void
atf_save_hierarchy (AtfInfo     *info,
		    AudioRegion *region)
{
  guint32 saved_pos;
  guint32 offset;
  TmpRegion *tmp_region;
  int i;

  info->cp += atf_write_int32 (info->fp, (guint32*) &region->length, 1);
  info->cp += atf_write_int32 (info->fp, (guint32*) &region->bps, 1);
  saved_pos = info->cp;
  tmp_region = tmp_region_new (region, 0, region->length);
  atf_write_float32 (info->fp, (gfloat*) tmp_region->data,
		     region->length * region->bps / sizeof (gfloat));
  tmp_region_destroy (tmp_region);
}


static Clip *
atf_load_clip (AtfInfo *info)
{
  Clip   *clip;
  Track  *track;
  guint32 saved_pos;
  guint32 offset;
  gint length;
  gint sample_rate;
  gint base_type;
  char version_tag[14];

  /* read in the tag information for the image */
  info->cp += atf_read_int8 (info->fp, (guint8*) &version_tag, 14);

  /* read in the clip length and type */
  info->cp += atf_read_int32 (info->fp, (guint32*) &length, 1);
  info->cp += atf_read_int32 (info->fp, (guint32*) &sample_rate, 1);
  info->cp += atf_read_int32 (info->fp, (guint32*) &base_type, 1);

  /* create the new clip */
  clip = at_clip_new (length, base_type, sample_rate);
  if (!clip)
    return NULL;

  /* read the clip properties */
  if (!atf_load_clip_props (info, clip))
    goto error;

  while (1)
    {
      /* read in the offset of the next track */
      info->cp += atf_read_int32 (info->fp, &offset, 1);

      /* if offset == 0 we are at the end of the track list. */
      if (offset == 0)
	break;

      /* save the current position because it is where the next offset
       * is stored
       */
      saved_pos = info->cp;

      /* seek to the track offset */
      atf_seek_pos (info, offset);

      /* read in the track */
      track = atf_load_track (info, clip);
      if (!track)
	goto error;

      /* add the track to the clip */
      at_clip_add_track (clip, track, g_slist_length (clip->tracks));

      /* restore the saved posistion so we'll be ready to read the
       * next offset
       */
      atf_seek_pos (info, saved_pos);
    }

  /* we would read in the channels here if they actually existed ;) */
  if (info->active_track)
    at_clip_set_active_track (clip, info->active_track->ID);

  clip->dirty = TRUE;
  /* at_clip_set_filename */
  return clip;

 error:
  /* FIXME what the hell happened to at_clip_destroy... arggggg */
  /* at_clip_destroy (clip);*/
  return NULL;
}

static gint
atf_load_clip_props (AtfInfo *info,
		     Clip    *clip)
{
  PropType prop_type;
  guint32 prop_size;

  while (1)
    {
      if (!atf_load_prop (info, &prop_type, &prop_size))
	return FALSE;

      switch (prop_type)
	{
	case PROP_END:
	  return TRUE;
	case PROP_COMPRESSION:
	  {
	    guint8 compression;

	    g_warning ("FIXME compression isn't currently implemented");
	    info->cp += atf_read_int8 (info->fp, (guint8*) &compression, 1);
	    
	    info->compression = compression;
	  }
	  break;
	default:
	  g_warning ("unexpected/unknown clip property: %d (skipping)",
		     prop_type);
	  {
	    guint8 buf[16];
	    guint amount;

	    while (prop_size > 0)
	      {
		amount = MIN (16, prop_size);
		info->cp += atf_read_int8 (info->fp, buf, amount);
		prop_size -= MIN (16, amount);
	      }
	  }
	  return FALSE;
	  break;
	}
    }

  return FALSE;
}

static gint
atf_load_track_props (AtfInfo *info,
		      Clip    *clip,
		      Track   *track)
{
  PropType prop_type;
  guint32 prop_size;

  while (1)
    {
      if (!atf_load_prop (info, &prop_type, &prop_size))
	return FALSE;

      switch (prop_type)
	{
	case PROP_END:
	  return TRUE;
	case PROP_ACTIVE_TRACK:
	  info->active_track = track;
	  break;
	case PROP_IS_COMPOSITE:
	  track->is_composite = TRUE;
	  clip->composite_track = track;
	  break;
	case PROP_VOLUME:
	  info->cp += atf_read_float32 (info->fp, (gfloat*) &track->volume, 1);
	  g_warning ("reading track->volume = %f", track->volume);
	  break;
	case PROP_MUTE:
	  info->cp += atf_read_int32 (info->fp, (guint32*) &track->mute, 1);
	  break;
	case PROP_OFFSET:
	  info->cp += atf_read_int32 (info->fp, 
				      (guint32*) &track->start_offset, 1);
	  break;
	case PROP_MODE:
	  info->cp += atf_read_int32 (info->fp, (guint32*) &track->mode, 1);
	  break;
	default:
	  g_warning ("unexpected/unknown track property: %d (skipping)", prop_type);

	  {
	    guint8 buf[16];
	    guint amount;

	    while (prop_size > 0)
	      {
		amount = MIN (16, prop_size);
		info->cp += atf_read_int8 (info->fp, buf, amount);
		prop_size -= MIN (16, amount);
	      }
	  }
	  break;
	}
    }

  return FALSE;
}

static gint
atf_load_prop (AtfInfo  *info,
	       PropType *prop_type,
	       guint32  *prop_size)
{
  info->cp += atf_read_int32 (info->fp, (guint32*) prop_type, 1);
  info->cp += atf_read_int32 (info->fp, (guint32*) prop_size, 1);
  return TRUE;
}

static Track *
atf_load_track (AtfInfo  *info,
		Clip     *clip)
{
  Track *track;
  int    length;
  int    type;
  int    hierarchy_offset;
  char  *name;

  /* read in the track length and type */
  info->cp += atf_read_int32 (info->fp, (guint32*) &length, 1);
  info->cp += atf_read_int32 (info->fp, (guint32*) &type, 1);

  /* read in the track name */
  info->cp += atf_read_string (info->fp, &name, 1);

  /* create a new_track */
  track = at_track_new (clip->ID, 0, length, type, name, 1.0, 0 /* FIXME_MODE */);
  if (!track)
    {
      g_free (name);
      return NULL;
    }

  /* read in the track properties */
  if (!atf_load_track_props (info, clip, track))
    goto error;

  info->cp += atf_read_int32 (info->fp, &hierarchy_offset, 1);
  atf_seek_pos (info, hierarchy_offset);

  if (!atf_load_hierarchy (info, track->data))
      goto error;
  at_track_build_preview (track);
  
  at_track_mark_dirty (track, AT_TRACK_CHANGED_NEW_TRACK);
  
  return track;

  error:
  at_track_delete (track);
  return NULL;
}

static gint
atf_load_hierarchy (AtfInfo       *info,
		    AudioRegion   *region)
{
  guint32 saved_pos;
  guint32 offset;
  TmpRegion *tmp_region;
  int length;
  int bps;
  int i;
  
  info->cp += atf_read_int32 (info->fp, (guint32*) &length, 1);
  info->cp += atf_read_int32 (info->fp, (guint32*) &bps, 1);
  
  if (region->length != length)
    {
      g_warning ("The region length and track length do not match");
      return FALSE;
    }
  tmp_region = tmp_region_new (region, 0, region->length); 
  atf_read_float32 (info->fp, (gfloat*) tmp_region->data, region->length * region->bps / sizeof (gfloat));
  tmp_region_sync (tmp_region);
  tmp_region_destroy (tmp_region);
  return TRUE;
}


/* generic i/o functions */
/* 
 * FIXME only a pssing attempt has been made to get the float read/write
 * routines to be portable.  In all probability they are _very_ broken
 * but right now I don't care.
 */

static void
atf_seek_pos (AtfInfo *info,
	      guint    pos)
{
  if (info->cp != pos)
    {
      info->cp = pos;
      fseek (info->fp, info->cp, SEEK_SET);
    }
}

static void
atf_seek_end (AtfInfo *info)
{
  fseek (info->fp, 0, SEEK_END);
  info->cp = ftell (info->fp);
}

static guint
atf_write_int32 (FILE     *fp,
		 guint32  *data,
		 gint      count)
{
  guint32 tmp;
  int i;

  if (count > 0)
    {
      for (i = 0; i < count; i++)
        {
          tmp = htonl (data[i]);
          atf_write_int8 (fp, (guint8*) &tmp, 4);
        }
    }

  return count * 4;
}

static guint
atf_write_int8 (FILE     *fp,
		guint8   *data,
		gint      count)
{
  guint total;
  int bytes;

  total = count;
  while (count > 0)
    {
      bytes = fwrite ((char*) data, sizeof (char), count, fp);
      count -= bytes;
      data += bytes;
    }

  return total;
}

static guint
atf_write_string (FILE     *fp,
		  gchar   **data,
		  gint      count)
{
  guint32 tmp;
  guint total;
  int i;

  total = 0;
  for (i = 0; i < count; i++)
    {
      if (data[i])
        tmp = strlen (data[i]) + 1;
      else
        tmp = 0;

      atf_write_int32 (fp, &tmp, 1);
      if (tmp > 0)
        atf_write_int8 (fp, (guint8*) data[i], tmp);

      total += 4 + tmp;
    }

  return total;
}

static guint
atf_read_float32 (FILE    *fp,
		   gfloat  *data,
		   gint     count)
{
  guint total = 0;
  AtfFloatBox u_data;
  guint32    i_data;
  int i;

  for (i = 0; i < count; i++)
    {
      total += atf_read_int32 (fp, &i_data, 1);
      u_data.i = i_data;
      data[i] = u_data.f; 
    }
  
  return total;
}
 
static guint
atf_read_int32 (FILE     *fp,
		guint32  *data,
		gint      count)
{
  guint total;

  total = count;
  if (count > 0)
    {
      atf_read_int8 (fp, (guint8*) data, count * 4);

      while (count--)
        {
          *data = ntohl (*data);
          data++;
        }
    }

  return total * 4;
}

static guint
atf_read_int8 (FILE     *fp,
	       guint8   *data,
	       gint      count)
{
  guint total;
  int bytes;

  total = count;
  while (count > 0)
    {
      bytes = fread ((char*) data, sizeof (char), count, fp);
      if (bytes <= 0) /* something bad happened */
        break;
      count -= bytes;
      data += bytes;
    }

  return total;
}

static guint
atf_read_string (FILE     *fp,
		 gchar   **data,
		 gint      count)
{
  guint32 tmp;
  guint total;
  int i;

  total = 0;
  for (i = 0; i < count; i++)
    {
      total += atf_read_int32 (fp, &tmp, 1);
      if (tmp > 0)
        {
          data[i] = g_new (gchar, tmp);
          total += atf_read_int8 (fp, (guint8*) data[i], tmp);
        }
      else
        {
          data[i] = NULL;
        }
    }

  return total;
}

static guint
atf_write_float32 (FILE    *fp,
		   gfloat  *data,
		   gint     count)
{
  guint total = 0;
  AtfFloatBox u_data;
  guint32    i_data;
  int i;

  for (i = 0; i < count; i++)
    {
      u_data.f = data[i];
      i_data = u_data.i;
      total += atf_write_int32 (fp, &i_data, 1);
     }
  
  return total;
}


#ifndef __AT_ENUMS_H__
#define __AT_ENUMS_H__

typedef enum
{
    AT_MIX_ADD_TRACK,
    AT_MIX_SUB_TRACK,
    AT_MIX_VOLUME_CHANGED,
    AT_MIX_OFFSET_CHANGED,
    AT_MIX_REMIX_ALL
} MixType;



/* these are to make us able to do 'smart' mixing.  For instance, there
   is no need to mix all tracks when just adding a new one. */

typedef enum
{
    AT_TRACK_CHANGED_NONE,
    AT_TRACK_CHANGED_VOLUME,
    AT_TRACK_CHANGED_DELETED,
    AT_TRACK_CHANGED_NEW_TRACK,
    AT_TRACK_CHANGED_OFFSET,
    AT_TRACK_CHANGED_MUTED,
    AT_TRACK_CHANGED_UNMUTED,
    /* if multiple things have been changed, 
       we are unable to do tricky mixing. */
    AT_TRACK_CHANGED_MULTIPLE
} TrackChange;


#endif /* __AT_ENUMS_H__ */


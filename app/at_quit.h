#ifndef __AT_QUIT_H__
#define __AT_QUIT_H__

void at_quit(void);
void at_quit_callback (GtkWidget *widget, gpointer data);

#endif /* __AT_QUIT_H__ */

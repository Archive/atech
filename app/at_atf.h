#ifndef __AT_ATF_H__
#define __AT_ATF_H__
#include <stdio.h>

typedef struct _AtfInfo AtfInfo;

struct _AtfInfo
{
  FILE *fp;
  gulong cp;
  char *filename;
  Track *active_track;
  int *ref_count;
  int compression;
  int file_version;
};

#endif /* __AT_ATF_H__ */






#ifndef __AT_TRACK_H__
#define __AT_TRACK_H__
#include <gtk/gtkwidget.h>
#include "at_type.h"
#include "region.h"
#include "at_view.h"
#include "at_enums.h"


typedef struct _Track Track;

struct _Track
{
    GtkWidget *wave_widget;     /* The GtkWave widget.      */
    GtkWidget *spinner;         /* spinner to set range on wave */

    gfloat *preview[2];         /* Preview for wave widget
				 * Currently it must be float */
    gint preview_len;           /* Length of Preview */
    gint preview_compression;   /* track->length / track->preview_compression for preview length */
    gint preview_dirty;         /* does the preview need rebuilding ? */

    gchar *name;                /* name of the track        */

    gint mute;                  /* is this track muted?     */
    gint is_composite;          /* is this the composite track ? */

    GSList *adjustments;        /* track adjustments...     */

    AudioRegion *data;          /* the audio....            */

    size_t start_offset;        /* start offset             */

    gint type;
    size_t length;              /* the length of the track  */
    gint bytes;

    gfloat volume;              /* % mixed                  */
    gint mode;                  /* track combination mode   */

    gint dirty;                 /* Count of how many times track has been changed */
    TrackChange change;         /* Original event to modify track  */
    gfloat old_volume;          /* For remixing when the volume is changed.  It's a bit of a
				   performance hack, but it does work well. */
    gint delete_me;             /* When track requires deletion, and
				   this is marked TRUE, the delete is done
				   at the end of the streamlined mix (if appropriate)*/
    
    gint ID;                    /* unique ID                */
    gint clip_ID;               /* ID of clip owner         */
};

/* function declarations */
Track *          at_track_new                    (gint, size_t, size_t, gint, 
						  gchar *, gfloat, gint);
void             at_track_delete                 (Track *track);
Track *          at_track_copy                   (Track *track);
void             at_track_resize                 (Track *track, int, guint32); 
void             at_track_build_preview          (Track *track);
void             at_track_update_preview         (Track *track);
void             at_track_adjust_volume_of_preview (Track *track, 
						  gfloat old_volume, 
						  gfloat new_volume);


/* access funtions */
void             at_track_invalidate_thumbnail   (gint);
void             at_track_translate              (Track *track, size_t);
Track *          at_track_get_ID                 (gint);
void             at_track_mark_dirty             (Track *track, TrackChange change);
atdata *         at_track_get_track_data         (Track *track);

/* callbacks */
void             at_track_new_cmd_callback       (GtkWidget *, gpointer);
void             at_track_delete_cmd_callback    (GtkWidget *, gpointer);
void             at_track_raise_cmd_callback     (GtkWidget *, gpointer);
void             at_track_lower_cmd_callback     (GtkWidget *, gpointer);

void             at_track_init                   (void);
gchar *          at_track_get_filename           (gint id);

Track *          at_clip_add_track               (Clip *clip, Track *, gint);
Track *          at_clip_remove_track            (Clip *clip, gint);
gchar *          at_track_get_record_track       (void);
void             at_track_record_done            (Clip *clip);
GtkWidget *      at_track_gui_new                (View *view, gint);

#endif /* __AT_TRACK_H__ */



















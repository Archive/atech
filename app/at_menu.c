/* Audiotechque -- Multitrack Mixer and Audio manipulation program
 * Copyright (C) 1997-1998 Larry Ewing and Ian Main
 *
 * This code was inspired from something.. I think it was Gzilla, 
 * maybe GIMP :-)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>
#include <strings.h>

#include "at_menu.h"
#include "at_main.h"
#include "at_com.h"
#include "at_quit.h"
#include "at_track.h"
#include "at_clip.h"


static void menus_remove_accel(GtkWidget *widget, 
			       gchar *signal_name, 
			       gchar * path);

static gint menus_install_accel(GtkWidget *widget, 
				gchar *signal_name, 
				gchar key, gchar modifiers, 
				gchar * path);

static GtkMenuFactory *factory = NULL;
static GtkMenuFactory *subfactory[1];
static GHashTable *entry_ht = NULL;

void at_menu_new(GtkWidget **menubar, GtkAccelGroup **table,
		 GtkMenuEntry *menu_items, gint nmenu_items)
{
  factory = gtk_menu_factory_new(GTK_MENU_FACTORY_MENU_BAR);
  
  subfactory[0] = gtk_menu_factory_new(GTK_MENU_FACTORY_MENU_BAR);
  
  gtk_menu_factory_add_subfactory(factory, subfactory[0], "<Main>");
  
  at_menu_create (factory, menu_items, nmenu_items);
  
  *menubar = subfactory[0]->widget;
  *table = subfactory[0]->accel_group;
}

void at_menu_create(GtkMenuFactory *menu, 
		    GtkMenuEntry *entries, int nmenu_entries)
{
  char *accelerator;
  int i;
  
  for (i = 0; i < nmenu_entries; i++) {
    accelerator = g_hash_table_lookup(entry_ht, entries[i].path);
    if (accelerator) {
      if (accelerator[0] == '\0')
	      entries[i].accelerator = NULL;
      else
	      entries[i].accelerator = accelerator;
    }
  }
  
  
  gtk_menu_factory_add_entries(factory, entries, nmenu_entries);
  

  for (i = 0; i < nmenu_entries; i++)
	  if (entries[i].widget) {
	    gtk_signal_connect(GTK_OBJECT(entries[i].widget), "install_accelerator",
			       (GtkSignalFunc) menus_install_accel,
			       entries[i].path);
	    gtk_signal_connect(GTK_OBJECT(entries[i].widget), "remove_accelerator",
			       (GtkSignalFunc) menus_remove_accel,
			       entries[i].path);
	  }
}

static gint menus_install_accel(GtkWidget * widget, gchar * signal_name, gchar key, gchar modifiers, gchar * path)
{
  char accel[64];
  char *t1, t2[2];
  
  accel[0] = '\0';
  if (modifiers & GDK_CONTROL_MASK)
	  strcat(accel, "<control>");
  if (modifiers & GDK_SHIFT_MASK)
	  strcat(accel, "<shift>");
  if (modifiers & GDK_MOD1_MASK)
	  strcat(accel, "<alt>");
  
  t2[0] = key;
  t2[1] = '\0';
  strcat(accel, t2);
  
  if (entry_ht) {
    t1 = g_hash_table_lookup(entry_ht, path);
    g_free(t1);
  } else
	  entry_ht = g_hash_table_new(g_str_hash, g_str_equal);
  
  g_hash_table_insert(entry_ht, path, g_strdup(accel));
  
  return TRUE;
}

static void menus_remove_accel(GtkWidget * widget, gchar * signal_name, gchar * path)
{
  char *t;
  
  if (entry_ht) {
    t = g_hash_table_lookup(entry_ht, path);
    g_free(t);
    
    g_hash_table_insert(entry_ht, path, g_strdup(""));
  }
}

void menus_set_sensitive(char *path, int sensitive)
{
  GtkMenuPath *menu_path;
  
  menu_path = gtk_menu_factory_find(factory, path);
  if (menu_path)
	  gtk_widget_set_sensitive(menu_path->widget, sensitive);
  else
	  g_warning("Unable to set sensitivity for menu which doesn't exist: %s", path);
}





/* Audiotechque -- Multitrack Mixer and Audio manipulation program
 * Copyright (C) 1997-1998 Larry Ewing and Ian Main
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdarg.h>

#include <libintl.h>
#include <gtk/gtk.h>
#include <gnome.h>

#include "at_interface.h"
#include "at_menu.h"
#include "at_com.h"
#include "at_quit.h"
#include "at_track.h"
#include "version.h"
#include "at_fileop.h"

GtkWidget *scrolled_window_vbox;


/* this is the GtkMenuEntry structure used to create new menus.  The
 * first member is the menu definition string.  The second, the
 * default accelerator key used to access this menu function with
 * the keyboard.  The third is the callback function to call when
 * this menu item is selected (by the accelerator key, or with the
 * mouse.) The last member is the data to pass to your callback function.
 */

GtkMenuEntry main_menu_items[] =
{
	{"<Main>/Preferences/Play...", NULL, at_com_play_configure, NULL},
    	{"<Main>/Preferences/Record...", NULL, at_com_record_configure, NULL}
};

GnomeUIInfo file_menu[] = {
	{GNOME_APP_UI_ITEM,
	N_("New..."), NULL,
	at_clip_new_cmd_callback, NULL, NULL,
	GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW,
	'N', GDK_CONTROL_MASK, NULL},

	{GNOME_APP_UI_ITEM,
	N_("Open..."), NULL,
	at_fileop_open_cmd, NULL, NULL,
	GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_OPEN,
	'O', GDK_CONTROL_MASK, NULL},

	{GNOME_APP_UI_ITEM,
	N_("About"), NULL,
	NULL, NULL, NULL,
	GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ABOUT,
	0, 0, NULL},

	GNOMEUIINFO_SEPARATOR,

	{GNOME_APP_UI_ITEM,
	N_("Quit"), NULL,
	at_quit, NULL, NULL,
	GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_QUIT,
	'Q', GDK_CONTROL_MASK, NULL},

	GNOMEUIINFO_END
};

GnomeUIInfo pref_menu[] = {
	{GNOME_APP_UI_ITEM,
	N_("Play..."), NULL,
	at_com_play_configure, NULL, NULL,
	GNOME_APP_PIXMAP_NONE, NULL,
	0, 0, NULL},

	{GNOME_APP_UI_ITEM,
	N_("Record..."), NULL,
	at_com_record_configure, NULL, NULL,
	GNOME_APP_PIXMAP_NONE, NULL,
	0, 0, NULL},

	GNOMEUIINFO_END
};

GnomeUIInfo main_menu[] = {
	GNOMEUIINFO_SUBTREE(N_("File"), file_menu),
	GNOMEUIINFO_SUBTREE(N_("Preferences"), pref_menu),
	GNOMEUIINFO_END
};

/* calculate the number of menu_item's */
gint nmain_menu_items = sizeof(main_menu_items) / sizeof(main_menu_items[0]);


static gint at_interface_delete (void)
{
    return(TRUE);
}


void at_interface_init(void)
{
    GtkWidget *window;
    
    gchar buf[1024];
    
    /* main window */
    sprintf (buf, "Audiotechque %s", VERSION);
    window = gnome_app_new ("atech", buf);

    gtk_window_set_policy(GTK_WINDOW(window), TRUE, TRUE, TRUE);

    gtk_container_border_width(GTK_CONTAINER(window), 0);

    gtk_signal_connect(GTK_OBJECT(window), "destroy",
		       GTK_SIGNAL_FUNC(at_quit), NULL);
    gtk_signal_connect(GTK_OBJECT(window), "delete_event",
		       GTK_SIGNAL_FUNC(at_interface_delete), NULL);

    gtk_window_position (GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    
    /* menubar */
    gnome_app_create_menus ( GNOME_APP(window), main_menu);

    /* show window */
    gtk_widget_show (GTK_WINDOW(window));

}


/* Just for debugging and playing around with */
void 
at_print (gchar *format, ...)
{
    GtkWidget *label;
    
    va_list args;
    gchar *buf;
    
    va_start (args, format);
    buf = g_strdup_vprintf (format, &args);
    va_end (args);

    label = gtk_label_new (buf);
    gtk_box_pack_start(GTK_BOX(scrolled_window_vbox), label, FALSE, TRUE, 0);
    gtk_widget_show(label);
}
 
GtkWidget *
at_interface_pixmapped_button_new (gchar *xpm_filename, 
					      gchar *label_text)
{
    GtkWidget *hbox;
    GtkWidget *pixmap;
    GtkWidget *button;
    GtkWidget *label;
    
    button = gtk_button_new();
    
    hbox = gtk_hbox_new(FALSE, 5);
    gtk_container_add (GTK_CONTAINER (button), hbox);
    gtk_widget_show (hbox);
    
    if (label_text != NULL) {
	label = gtk_label_new (label_text);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, TRUE, 1);
	gtk_widget_show (label);
    }
    
    if (xpm_filename !=NULL) {
	pixmap = at_interface_pixmap_new (button, xpm_filename);
	gtk_box_pack_start (GTK_BOX (hbox), pixmap, FALSE, TRUE, 1);
	gtk_widget_show (pixmap);
    }
    
    return (button);
}
   
/* utility function */

GtkWidget *at_interface_pixmap_new (GtkWidget *parent, gchar *xpm_filename)
{
    GtkWidget *pixmapwid;
    GdkPixmap *pixmap;
    GdkBitmap *mask;
    GtkStyle *style;

    gtk_widget_realize (parent);

    style = gtk_widget_get_style(parent);

    /* now on to the xpm stuff.. load xpm */
    pixmap = gdk_pixmap_create_from_xpm (parent->window, &mask,
					 &style->bg[GTK_STATE_NORMAL],
					 xpm_filename);
    pixmapwid = gtk_pixmap_new (pixmap, mask);
    return(pixmapwid);
}       

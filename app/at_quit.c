/* Audiotechque -- Multitrack Mixer and Audio manipulation program
 * Copyright (C) 1997-1998 Ian Main
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <gtk/gtk.h>

#include "at_quit.h"
#include "at_com.h"

/* in the future, we may wish to pop up an 'are you sure ?' box, and do
 * more cleanup.  This may also be called by signal handlers in the
 * future.  We have to cleanup all them temp files. */

void at_quit_callback (GtkWidget *widget, gpointer data)
{
    at_quit();
}

void at_quit (void)
{
    at_com_cleanup();
    
    gtk_main_quit();
}


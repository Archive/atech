#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <sys/ioctl.h>

/* this is the correct location really */
/* #include <linux/sound.h> */
#include "sound.h"


#include <gtk/gtk.h>

#include "libatech.h"
#include "atech_record.h"

#define DSP_BUF_SIZE 4096


/* our lifeline to the main module */
IpcProcess *main_module;

/* are we currently recording ?  TRUE/FALSE */
gint recording;

/* for gdk_input_add - used on record device */
gint record_tag;

/* file descriptors */
gint destfd;
gint dspfd;

/* getting the feeling this should all go in a struct :) */

/* this will be a linked list of files to mix in the future */
gchar *output_file;		    

/* one global struct to keep state info for us - mostly used
 * for IPC */
AtechRecordAttr record_info;


static void set_ALSA_dsp (gint fd, gint rate,
			  gint bits, gint channels);

static void start_recording(IpcProcess *ipcp, gpointer data);

static void stop_recording(IpcProcess *ipcp, gpointer data);

static void configure(IpcProcess *ipcp, gpointer data);

static void record_quit(IpcProcess *ipcp, gpointer data);

static void record_data (void);

static void stop_recording_real (void);

static void native_to_AFF (guint8 *buf, at_sample *sample, gint size);

gint main (gint argc, gchar *argv[])
{
        
    gtk_init(&argc, &argv);
    
    /* obviously we're not recording when we start */
    recording = FALSE;

    /* initialize connection to main module */
    main_module = at_ipc_init(argc, argv);

    /* setup sane defaults.. this will probably be loaded from
     * a config file in the future. */
    record_info.rate = 44100;
    record_info.bits = 16;
    record_info.channels = 2;
    strcpy (record_info.device, "/dev/sndpcm00");
    
    /* set up our callbacks for various signals */
    at_ipc_signal_connect (main_module, "start_recording", 
			   start_recording, NULL);
    at_ipc_signal_connect (main_module, "stop_recording", 
			   stop_recording, NULL);
    at_ipc_signal_connect (main_module, "configure",
			   configure, NULL);
    at_ipc_signal_connect (main_module, "destroy",
			   record_quit, NULL);
    
    /* sit in gtk_main waiting for input from main module */
    gtk_main();
    
    return(0);
}

static void start_recording(IpcProcess *ipcp, gpointer data)
{
    at_ipc_signal_recv_args(main_module, "alloc string",
			    &output_file);
    
    /* if we're already recording, just leave */
    if (!recording) {
	recording = TRUE;
    } else {
	return;
    }
    
    if ( (dspfd = open(record_info.device, O_RDONLY)) < 0) {
	g_warning("Opening %s: %s\n", record_info.device, g_strerror(errno));
	stop_recording_real();
	return;
    }

    if ( (destfd = open(output_file, O_WRONLY | O_CREAT | O_TRUNC, 0600)) < 0) {
	g_warning("Opening %s: %s\n", output_file, g_strerror(errno));
	stop_recording_real();
	return;
    }

    g_print ("recording file %s: device %s - rate %d - bits %d - channels %d\n",
	     output_file, record_info.device, record_info.rate, record_info.bits,
	     record_info.channels);   
    
    set_ALSA_dsp (dspfd, record_info.rate, 
		 record_info.bits, record_info.channels);
    
    record_tag = gtk_idle_add (record_data, NULL);
}

static void stop_recording(IpcProcess *ipcp, gpointer data)
{
    stop_recording_real();
}

static void stop_recording_real (void)
{
    /* add GUI error ? */
    recording = FALSE;
    gtk_idle_remove (record_tag);
    close (dspfd);
    close (destfd);
    
    g_print ("emitting 'done_recording' signal to master\n");

    at_ipc_signal_emit (main_module, "recording_completed");
}

static void record_data (void)
{
    gint nread;
    guint8 buf[DSP_BUF_SIZE];
    at_sample sample[DSP_BUF_SIZE];

    if (recording == FALSE) {
	stop_recording_real();
	return;
    }

    if ( (nread = read(dspfd, buf, DSP_BUF_SIZE)) < DSP_BUF_SIZE) {
	g_warning("Reading from '%s': %s\n", record_info.device, g_strerror(errno));
	stop_recording_real();
    }

    native_to_AFF (buf, sample, DSP_BUF_SIZE);

    /* it looks funky, but it's right.  The record_info.bits / 8 gives the number of
       bytes per sample */
    nread = nread * AT_SAMPLE_SIZE / (record_info.bits / 8);
    
    if ( (write(destfd, sample, nread)) < 0) {
	g_warning("Writing to '%s': %s\n", output_file, g_strerror(errno));
	stop_recording_real();
    }
}

static void native_to_AFF (guint8 *buf, at_sample *sample, gint size)
{
    guint i;
    guint16 *buf16 = (gint16 *) buf;

    if (record_info.bits == 8) {
        for (i=0; i < size; i++) {
	    sample[i] = at_convert_8_bit_PCM_to_AFF(buf[i]);
	}
    } else {
	size = size / sizeof (guint16);
        for (i=0; i < size; i++) {
	    sample[i] = at_convert_16_bit_PCM_to_AFF(buf16[i]);
	}
    }
}

    
static void set_ALSA_dsp (gint fd, gint rate, 
			  gint bits, gint channels)
{
    snd_pcm_format_t pcm_fmt;

    /* set format of buffer */
    if (bits == 16) {
	/* Little Endian unsigned 16 bit doesnt seem to work properly */
	/* pcm_fmt.format = SND_PCM_SFMT_U16_LE; */
	pcm_fmt.format = SND_PCM_SFMT_S16_LE;
    } else {
	/* signed 8 bit */
	pcm_fmt.format = SND_PCM_SFMT_S8;
    }
    pcm_fmt.rate = rate;
    pcm_fmt.channels = channels;

    if ( ioctl( fd, SND_PCM_IOCTL_RECORD_FORMAT, &pcm_fmt ) < 0 ) { 
	g_warning ("Could not set record variables: %s",
		   g_strerror(errno));
    }
}

static void record_quit(IpcProcess *ipcp, gpointer data)
{
    gtk_main_quit();
}

/******************************************************
 * 
 * This begins the user interface section.  It only does
 * the configuration for the record server.
 *
 ******************************************************/

GtkWidget *config_window;
GtkWidget *device_entry;
gint config_window_hidden;

static void config_hide (GtkWidget *widget, gpointer data)
{
    gchar *device_text;
    
    gtk_widget_hide (config_window);
    config_window_hidden = TRUE;
    
    device_text = gtk_entry_get_text (GTK_ENTRY (device_entry));
    
    strcpy (record_info.device, device_text);
    
    g_print ("device %s - rate %d - bits %d - channels %d\n",
	     record_info.device, record_info.rate, record_info.bits,
	     record_info.channels);   
}

static void set_record_bits (GtkWidget *widget, gpointer data)
{
    gint bits = (gint) data;
    
    record_info.bits = bits;
}

static void set_record_rate (GtkWidget *widget, gpointer data)
{
    gint rate = (gint) data;
    
    record_info.rate = rate;
}

static void set_record_channels (GtkWidget *widget, gpointer data)
{
    gint channels = (gint) data;
    
    record_info.channels = channels;
}


static void configure (IpcProcess *ipcp, gpointer data)
{
    GtkWidget *box1;
    GtkWidget *button;
    GtkWidget *hbox;
    GtkWidget *vbox;
    GtkWidget *frame;
    
    if (config_window_hidden) {
	gtk_widget_show (config_window);
	config_window_hidden = FALSE;
	return;
    }
    
    /* configuration window */
    config_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_signal_connect(GTK_OBJECT(config_window), "delete_event",
		       GTK_SIGNAL_FUNC (config_hide), NULL);
    gtk_signal_connect(GTK_OBJECT(config_window), "destroy",
		       GTK_SIGNAL_FUNC (config_hide), NULL);
    gtk_window_set_title (GTK_WINDOW(config_window), "Record Server Config");
    gtk_container_border_width (GTK_CONTAINER(config_window), 5);

    /* main vbox */
    box1 = gtk_vbox_new (FALSE, 5);
    gtk_container_border_width (GTK_CONTAINER (box1), 10);
    gtk_container_add(GTK_CONTAINER (config_window), box1);
    gtk_widget_show(box1);
    
    /* radio buttons for bits - 16 or 8 */
    /* start with frame */
    frame = gtk_frame_new ("Sample Size");
    gtk_box_pack_start (GTK_BOX (box1), frame, TRUE, TRUE, 0);
    gtk_widget_show (frame);

    vbox = gtk_vbox_new (FALSE, 5);
    gtk_container_border_width (GTK_CONTAINER (vbox), 5);
    gtk_container_add (GTK_CONTAINER (frame), vbox);
    gtk_widget_show (vbox);

    /* 16 bit */
    button = gtk_radio_button_new_with_label (NULL, "16 bit");
    gtk_signal_connect (GTK_OBJECT (button), "clicked", 
			GTK_SIGNAL_FUNC (set_record_bits), (gpointer)16);
    gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (button), TRUE);
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_widget_show (button);
    
    /* 8 bit */
    button = gtk_radio_button_new_with_label (
					      gtk_radio_button_group (GTK_RADIO_BUTTON (button)),
					      "8 bit");
    gtk_signal_connect (GTK_OBJECT (button), "clicked", 
			GTK_SIGNAL_FUNC (set_record_bits), (gpointer)8);
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_widget_show (button);
    
    /* buttons for sample frequency - 44100Khz, 22050Khz, 11025khz*/
    /* radio buttons for bits - 16 or 8 */
    frame = gtk_frame_new ("Sample Frequency");
    gtk_box_pack_start (GTK_BOX (box1), frame, TRUE, TRUE, 0);
    gtk_widget_show (frame);

    vbox = gtk_vbox_new (FALSE, 5);
    gtk_container_border_width (GTK_CONTAINER (vbox), 5);
    gtk_container_add (GTK_CONTAINER (frame), vbox);
    gtk_widget_show (vbox);

    /* 44100 khz */
    button = gtk_radio_button_new_with_label (NULL, "44100 Khz");
    gtk_signal_connect (GTK_OBJECT (button), "clicked", 
			GTK_SIGNAL_FUNC (set_record_rate), (gpointer)44100);
    gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (button), TRUE);
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_widget_show (button);
    
    /* 22050 Khz */
    button = gtk_radio_button_new_with_label (
					      gtk_radio_button_group (GTK_RADIO_BUTTON (button)),
					      "22050 Khz");
    gtk_signal_connect (GTK_OBJECT (button), "clicked", 
			GTK_SIGNAL_FUNC (set_record_rate), (gpointer)22050);
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_widget_show (button);

    /* 11025 Khz */
    button = gtk_radio_button_new_with_label (
					      gtk_radio_button_group (GTK_RADIO_BUTTON (button)),
					      "11025 Khz");
    gtk_signal_connect (GTK_OBJECT (button), "clicked", 
			GTK_SIGNAL_FUNC (set_record_rate), (gpointer)11025);
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_widget_show (button);

    /* radio buttons for channels */
    /* start with frame */
    frame = gtk_frame_new ("Channels");
    gtk_box_pack_start (GTK_BOX (box1), frame, TRUE, TRUE, 0);
    gtk_widget_show (frame);

    vbox = gtk_vbox_new (FALSE, 5);
    gtk_container_border_width (GTK_CONTAINER (vbox), 5);
    gtk_container_add (GTK_CONTAINER (frame), vbox);
    gtk_widget_show (vbox);

    /* Stereo */
    button = gtk_radio_button_new_with_label (NULL, "Stereo");
    gtk_signal_connect (GTK_OBJECT (button), "clicked", 
			GTK_SIGNAL_FUNC (set_record_channels), (gpointer)2);
    gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (button), TRUE);
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_widget_show (button);
    
    /* Mono */
    button = gtk_radio_button_new_with_label (
					      gtk_radio_button_group (GTK_RADIO_BUTTON (button)),
					      "Mono");
    gtk_signal_connect (GTK_OBJECT (button), "clicked", 
			GTK_SIGNAL_FUNC (set_record_channels), (gpointer)1);
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    gtk_widget_show (button);

    
    /* frame for device entry */
    frame = gtk_frame_new ("Recording device");
    gtk_box_pack_start (GTK_BOX (box1), frame, TRUE, TRUE, 0);
    gtk_widget_show (frame);

    vbox = gtk_vbox_new (FALSE, 5);
    gtk_container_border_width (GTK_CONTAINER (vbox), 5);
    gtk_container_add (GTK_CONTAINER (frame), vbox);
    gtk_widget_show (vbox);
    
    /* device entry */
    device_entry = gtk_entry_new();
    gtk_box_pack_end(GTK_BOX(vbox), device_entry, FALSE, FALSE, 6);
    gtk_entry_set_text(GTK_ENTRY(device_entry), record_info.device);
    gtk_widget_show(device_entry);
        
    /* hbox for close and ___ buttons */
    hbox = gtk_hbox_new (FALSE, 1);
    gtk_box_pack_start(GTK_BOX (box1), hbox, FALSE, FALSE, 0);
    gtk_widget_show (hbox);
    
    /* close button */
    button = gtk_button_new_with_label("Close");
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
			GTK_SIGNAL_FUNC (config_hide), 
			NULL);
    gtk_box_pack_start(GTK_BOX (hbox), button, TRUE, TRUE, 6);
    GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
    gtk_widget_grab_default (button);
    gtk_widget_show (button);

    /* show the window */
    gtk_widget_show (config_window);
}


#ifndef __REGION_H__
#define __REGION_H__
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <glib.h>

#define REGION_MMAP 0
#define REGION_MALLOC 1
#define MIN_MMAP_LENGTH 12

#define AT_SYNC 0
#define AT_ASYNC 1

typedef struct _TmpRegion TmpRegion;
typedef struct _AudioRegion AudioRegion;

struct _AudioRegion 
{
  int      type;                /* the type of the region    */
  
  char    *filename;            /* the filename of the mmap  */
  int      fd;                  /* the fd of file for mmap   */
  off_t    offset;              /* the initial offset        */
  size_t   length;              /* the length of the buffer  */
  
  gint     bps;                 /* bytes per sample */
  
  TmpRegion *tmp_region;        /* tmp region for mmap access */
  gint     tmp_region_ref;      /* ref count of tmp regions */
};


struct _TmpRegion
{
  int type;                   /* type of the data */
  size_t length;              /* the length of the data (number of samples) */
  gint  bps;                  /* bytes per sample  */
  
  gint  sample_rate;          /* sample rate CURRENTLY UNUSED */
  gint ref;                   /* keep track of reference counts */

  void *data;                 /* mmap'd file data */

  AudioRegion *parent;
};

/* access functions */

AudioRegion*  region_new                (size_t length, gint bps);           
AudioRegion*  region_new_from_file      (char *, gint bps);
void          region_delete             (AudioRegion *);
void region_resize (AudioRegion *, size_t offset, size_t length);	 
void region_sync (AudioRegion *region);

void tmp_region_sync (TmpRegion *region);
void tmp_region_zero (TmpRegion *region);
TmpRegion* tmp_region_new (AudioRegion *src,
			   size_t offset, 
			   size_t length);

void tmp_region_destroy (TmpRegion *del);


#endif /* __REGION_H__ */















#ifndef __LIBATECH_H__
#define __LIBATECH_H__

#include "libatech/atech_ipc.h"
#include "libatech/atech_misc.h"
#include "libatech/atech_convert.h"
#include "libatech/atech_menu.h"
#include "libatech/gtkwave.h"
#include "libatech/atech_plugin.h"

#endif /* __LIBATECH_H__ */
